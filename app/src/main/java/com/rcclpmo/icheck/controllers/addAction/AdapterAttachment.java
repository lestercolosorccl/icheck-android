package com.rcclpmo.icheck.controllers.addAction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseViewHolder;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.helpers.FileHelper;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Attachment;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class AdapterAttachment extends RecyclerView.Adapter<AdapterAttachment.AttachmentViewHolder> {

    private List<Attachment> attachmentList;
    private RecyclerViewClickListener<Object> listener;
    private Context context;

    public AdapterAttachment(Context context,List<Attachment> attachmentList, RecyclerViewClickListener<Object> listener){
        this.attachmentList = attachmentList;
        this.listener = listener;
        this.context = context;

    }

    @Override
    public AttachmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_attachment_item,parent,false);

        return new AttachmentViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(AttachmentViewHolder holder, int position) {
        holder.setViews(attachmentList.get(position));
    }

    @Override
    public int getItemCount() {
        return attachmentList.size();

    }

    public class AttachmentViewHolder extends BaseViewHolder<Object> {

        private ImageView ivUploadFile;
        private ImageView ivDelete;
        private ApplicationClass applicationClass;

        public AttachmentViewHolder(View itemView, RecyclerViewClickListener<Object> listener) {
            super(itemView, listener);

            ivUploadFile = (ImageView) itemView.findViewById(R.id.nivAttachment);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDeletedAttachments);
            applicationClass = ApplicationClass.getInstance();

            ivUploadFile.setOnClickListener(this);
            itemView.setOnClickListener(this);
            ivDelete.setOnClickListener(this);
        }

        @Override
        public Object getObject() {
            return attachmentList.get(getAdapterPosition());
        }

        @Override
        public void setViews(Object object) {

            Attachment uploadFile = (Attachment) object;

            if(uploadFile.getUploadedName().contains(".docx")|| uploadFile.getUploadedName().contains(".doc")){

                ivUploadFile.setImageResource(R.drawable.ic_docx);

            }else if (uploadFile.getUploadedName().contains(".pdf")){

                ivUploadFile.setImageResource(R.drawable.ic_pdf);

            } else {

                String bgPath = "";

                /****
                 * check the image location
                 */
                if (!uploadFile.getUploadedName().contains("/")){

                    bgPath = String.format("%s/%s/%s", applicationClass.getFilesDir().toString(),
                            context.getString(R.string.attachment_path), uploadFile.getUploadedName());

                }else {
                    bgPath = uploadFile.getUploadedName();

                }

                /****
                 *
                 */
                if (new File(bgPath).isFile()) {
//                    Bitmap bmp = FileHelper.decodeSampledBitmapFromFile(bgPath,500,600);
//                    ivUploadFile.setImageBitmap(bmp);

                    ivUploadFile.setImageBitmap(rotateImage(bgPath));
                }else {
                    //todo add display when image is not available
                }

            }

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.ivDeletedAttachments:

                    listener.onRecyclerItemClick(-1,getObject());
                    break;

                default:
                    super.onClick(view);
                    break;
            }
        }
    }

    private Bitmap rotateImage(String bgPath){
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        Bitmap bm = FileHelper.decodeSampledBitmapFromFileWithBounds(bgPath,500,600,bounds);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(bgPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);

        return rotatedBitmap;
    }
}
