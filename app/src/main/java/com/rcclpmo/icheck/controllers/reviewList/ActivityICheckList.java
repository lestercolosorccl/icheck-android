package com.rcclpmo.icheck.controllers.reviewList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.iCheckAPI;
import com.rcclpmo.icheck.bases.BaseActivity;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.controllers.addAction.ActivityCheckItem;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.PaginationScrollListener;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.FilterDataList;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.utilities.GSONUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ActivityICheckList extends BaseActivity implements
        RecyclerViewClickListener<Object>,View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    /****
     * views
     */
    private RecyclerView rvActions;
    private Button btnAddNewAction, btnFilterOption, btnGenerateReport;
    private TextView tvSelectArea, tvSelectProject, tvSelectClaimOwner, tvSelectDiscipline, tvSelectPriority, tvSelectStatus;
    private TextView tvLoader;
    private RelativeLayout rlLoader;
    private PopupWindow pwArea, pwInspectionType, pwProject;
    private SwipeRefreshLayout swipeWalkThruList;
    private Toolbar toolbar;
    private FloatingActionButton fabUp, fabDown;
//    private RelativeLayout rlDelete;

    /***
     * classes and objects
     */
    private AdapterICheckItem adapterICheckItem;
    private SyncDBTransaction dbTransaction;
    private RequestQueue requestQueue;
    private ApplicationClass applicationClass;
    LinearLayoutManager layoutManager;

    /****
     * variables
     */
    private List<ICheckItem> ICheckItemList;
    private List<FilterDataList> selectedFilterParamList;
    private List<Project> projectList;
    private List<Area> areaList;
    private List<Deck> deckList;
    private List<Discipline> disciplineList;
    private String toBeDeletedId;
    private String selectedRCLownerId;
    private ICheckItemData selectedFilterParams;
    private Area selectedArea;
    private Deck selectedDeck;
    private Discipline selectedDiscipline;
    private String selectedStatus;
    private String selectedPriority;
    private People selectedClaimOwner;
//    private People selectedPeople;
    private File reportGeneratedFile;
    private int actionLogsItemCount = 0;
    //    private Remark toBeDeletedRemark;
    private Project selectedProject;
    private boolean isUp = true;
    private String searchText= "";


    @Override
    protected void initData(Intent data) {
        ICheckItemList = new ArrayList<>();
        disciplineList = new ArrayList<>();
        projectList = new ArrayList<>();
        areaList = new ArrayList<>();
        deckList = new ArrayList<>();

        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();
        dbTransaction = new SyncDBTransaction(getApplicationContext());

        selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class,
                DatabaseConstants.KEY_PROJECT_ID, applicationClass.getShipId());

        loadLocalProject();
        loadLocalAreaList();
        loadLocalDeck();
        loadLocalProviderList();

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_review_list2;
    }


    @Override
    protected void initViews() {
        rvActions = (RecyclerView) findViewById(R.id.rvActions);
        btnAddNewAction = (Button) findViewById(R.id.btnAddNewAction);
        btnFilterOption = (Button) findViewById(R.id.btnOptions);
        btnGenerateReport = (Button) findViewById(R.id.btnGenerateReport);
        rlLoader = (RelativeLayout) findViewById(R.id.rlLoader);
        tvSelectArea = (TextView) findViewById(R.id.tvSelectArea);
        tvSelectPriority = (TextView) findViewById(R.id.tvSelectPriority);
        tvSelectStatus = (TextView) findViewById(R.id.tvSelectStatus);
        tvSelectProject = (TextView) findViewById(R.id.tvSelectProject);
        tvSelectDiscipline = (TextView) findViewById(R.id.tvSelectDiscipline);
        tvSelectClaimOwner = (TextView) findViewById(R.id.tvSelectClaimOwner);
        tvLoader = (TextView) findViewById(R.id.tvLoader);
        swipeWalkThruList = (SwipeRefreshLayout) findViewById(R.id.swipeWalkThruList);
        fabDown = (FloatingActionButton) findViewById(R.id.fabAdd);
        fabUp = (FloatingActionButton) findViewById(R.id.fabUp);

        toolbar =(Toolbar) findViewById(R.id.fragment_toolbar);
        initSearch();
        setSupportActionBar(toolbar);
//        rlDelete = (RelativeLayout) findViewById(R.id.rlDelete);

        initListeners();
        initRecyclerView();
        initRecyclerViewPaginationListener();
        initAppBar();
        setActionBar();
        setViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchView searchView = (SearchView)toolbar.getMenu().findItem(R.id.search).getActionView() ;
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)searchView.
                findViewById(android.support.v7.appcompat.R.id.search_src_text);

        theTextArea.setTextColor(Color.WHITE);

        initsearchListener(searchView);

        return super.onCreateOptionsMenu(menu);
    }

    private void initSearch(){
        toolbar.inflateMenu(R.menu.menu_search);

    }

    private void initsearchListener(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapterICheckItem.filter(newText);
                searchText = newText;
                return true;
            }
        });
    }

    private void initListeners(){
        fabUp.setOnClickListener(this);
        fabDown.setOnClickListener(this);
        btnFilterOption.setOnClickListener(this);
        btnAddNewAction.setOnClickListener(this);
        btnGenerateReport.setOnClickListener(this);
        tvSelectArea.setOnClickListener(this);
        tvSelectProject.setOnClickListener(this);
        tvSelectClaimOwner.setOnClickListener(this);
        tvSelectDiscipline.setOnClickListener(this);
        tvSelectPriority.setOnClickListener(this);
        tvSelectStatus.setOnClickListener(this);
        swipeWalkThruList.setOnRefreshListener(this);

    }

    private void setActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getString(R.string.dashboard_review_list));

    }

    private void initAppBar(){
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    fabUp.setImageResource(R.drawable.ic_arrow_downward_white);
                    isUp = false;

                } else if (verticalOffset == 0) {
                    fabUp.setImageResource(R.drawable.ic_arrow_up_white);
                    isUp = true;

                } else {
//                     Somewhere in between

                }
            }
        });

    }

    private void initRecyclerView(){
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        adapterICheckItem = new AdapterICheckItem(getApplicationContext(), ICheckItemList, this);
        rvActions.setLayoutManager(layoutManager);
        rvActions.setAdapter(adapterICheckItem);
        rvActions.setHasFixedSize(true);

    }

    private void setViews(){
        if (selectedProject!=null){
            tvSelectProject.setText(selectedProject.getProjectName());

        }

        btnGenerateReport.setVisibility(View.VISIBLE);
        btnAddNewAction.setText(getString(R.string.dashboard_add_new_action));
        btnFilterOption.setText(R.string.disposition_list_filter);

    }

    @Override
    public void onResume() {
        super.onResume();

        requestGetActions();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        requestQueue.cancelAll(APIRequestCode.TAG_GET_ACTION_LOGS);
        // TODO: 05/10/2017 add tag for request
    }

    private void editAction(String id){
        Intent intent = new Intent(getApplicationContext(), ActivityCheckItem.class);
        intent.putExtra(CommonConstants.KEY_REMARK_ID, id);
        startActivity(intent);

    }

    private void addAction(@Nullable String id){
        Intent intent = new Intent(getApplicationContext(), ActivityCheckItem.class);
        intent.putExtra(CommonConstants.KEY_IS_ADD_REMARK, true);
        startActivity(intent);
    }

    private void showLoader(boolean isShown, String loader){
        rlLoader.setVisibility(isShown? View.VISIBLE: View.GONE);
        tvLoader.setText(loader);
    }


    private void showOption(Bundle bundle){
        DialogFragmentOptionSelection dialog = DialogFragmentOptionSelection.createInstance(bundle);
        dialog.show(getSupportFragmentManager(),"create dialog");
    }

    private void goToGenerateReportDialog(Bundle data){
        DialogFragmentExportPDF dialog = DialogFragmentExportPDF.createInstance(data);
//        dialog.setTargetFragment(this, CommonConstants.GENERATE_REPORT_REQUEST_CODE);
        dialog.show(getSupportFragmentManager(),"create dialog");

    }


    private void initRecyclerViewPaginationListener(){
        rvActions.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                if (ICheckItemList.size() == actionLogsItemCount){
                    actionLogsItemCount = actionLogsItemCount +100;
                    requestGetActions();

                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onRecyclerItemClick(int position, Object object) {
        Log.v("lester-log-click", String.valueOf(object)+"-"+position);
        if(position == -99){
            handleSelectedStatus((String) object);
            actionLogsItemCount = 0;
            requestGetActions();
        }else if(position == -98){
            handleSelectedPriority((String) object);
            actionLogsItemCount = 0;
            requestGetActions();

        }else if (object instanceof ICheckItem){
            if (position != -1){
                editAction(((ICheckItem) object).getId());
            }
        }else if (object instanceof Area){
            handleSelectedArea((Area) object);
            actionLogsItemCount = 0;
            requestGetActions();

        }else if (object instanceof Discipline){

            handleSelectedProvider((Discipline) object);
            actionLogsItemCount = 0;
            requestGetActions();

        }else if (object instanceof People){

            handleSelectedCLaimOwner((People) object);
            actionLogsItemCount = 0;
            requestGetActions();

        }else if (object instanceof Project){
            handleSelectedProject((Project) object);
            requestGetActions();
        }
    }

    private void handleSelectedArea(Area area){
        if (area.getAreaId().equals(DatabaseConstants.ALL_VALUE)){
            selectedArea = null;
        }else {
            selectedArea = area;
        }

        tvSelectArea.setText(selectedArea !=null? selectedArea.getAreaName():getString(R.string.filter_all));

    }

    private void handleSelectedProvider(Discipline discipline){
        if (discipline.getDisciplineId().equals(DatabaseConstants.ALL_VALUE)){
            selectedDiscipline = null;
        }else {
            selectedDiscipline = discipline;
        }
        tvSelectDiscipline.setText(selectedDiscipline !=null? selectedDiscipline.getDiscipline():getString(R.string.filter_all));
    }

    private void handleSelectedStatus(String status){
        selectedStatus = status;
        tvSelectStatus.setText(selectedStatus !=null? selectedStatus:getString(R.string.filter_all));
        if(status.equals("All")){
        selectedStatus = "";
        }

    }

    private void handleSelectedPriority(String priority){
        selectedPriority = priority;
        tvSelectPriority.setText(selectedPriority !=null? selectedPriority:getString(R.string.filter_all));
        if(priority.equals("All")){
            selectedPriority = "";
        }

    }

    private void handleSelectedCLaimOwner(People people){
        if (people.getUserId().equals(DatabaseConstants.ALL_VALUE)){
            selectedClaimOwner = null;
        }else {
            selectedClaimOwner = people;
        }

        tvSelectClaimOwner.setText(selectedClaimOwner !=null? selectedClaimOwner.getName():getString(R.string.filter_all));

    }

    private void handleSelectedProject(Project project){
        selectedProject = project;
        applicationClass.saveShipId(selectedProject.getProjectId(),selectedProject.getProjectName());
//        applicationClass.saveProjectId(selectedProject.getShipId());
        //set the ship name
        tvSelectProject.setText(selectedProject.getProjectName());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnOptions:
                break;

            case R.id.btnAddNewAction:
                addAction(null);

                break;

            case R.id.tvSelectArea:

//                if (selectedProject!=null){
                    Bundle bundle = new Bundle();
                    bundle.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_AREA);
                    bundle.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_REVIEW_LIST);
//                    bundle.putString(CommonConstants.DATA_KEY_PARENT_ID, selectedProject.getShipId());
                    showOption(bundle);

//                }else {
//                    Toast.makeText(getApplicationContext(), "Please select Project",Toast.LENGTH_SHORT).show();
//
//                }
                break;

            case R.id.tvSelectProject:
                Bundle bundle2 = new Bundle();
                bundle2.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PROJECT);
                bundle2.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_REVIEW_LIST);

                showOption(bundle2);
                break;

            case R.id.tvSelectClaimOwner:
                    Bundle bundle3 = new Bundle();
                    bundle3.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PEOPLE);
                    bundle3.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_REVIEW_LIST);
//                    bundle3.putString(CommonConstants.DATA_KEY_PARENT_ID, selectedProject.getShipId());
                    showOption(bundle3);
                break;



            case R.id.tvSelectDiscipline:
                Bundle bundle4 = new Bundle();
                bundle4.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_DISCIPLINE);
                bundle4.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_REVIEW_LIST);

                showOption(bundle4);

                break;

            case R.id.tvSelectStatus:
                Bundle bundle5 = new Bundle();
                bundle5.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_STATUS_FILTER);
                bundle5.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle5.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION_STATUS);
                showOption(bundle5);
                break;

            case R.id.tvSelectPriority:
                Bundle bundle6 = new Bundle();
                bundle6.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PRIORITY_FILTER);
                bundle6.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle6.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION_PRIORITY);
                showOption(bundle6);
                break;

            case R.id.btnGenerateReport:
                // TODO: 5/31/2017 check if remark list is empty
                if (ICheckItemList !=null){
                    if (!ICheckItemList.isEmpty()){
                        Bundle data = new Bundle();
                        data.putParcelableArrayList(CommonConstants.KEY_FILTER_DATA_LIST,
                                (ArrayList<? extends Parcelable>) selectedFilterParamList);
                        data.putParcelable(CommonConstants.KEY_FILTER_PARAM, selectedFilterParams);
                        data.putString(CommonConstants.KEY_SEARCH_TEXT, searchText);
                        goToGenerateReportDialog(data);
                    }
                }
                break;

            case R.id.fabAdd:
                addAction(null);

                break;


            case R.id.fabUp:

                if (isUp){
                    AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.app_bar);
                    appBarLayout.setExpanded(false, true);
                }else {

                    AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.app_bar);
                    appBarLayout.setExpanded(true, true);
                }
                break;


            default:
                break;
        }

    }


    @Override
    public void onRefresh() {

        actionLogsItemCount = 0;
        requestGetActions();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode){

            }
        }


    }

    @Override
    protected void handleRefreshToken() {

    }

    @Override
    public void handleCommonAPI(int requestCode) {

    }

    @Override
    protected void handleNotification(String message) {
        Snackbar snackbar = Snackbar.make(rlLoader,message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_300));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.amber_500));
        snackbar.show();
    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        switch (requestCode){

            case APIRequestCode.CODE_GET_GUARANTEE_ITEMS:
                //clear local items at first load
                if (actionLogsItemCount ==0){
                    deleteDeletedActionList();
                }
                handleGetActionLogs(object);

                showLoader(false, "");
                break;

//            case APIRequestCode.CODE_DELETE_REMARK:
//                dbTransaction.deleteRealmObject(ICheckItem.class, toBeDeletedId,
//                        DatabaseConstants.KEY_ID);
//                showLoader(false);
//                break;

        }

    }

    @Override
    public void onFailed(int requestCode, String message) {
//        super.onFailed(requestCode, message);

        switch (requestCode){

            case APIRequestCode.CODE_GET_GUARANTEE_ITEMS:
                showErrorMessage(message);
                loadLocalActionLogList();
                showLoader(false,"");

                break;

//            case APIRequestCode.CODE_DELETE_REMARK:
//                showLoader(false);
//                handleOfflineDeleteRemark();
//                showErrorMessage(message);
//                break;

        }

    }

    private void showErrorMessage(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    private void requestGetActions(){

        Log.i("REVIEW_LIST_TAG", "get remarks");
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_ACTION_LIST);
        Request request = iCheckAPI.getICheckItems(APIRequestCode.CODE_GET_GUARANTEE_ITEMS,
                getRemarkRequestParameters(),this);

        request.setTag(APIRequestCode.TAG_GET_ACTION_LOGS);
        requestQueue.add(request);

    }

    private ICheckItemData getRemarkRequestParameters(){
        // TODO: 20/12/2017 remove parameters
        selectedFilterParams = new ICheckItemData();
        selectedFilterParams.setLength("100");
        selectedFilterParams.setStart(String.valueOf(actionLogsItemCount));
        selectedFilterParams.setProjectId(selectedProject!=null? selectedProject.getProjectId():"");
        selectedFilterParams.setAreaId(selectedArea !=null? selectedArea.getAreaId():"");
        selectedFilterParams.setDisciplineId(selectedDiscipline !=null? selectedDiscipline.getDisciplineId():"");
        selectedFilterParams.setFilter(getSelectedFilter().size()!=0? GSONUtility.convertToJSON(getSelectedFilter()):"");
        selectedFilterParams.setClaimOwner(selectedClaimOwner!=null? selectedClaimOwner.getUserId():"");
        selectedFilterParams.setStatus(selectedStatus!=null? selectedStatus:"");
        selectedFilterParams.setPriority(selectedPriority!=null? selectedPriority:"");
//        selectedFilterParams.setSort(getSelectedSort().size()!=0? GSONUtility.convertToJSON(getSelectedSort()):"");
        selectedFilterParams.setSort("");//temp the sort to empty

        return selectedFilterParams;
    }

    private List<FilterDataList> getSelectedFilter(){
        selectedFilterParamList = new ArrayList<>();

        //todo add filter if needed
        return selectedFilterParamList;
    }

    private void handleGetActionLogs(Object object){
        List<ICheckItem> remarks = (List<ICheckItem>) object;
        dbTransaction.add(remarks);

        adapterICheckItem.setActionCopy(selectedFilterParams,selectedFilterParamList);

        if (remarks.size()!=0){
            for(ICheckItem item: remarks){
                ICheckItemList.add(item);
                adapterICheckItem.notifyItemChanged(ICheckItemList.size()-1);

            }

        }

        swipeWalkThruList.setRefreshing(false);

    }

    private void loadLocalActionLogList(){
        ICheckItemList.clear();
        ICheckItemList.addAll(dbTransaction.getActionsWithFilter(ICheckItem.class,selectedFilterParams,selectedFilterParamList));

        adapterICheckItem.setActionCopy(selectedFilterParams,selectedFilterParamList);
        adapterICheckItem.notifyDataSetChanged();
        swipeWalkThruList.setRefreshing(false);
    }

    private void deleteDeletedActionList(){
        List<ICheckItem> oldWalkThruRemarks = new ArrayList<>();
        oldWalkThruRemarks.addAll(dbTransaction.getActionsWithFilter(ICheckItem.class,selectedFilterParams,selectedFilterParamList));
        for (ICheckItem item: oldWalkThruRemarks){
            if (item.isSync()) {
                dbTransaction.delete(ICheckItem.class, item);
            }
        }
        ICheckItemList.clear();
        adapterICheckItem.notifyDataSetChanged();


    }

    private void loadLocalProject(){
        projectList.addAll(dbTransaction.getAll(Project.class));
    }

    private Project createAllProjectSelectionSelection(){

        Project item = new Project();
        item.setProjectId(DatabaseConstants.ALL_VALUE);
        item.setProjectName(getString(R.string.filter_all));

        return item;
    }

    private void loadLocalAreaList(){
        areaList.clear();
        areaList.addAll(dbTransaction.getAll(Area.class));
//        areaList.add(0,createAllVenueSelection());
    }

    private Area createAllVenueSelection(){

        Area venue = new Area();
        venue.setAreaId(DatabaseConstants.ALL_VALUE);
        venue.setAreaName(getString(R.string.filter_all));

        return venue;
    }

    private void loadLocalDeck(){
        deckList.clear();
        deckList.addAll(dbTransaction.getAll(Deck.class));
//        deckList.add(0,createAllDeckSelection());
    }

    private Deck createAllDeckSelection(){

        Deck deck = new Deck();
        deck.setId(DatabaseConstants.ALL_VALUE);
        deck.setDeckName(getString(R.string.filter_all));
        dbTransaction.add(deck);

        return deck;
    }

    private void loadLocalProviderList(){
        disciplineList.clear();
        disciplineList.addAll(dbTransaction.getAll(Discipline.class));
//        disciplineList.add(0, createAllProviderSelection());

    }

    private Discipline createAllProviderSelection(){

        Discipline discipline = new Discipline();
        discipline.setDisciplineId(DatabaseConstants.ALL_VALUE);
        discipline.setDiscipline(getString(R.string.filter_all));

        return discipline;
    }


//    private void deleteRemarkByRemarkId(String remarkId){
//
//        Log.i("REVIEW_LIST_TAG", "delete remarks");
//        showLoader(true);
//        requestQueue.add(deleteRemarksById(remarkId, CommonConstants.REMARK_TYPE_WALK_THRU));
//    }
//
//    private Request deleteRemarksById(String remarkId, String type){
//
//        Request request = CommonAPI.deleteRemarksById(APIRequestCode.CODE_DELETE_REMARK, remarkId, type, this);
//        request.setTag(APIRequestCode.TAG_DELETE_REMARK);
//
//        return request;
//    }
//
//    private void handleOfflineDeleteRemark(){
//        dbTransaction.deleteRemrk(Remark.class, toBeDeletedId, null);
//    }

}



