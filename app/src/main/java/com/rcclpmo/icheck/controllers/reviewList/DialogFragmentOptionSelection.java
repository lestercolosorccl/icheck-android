package com.rcclpmo.icheck.controllers.reviewList;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseDialogFragment;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.controllers.addAction.ActivityCheckItem;
import com.rcclpmo.icheck.controllers.dashboard.ActivityDashboard;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.Supplier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DialogFragmentOptionSelection extends BaseDialogFragment implements RecyclerViewClickListener<Object> {

    private RecyclerView rvItems;
    private AdapterOption adapterOption;
    private Toolbar toolbar;

    private int optionType;
    private int activity;
    private String parentId;

    private List<Project> projectList;
    private List<Deck> deckList;
    private List<Area> areaList;
    private List<Discipline> disciplineList;
    private List<People> peopleList;
    private List<Supplier> supplierList;
    private List<String> statusList, remarktypeList, impactList, workByList, priorityList;

    private boolean isWithAllSelection = true;

    private SyncDBTransaction dbTransaction;

    public static DialogFragmentOptionSelection createInstance(@Nullable Bundle data){

        DialogFragmentOptionSelection fragment = new DialogFragmentOptionSelection();
        fragment.setArguments(data);

        return fragment;
    }


    @Override
    public void initMain() {
        setLayoutId(R.layout.dialog_fragment_filter_option);

    }

    @Override
    public void initData(Bundle bundle) {

        projectList = new ArrayList<>();
        areaList = new ArrayList<>();
        deckList = new ArrayList<>();
        disciplineList = new ArrayList<>();
        peopleList = new ArrayList<>();
        supplierList = new ArrayList<>();
        statusList =new ArrayList<>();
        priorityList = new ArrayList<>();
        impactList = new ArrayList<>();
        workByList = new ArrayList<>();

        dbTransaction = new SyncDBTransaction(getActivity());

        if (bundle!=null){

            optionType = bundle.getInt(CommonConstants.DATA_KEY_OPTION_TYPE,CommonConstants.OPTION_TYPE_PROJECT);
            activity = bundle.getInt(CommonConstants.DATA_KEY_ACTIVITY);
            isWithAllSelection = bundle.getBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION,true);
            parentId = bundle.getString(CommonConstants.DATA_KEY_PARENT_ID);
            Log.v("lester-log", String.valueOf(bundle));
        }

    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected boolean isCustomDialog() {
        return true;
    }

    @Override
    protected void handleCommonAPI(int requestCode) {

    }

    @Override
    public void initViews(View view, Bundle savedInstanceState) {
/***parent view to animate*/

        View viewToAnimate = view.findViewById(R.id.rlParent);
        setViewToAnimate(viewToAnimate);
        setAnimationEnter(R.anim.anim_slide_up_enter);
        setAnimationExit(R.anim.anim_slide_down_exit);

        rvItems = view.findViewById(R.id.rvItems);
        toolbar = view.findViewById(R.id.toolbar);

        switch (optionType){

            case CommonConstants.OPTION_TYPE_PROJECT:
                projectList.addAll(dbTransaction.getAll(Project.class));
                toolbar.setTitle(getString(R.string.add_action_select_project));
                initRecyclerView(projectList);
                break;

            case CommonConstants.OPTION_TYPE_AREA:
                areaList.addAll(dbTransaction.getAll(Area.class));
//                areaList.addAll(dbTransaction.getDynamicRealmListObject(Area.class, DatabaseConstants.KEY_PROJECT_ID, parentId));
                toolbar.setTitle(getString(R.string.add_action_select_area));
                if (isWithAllSelection){
                    areaList.add(0, createAllAreaSelection());

                }
                initRecyclerView(areaList);
                break;

            case CommonConstants.OPTION_TYPE_DECK:
                deckList.addAll(dbTransaction.getAll(Deck.class));
//                deckList.addAll(dbTransaction.getDynamicRealmListObject(Deck.class, DatabaseConstants.KEY_SHIP_ID, parentId));
                toolbar.setTitle(getString(R.string.add_safety_item_select_deck));
                if (isWithAllSelection){
                    deckList.add(0,createAllDeckSelection());

                }
                initRecyclerView(deckList);
                break;

            case CommonConstants.OPTION_TYPE_DISCIPLINE:
                disciplineList.addAll(dbTransaction.getAll(Discipline.class));
                toolbar.setTitle(getString(R.string.add_action_select_discipline));
                if (isWithAllSelection){
                    disciplineList.add(0, createAllDisciplineSelection());

                }
                initRecyclerView(disciplineList);
                break;

            case CommonConstants.OPTION_TYPE_STATUS:
                statusList = Arrays.asList(getResources().getStringArray(R.array.status));
                toolbar.setTitle(getString(R.string.add_safety_item_select_status));
                initRecyclerView(statusList);
                break;

            case CommonConstants.OPTION_TYPE_STATUS_FILTER:
                statusList = Arrays.asList(getResources().getStringArray(R.array.status_filter));
                toolbar.setTitle(getString(R.string.add_safety_item_select_status));
                initRecyclerView(statusList);
                break;

            case CommonConstants.OPTION_TYPE_IMPACT:
                statusList = Arrays.asList(getResources().getStringArray(R.array.impact));
                toolbar.setTitle(getString(R.string.label_select_impact));
                initRecyclerView(statusList);
                break;

            case CommonConstants.OPTION_TYPE_PRIORITY:
                priorityList = Arrays.asList(getResources().getStringArray(R.array.priority));
                toolbar.setTitle(getString(R.string.label_select_priority));
                initRecyclerView(priorityList);
                break;
            case CommonConstants.OPTION_TYPE_PRIORITY_FILTER:
                priorityList = Arrays.asList(getResources().getStringArray(R.array.priority_filter));
                toolbar.setTitle(getString(R.string.label_select_priority));
                initRecyclerView(priorityList);
                break;

            case CommonConstants.OPTION_TYPE_WORK_BY:
                statusList = Arrays.asList(getResources().getStringArray(R.array.work_by));
                toolbar.setTitle(getString(R.string.label_select_work_by));
                initRecyclerView(statusList);
                break;

            case CommonConstants.OPTION_TYPE_REMARK_TYPE:
                remarktypeList = Arrays.asList(getResources().getStringArray(R.array.remark_type));
                toolbar.setTitle(getString(R.string.label_select_remarktype));
                initRecyclerView(remarktypeList);
                break;

            case CommonConstants.OPTION_TYPE_AREA_MANAGER:
                peopleList.addAll(dbTransaction.getAll(People.class));
                toolbar.setTitle(getString(R.string.label_select_area_manager));
                if (isWithAllSelection){
                    peopleList.add(0, createAllPeopleSelection());

                }
                initRecyclerView(peopleList);
                break;

            case CommonConstants.OPTION_TYPE_PEOPLE:
                peopleList.addAll(dbTransaction.getAll(People.class));
                toolbar.setTitle(getString(R.string.reviewlist_remark_owner));
                if (isWithAllSelection){
                    peopleList.add(0, createAllPeopleSelection());
                }
                initRecyclerView(peopleList);
                break;

            case CommonConstants.OPTION_TYPE_SUPPLIER:
                supplierList.addAll(dbTransaction.getAll(Supplier.class));
                toolbar.setTitle(getString(R.string.label_select_contractor));
                initRecyclerView(supplierList);
                break;

            default:break;
        }

        initToolbarListener();

    }

    private Deck createAllDeckSelection(){

        Deck deck = new Deck();
        deck.setId(DatabaseConstants.ALL_VALUE);
        deck.setDeckName(DatabaseConstants.ALL);
//        dbTransaction.add(deck);

        return deck;
    }

    private Discipline createAllMeetingTypeSelection(){

        Discipline item = new Discipline();
        item.setDisciplineId(DatabaseConstants.ALL_VALUE);
        item.setDiscipline(DatabaseConstants.ALL);
//        dbTransaction.add(item);

        return item;
    }


    private Discipline createAllDisciplineSelection(){

        Discipline item = new Discipline();
        item.setDisciplineId(DatabaseConstants.ALL_VALUE);
        item.setDiscipline(DatabaseConstants.ALL);
//        dbTransaction.add(item);

        return item;
    }

    private People createAllPeopleSelection(){

        People item = new People();
        item.setUserId(DatabaseConstants.ALL_VALUE);
        item.setName(DatabaseConstants.ALL);
//        dbTransaction.add(item);
        return item;
    }

    private Area createAllAreaSelection(){

        Area area = new Area();
        area.setAreaId(DatabaseConstants.ALL_VALUE);
        area.setAreaName(DatabaseConstants.ALL);

        return area;
    }

    private Deck createAGeneralDeckSelection(){

        Deck deck = new Deck();
        deck.setId(CommonConstants.GENERAL_ID);
        deck.setDeckName(CommonConstants.GENERAL_VALUE);
        dbTransaction.add(deck);

        return deck;
    }

    private Area createGeneralAreaSelection(){

        Area area = new Area();
        area.setAreaId(CommonConstants.GENERAL_ID);
        area.setAreaName(CommonConstants.GENERAL_VALUE);

        return area;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        toolbar.inflateMenu(R.menu.menu_search);
        toolbar.setNavigationIcon(R.drawable.ic_back_button_white);
        SearchView searchView = (SearchView)toolbar.getMenu().findItem(R.id.search).getActionView() ;
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)searchView.
                findViewById(android.support.v7.appcompat.R.id.search_src_text);

        theTextArea.setTextColor(Color.WHITE);

        search(searchView);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void initToolbarListener(){
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideParent();

            }
        });
    }

    private void initRecyclerView(List list){

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapterOption = new AdapterOption(getActivity(), list, this);
        rvItems.setLayoutManager(layoutManager);
        rvItems.setAdapter(adapterOption);
        rvItems.setHasFixedSize(true);
    }

    @Override
    public void onRecyclerItemClick(int position, Object object) {
        Log.v("lester-log-click", position+"-"+object+"-"+activity);

        switch (activity){
            case CommonConstants.ACTIVITY_ADD_ACTION:
                ActivityCheckItem activity = (ActivityCheckItem) getActivity();
                activity.onRecyclerItemClick(optionType,object);//temp position
                break;
            case CommonConstants.ACTIVITY_REVIEW_LIST:
                ActivityICheckList activity2 = (ActivityICheckList) getActivity();
                activity2.onRecyclerItemClick(0,object);//temp position
                break;
            case CommonConstants.ACTIVITY_ADD_ACTION_STATUS:
                ActivityICheckList activity3 = (ActivityICheckList) getActivity();
                activity3.onRecyclerItemClick(-99,object);//temp position
                break;
            case CommonConstants.ACTIVITY_ADD_ACTION_PRIORITY:
                ActivityICheckList activity4 = (ActivityICheckList) getActivity();
                activity4.onRecyclerItemClick(-98,object);//temp position
                break;
            case CommonConstants.ACTIVITY_DASHBOARD:
                ActivityDashboard activity5 = (ActivityDashboard) getActivity();
                activity5.onRecyclerItemClick(0,object);//temp position
                break;
        }

        hideParent();

    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapterOption.filter(newText);
                return true;
            }
        });
    }

}
