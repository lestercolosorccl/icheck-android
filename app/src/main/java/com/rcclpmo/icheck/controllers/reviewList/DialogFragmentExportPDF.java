package com.rcclpmo.icheck.controllers.reviewList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.iCheckAPI;
import com.rcclpmo.icheck.bases.BaseDialogFragment;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.helpers.DateHelper;
import com.rcclpmo.icheck.helpers.DecodeFileHelper;
import com.rcclpmo.icheck.helpers.ImageHelper;
import com.rcclpmo.icheck.helpers.PDFFooterHelper;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.FilterDataList;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.User;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 05/01/2018.
 */

public class DialogFragmentExportPDF extends BaseDialogFragment {

    /****
     *CONSTANTS
     */
    private final static int STATUS_GENERATE = 100;
    private final static int STATUS_GENERATING = 200;
    private final static int STATUS_PREVIEW = 300;

    /****
     * VIEWS
     */
    private EditText etPDFTitle;
    private Button btnGenerateReport;
    private TextView tvCancel;
    private TextView tvReportPath;
    private TextView tvDownloadingImages;
    private CheckBox cbAttachPhoto;
    /****
     * CLASSES
     */
    private ApplicationClass applicationClass;
    private SyncDBTransaction dbTransaction;
    private RequestQueue requestQueue;

    /****
     * DATA TYPE AND OBJECT MODELS
     */
    private List<ICheckItem> ICheckItemList;
    private List<ICheckItem> ICheckItemCopy;
    private List<FilterDataList> filterDataLists;
    private List<Attachment> attachmentList;
    private ICheckItemData selectedFilterParams;
    private File reportGeneratedFile;
    private String fileName = "";
    private User user;
    private int attachmentPosition = 0;
    private String searchText = "";


    public static DialogFragmentExportPDF createInstance(@Nullable Bundle data){

        DialogFragmentExportPDF fragment = new DialogFragmentExportPDF();
        fragment.setArguments(data);

        return fragment;
    }

    @Override
    public void initMain() {
        setLayoutId(R.layout.dialog_fragment_export);

    }

    @Override
    public void initData(Bundle bundle) {

        dbTransaction = new SyncDBTransaction(getActivity());
        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();

        ICheckItemList = new ArrayList<>();
        ICheckItemCopy = new ArrayList<>();
        filterDataLists = new ArrayList<>();
        selectedFilterParams = new ICheckItemData();
        attachmentList = new ArrayList<>();

        if (bundle != null){
            filterDataLists = bundle.getParcelableArrayList(CommonConstants.KEY_FILTER_DATA_LIST);
            selectedFilterParams = bundle.getParcelable(CommonConstants.KEY_FILTER_PARAM);
            searchText = bundle.getString(CommonConstants.KEY_SEARCH_TEXT);

            ICheckItemList.addAll(dbTransaction.getActionsWithFilter(ICheckItem.class,selectedFilterParams,filterDataLists));
            ICheckItemCopy.addAll(ICheckItemList);

            if (!searchText.equals("")){
                filter(searchText);

            }

        }

        user = (User) dbTransaction.getFirstObject(User.class);
    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected boolean isCustomDialog() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        requestQueue.cancelAll(APIRequestCode.REQUEST_TAG);
        requestQueue.cancelAll(APIRequestCode.TAG_DOWNLOAD_IMAGE);

    }

    @Override
    public void initViews(View view, Bundle savedInstanceState) {

        View viewToAnimate = view.findViewById(R.id.rlParent);
        setViewToAnimate(viewToAnimate);
        setAnimationEnter(R.anim.anim_slide_up_enter);
        setAnimationExit(R.anim.anim_slide_down_exit);

        etPDFTitle = (EditText) view.findViewById(R.id.etPDFTitle);
        btnGenerateReport = (Button) view.findViewById(R.id.btnGenerateReport);
        tvCancel = (TextView) view.findViewById(R.id.tvCancel);
        tvReportPath = (TextView) view.findViewById(R.id.tvReportPath);
        tvDownloadingImages = (TextView) view.findViewById(R.id.tvDownloadingImages);
        cbAttachPhoto = (CheckBox) view.findViewById(R.id.cbAttachPhoto);

        setListeners();
    }

    private void setListeners(){

        tvCancel.setOnClickListener(this);
        btnGenerateReport.setOnClickListener(this);
        cbAttachPhoto.setOnClickListener(this);

    }

    private void viewPdf(File myFile){
        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(ImageHelper.getFileDirectoryDirectory(getActivity(),myFile), "application/pdf");
//        Uri u = Uri.fromFile(myFile);
//        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        hideParent();
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){

            case R.id.btnGenerateReport:

                if (btnGenerateReport.getText().toString().equals(getString(R.string.export_preview))){

                    viewPdf(reportGeneratedFile);

                }else {
                    if (!etPDFTitle.getText().toString().equals("")){

                        if (!cbAttachPhoto.isChecked()){
                            startGeneratingPDFReport();

                        }else {
                            tvDownloadingImages.setVisibility(View.VISIBLE);
                            toggleGenerateReportButton(STATUS_GENERATING);
                            requestGetFileByIdModuleType();
                        }


                    }else {
                        Toast.makeText(getActivity(),getString
                                (R.string.export_enter_pdf_title),Toast.LENGTH_SHORT).show();
                    }

                }

                break;

            case R.id.cbAttachPhoto:
                break;

            default:
                hideParent();
        }
    }

    public void filter(String text) {

        ICheckItemList.clear();
        if(text.isEmpty()){
            ICheckItemList.addAll(ICheckItemCopy);
        } else{
            text = text.toLowerCase();

            for(ICheckItem item: ICheckItemCopy){


                if (item.getArea()!=null){
                    if (item.getArea().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getPriority()!=null){
                    if (item.getPriority().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getDeck()!=null){
                    if (item.getDeck().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getDiscipline()!=null){
                    if (item.getDiscipline().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getActionOwner()!=null){
                    if (item.getActionOwner().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

//                if (item.getMakerModelNo()!=null){
//                    if (item.getMakerModelNo().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//                if (item.getMakerPartname()!=null){
//                    if (item.getMakerPartname().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//
//                if (item.getMakerPartNo()!=null){
//                    if (item.getMakerPartNo().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//
//                if (item.getClaimOwnerName()!=null){
//                    if (item.getClaimOwnerName().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//                if (item.getDateOfClaim()!=null){
//                    if (item.getDateOfClaim().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//
//                if (item.getDescriptionOfDefect()!=null){
//                    if (item.getDescriptionOfDefect().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//
//                if (item.getRequestedAction()!=null){
//                    if (item.getRequestedAction().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }
//
//
//                if (item.getWorkBy()!=null){
//                    if (item.getWorkBy().toLowerCase().contains(text)){
//
//                        ICheckItemList.add(item);
//                        continue;
//                    }
//                }

                if (item.getComment()!=null){
                    if (item.getComment().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

            }

        }

    }


    private void startGeneratingPDFReport(){


        toggleGenerateReportButton(STATUS_GENERATING);
        Log.i("PDF_GENERATOR","generating...");
        fileName = etPDFTitle.getText().toString();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    generatePDF();
                } catch (FileNotFoundException e) {
                    toggleGenerateReportButton(STATUS_GENERATE);
                    e.printStackTrace();
                } catch (DocumentException e) {
                    toggleGenerateReportButton(STATUS_GENERATE);
                    e.printStackTrace();
                }
            }
        }, 200);


    }

    private void toggleGenerateReportButton(int toogleType){
        switch (toogleType){
            case STATUS_GENERATING:
                btnGenerateReport.setText(getString(R.string.export_generating));
                break;
            case STATUS_PREVIEW:
                btnGenerateReport.setText(getString(R.string.export_preview));
                break;
            default:
                btnGenerateReport.setText(getString(R.string.export_generate));
                break;
        }
    }

    private void showReportPath(){

        tvReportPath.setText(String.format("%s %s",
                getString(R.string.export_report_path),reportGeneratedFile.getPath().toString()));
        tvReportPath.setVisibility(View.VISIBLE);
    }

    private void generatePDF() throws FileNotFoundException, DocumentException {
        File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), getString(R.string.export_report_generated_path));
        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i("PDF_GENERATOR", "Pdf Directory created");
        }

        reportGeneratedFile = new File(pdfFolder + File.separator + fileName + ".pdf");

        OutputStream outputStream = new FileOutputStream(reportGeneratedFile);
        Document document = new Document(PageSize.A4.rotate(),20,20,60,60);
        document.setMarginMirroring(false);
//        PdfWriter.getInstance(document, outputStream);
        PdfWriter writer = PdfWriter.getInstance(document,outputStream);
        String reporter = String.format("%s %s %s (%s)",getString(R.string.export_issued_by),
                user.getFirstName(), user.getLastName(), DateHelper.getCurrentFormattedDateForPDF());
        writer.setPageEvent(new PDFFooterHelper(getActivity(),fileName,reporter,getString(R.string.company_name)));

        //create pdf
        document.open();

        // a table with 10 columns
        float[] columnWidths = {3, 10, 12, 10, 7, 7, 7 , 7, 6,6,10,7,10};
        PdfPTable table = new PdfPTable(columnWidths);

        table.setWidthPercentage(100);
        table.setSpacingBefore(0f);
        table.setSpacingAfter(0f);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);


        //font for header
        Font bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
        Font normal = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);

        // the cell object for header
        PdfPCell cellHeader = new PdfPCell(new Phrase(getString(R.string.add_action_due_date)));
//        cellHeader.setUseBorderPadding(true);
//        cellHeader.setUseVariableBorders(true);
        // set header row
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_no),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_project),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_attachments),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_remarks),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_area),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_deck),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_discipline),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_remarktype),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_priority),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_status),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_remarkowner),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_duedate),bold));
        table.addCell(cellHeader);
        cellHeader.setPhrase(new Phrase(getString(R.string.export_header_comments),bold));
        table.addCell(cellHeader);

        // set the first row as the headers
        table.setHeaderRows(1);
        int x = 1;
        for (ICheckItem item: ICheckItemList){


            PdfPCell cell = new PdfPCell(new Phrase(""));

            if (cbAttachPhoto.isChecked()){

                //get files of current remark
                List<Attachment> uploadFileList = new ArrayList<>();
                uploadFileList.addAll(dbTransaction.getUploadFileList(Attachment.class,
                        item.getId()));

                if (uploadFileList!=null){
                    if (!uploadFileList.isEmpty()){

                        for (Attachment uploadFile: uploadFileList){

                            if(!uploadFile.getUploadedName().contains(CommonConstants.DOC_FILE_TYPE)&&
                                    !uploadFile.getUploadedName().contains(CommonConstants.DOCX_FILE_TYPE)
                                    &&!uploadFile.getUploadedName().contains(CommonConstants.PDF_FILE_TYPE)){

                                String bgPath = "";
                                try {

                                    if (!uploadFile.getUploadedName().contains("/")){

                                        bgPath = String.format("%s/%s/%s", applicationClass.getFilesDir().toString(),
                                                getString(R.string.attachment_path), uploadFile.getUploadedName());

                                    }else {
                                        bgPath = uploadFile.getUploadedName();

                                    }

                                    Bitmap bmp = DecodeFileHelper.decodeSampledBitmapFromFile(bgPath,300,300);
//                            Bitmap bmp = BitmapFactory.decodeStream(ims);
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    Image image = Image.getInstance(stream.toByteArray());
                                    cell.addElement(image);
                                    cell.addElement(new Paragraph("\n"));
                                } catch (IOException e) {
                                    cell.addElement(new Paragraph(""));
                                    e.printStackTrace();
                                }

                            }

                        }


                    }else {
                        cell.addElement(new Paragraph(""));
                    }
                }else {

                    cell.addElement(new Paragraph(""));
                }
            }else {
                cell.addElement(new Paragraph(""));

            }


            String projectname  = "";
            String areaname     = "";
            String deckname     = "";
            String discipline   = "";
            String actionowner  = "";

            Project selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class, DatabaseConstants.KEY_PROJECT_ID, item.getProjectId());

            if(selectedProject!=null){
                projectname = selectedProject.getProjectName();
            }


            //temp add necessary table cell
            table.addCell(new Phrase(String.valueOf(x),normal));
            table.addCell(new Phrase(projectname,normal));
            table.addCell(cell);
            table.addCell(new Phrase(item.getRemark(),normal));
            table.addCell(new Phrase(item.getArea(),normal));
            table.addCell(new Phrase(item.getDeck(),normal));
            table.addCell(new Phrase(item.getDiscipline(),normal));
            table.addCell(new Phrase(item.getRemarkType(),normal));
            table.addCell(new Phrase(item.getPriority(),normal));
            table.addCell(new Phrase(item.getStatus(),normal));
            table.addCell(new Phrase(item.getActionOwner(),normal));
            table.addCell(new Phrase(item.getDueDate(),normal));
            table.addCell(new Phrase(item.getComment(),normal));

            x = x+1;

        }

        document.add(table);

        document.close();

        toggleGenerateReportButton(STATUS_PREVIEW);
        showReportPath();

    }


    @Override
    protected void handleCommonAPI(int requestCode) {

        switch (requestCode){

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                attachmentPosition = attachmentPosition+1;
//                showLoader(false);
                requestImageAttachment(attachmentPosition);
                break;


            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                attachmentPosition = attachmentPosition+1;
//                showLoader(false);
                requestImageAttachment(attachmentPosition);
                break;

            default:break;
        }

    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        switch (requestCode){

            case APIRequestCode.CODE_GET_ATTACHMENTS:
                handleGetFileByIdModuleType(object);
                loadLocalUploadFile();
                /****
                 * request the image attachment 1x1
                 */
                attachmentPosition = 0;
                requestImageAttachment(attachmentPosition);
                break;

        }
    }

    @Override
    public void onFailed(int requestCode, String message) {
        super.onFailed(requestCode, message);

        switch (requestCode){

            case APIRequestCode.CODE_GET_ATTACHMENTS:
                loadLocalUploadFile();
                /****
                 * request the image attachment 1x1
                 */
                attachmentPosition = 0;
                requestImageAttachment(attachmentPosition);
                break;

        }
    }

    private String getRemarkIds(){
        ArrayList<String> mList = new ArrayList<>();
        int pos = 0;

        for(ICheckItem remark: ICheckItemList){
            mList.add(remark.getId());
            pos = pos+1;
        }
        JSONArray array = new JSONArray(mList);

        return array.toString();
    }

    private void requestGetFileByIdModuleType(){
//        showLoader(true);
        Request request = iCheckAPI.getAttachments(APIRequestCode.CODE_GET_ATTACHMENTS,getRemarkIds(), this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void handleGetFileByIdModuleType(Object object){
        List<Attachment> list = (List<Attachment>) object;
        dbTransaction.add(list);
    }

    private void loadLocalUploadFile(){
        attachmentList.clear();
        attachmentList.addAll(dbTransaction.getUploadFileList(Attachment.class,
                getRemarkIds()));

    }

    /***request for the attachment item 1x1*/
    private void requestImageAttachment(int position){

        if (position+1 <= attachmentList.size()){

            Attachment item = attachmentList.get(position);

            String bgPath = String.format("%s/%s", ImageHelper.getAttachmentPath(getActivity()),item.getUploadedName());
            if (!new File(bgPath).isFile()){
//                showLoader(true);
                if(bgPath.contains(CommonConstants.DOC_FILE_TYPE)|| bgPath.contains(CommonConstants.DOCX_FILE_TYPE)
                        ||bgPath.contains(CommonConstants.PDF_FILE_TYPE)){
                    //todo don't download files cannot be show to pdf

                    attachmentPosition = attachmentPosition +1;
                    requestImageAttachment(attachmentPosition);
//                    requestQueue.add(downloadDocFile(item.getUploadedName()));

                }else {
                    requestQueue.add(downloadImage(item.getUploadedName()));

                }

            }else {

                attachmentPosition = attachmentPosition +1;
                requestImageAttachment(attachmentPosition);
            }

        }else {


            tvDownloadingImages.setVisibility(View.GONE);
            startGeneratingPDFReport();
        }

    }
}

