package com.rcclpmo.icheck.controllers.dashboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.BulkAPI;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.bases.BaseDialogFragment;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.helpers.BulkHelper;
import com.rcclpmo.icheck.helpers.ImageHelper;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.BulkAction;
import com.rcclpmo.icheck.models.BulkModel;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.UploadedAttachment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DialogFragmentSync extends BaseDialogFragment {

    /****
     * CLASSES
     */
    private ApplicationClass applicationClass;
    private RequestQueue requestQueue;
    private SyncDBTransaction dbTransaction;

    private Button btnClose;
    private ImageView ivRemarks, ivDiagrams, ivUploadFiles, ivDeletedAttachments;
    private ProgressBar loaderRemarks, loaderDiagrams, loaderUploadFiles, loaderDeletedAttachments;

    private String bulkJSON = "";
    private List<ICheckItem> ICheckItemList;
    private List<Attachment> attachments, deletedAttachments;
    private int attachmentPosition = 0;

    public static DialogFragmentSync createInstance(@Nullable Bundle data){

        DialogFragmentSync fragment = new DialogFragmentSync();
        fragment.setArguments(data);

        return fragment;
    }


    @Override
    public void initMain() {
        setLayoutId(R.layout.dialog_fragment_sync);
    }

    @Override
    public void initData(Bundle bundle) {

        dbTransaction = new SyncDBTransaction(getActivity());
        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();

        ICheckItemList = new ArrayList<>();
        attachments = new ArrayList<>();
        deletedAttachments = new ArrayList<>();

    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected boolean isCustomDialog() {
        return false;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setTitle(getString(R.string.sync));

        bulkSyncActions();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setCanceledOnTouchOutside(false);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initViews(View view, Bundle savedInstanceState) {

        /***parent view to animate*/

        View viewToAnimate = view.findViewById(R.id.rlParent);
        setViewToAnimate(viewToAnimate);
        setAnimationEnter(R.anim.anim_slide_up_enter);
        setAnimationExit(R.anim.anim_slide_down_exit);

        ivRemarks = (ImageView) view.findViewById(R.id.ivSyncRemarks);
        ivDiagrams = (ImageView) view.findViewById(R.id.ivDiagrams);
        ivUploadFiles = (ImageView) view.findViewById(R.id.ivUploadFiles);
        ivDeletedAttachments = (ImageView) view.findViewById(R.id.ivDeletedAttachments);
        loaderRemarks = (ProgressBar) view.findViewById(R.id.loaderSyncRemarks);
        loaderDiagrams = (ProgressBar) view.findViewById(R.id.loaderDiagrams);
        loaderUploadFiles = (ProgressBar) view.findViewById(R.id.loaderUploadFiles);
        loaderDeletedAttachments = (ProgressBar) view.findViewById(R.id.loaderDeletedAttachments);
        btnClose = (Button) view.findViewById(R.id.btnClose);

        btnClose.setOnClickListener(this);

    }

    private void hideLoader(View loader, ImageView done){
        done.setImageResource(R.drawable.ic_sync_done);
        loader.setVisibility(View.GONE);
        done.setVisibility(View.VISIBLE);
    }

    private void showLoader(View loader, ImageView imageView){
        imageView.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        requestQueue.cancelAll(APIRequestCode.REQUEST_TAG);


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()){

            default:
                hideParent();
                break;
        }
    }


    @Override
    protected void handleCommonAPI(int requestCode) {

    }



    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        Log.v("lester-log-delete", String.valueOf(requestCode));

        switch (requestCode){

            case APIRequestCode.CODE_BULK_DISPOSITION_ITEMS:
                handleSyncRemarks(object);
                hideLoader(loaderRemarks, ivRemarks);
                bulkAttachment();
                break;

            case APIRequestCode.CODE_UPLOAD_ATTACHMENT:
                handleSyncAttachment(object, attachmentPosition);
                attachmentPosition = attachmentPosition+1;
                requestSyncAttachment(attachmentPosition);
                break;

            case APIRequestCode.CODE_BULK_SYNC_DELETE_ATTACHMENTS:
                handleSyncDeleteAttachments(object);
                hideLoader(loaderDeletedAttachments, ivDeletedAttachments);
                break;


            default:break;
        }

    }

    @Override
    public void onFailed(int requestCode, String message) {
        super.onFailed(requestCode, message);

        switch (requestCode){

            case APIRequestCode.CODE_BULK_DISPOSITION_ITEMS:
                hideLoader(loaderRemarks, ivRemarks);
                ivRemarks.setImageResource(R.drawable.ic_sync_failed);
                break;

            case APIRequestCode.CODE_UPLOAD_ATTACHMENT:
                attachmentPosition = attachmentPosition+1;
                requestSyncAttachment(attachmentPosition);
                break;

            case APIRequestCode.CODE_BULK_SYNC_DELETE_ATTACHMENTS:
                hideLoader(loaderDeletedAttachments, ivDeletedAttachments);
                ivDeletedAttachments.setBackgroundResource(R.drawable.ic_sync_failed);
                break;

            default:break;
        }
    }




    private void bulkSyncActions(){
        ICheckItemList = (List<ICheckItem>) dbTransaction.getUnsyncObjects(ICheckItem.class);
        if (ICheckItemList !=null){
            if (ICheckItemList.size()!=0){

                bulkJSON = BulkHelper.formatBulkActions(getActivity());
                Log.v("lester-log-json", bulkJSON);
                requestSyncRemarks(bulkJSON);

            }else {
                hideLoader(loaderRemarks, ivRemarks);
                bulkAttachment();

            }
        }else {
            hideLoader(loaderRemarks, ivRemarks);
            bulkAttachment();
        }
    }

    private void handleSyncAttachment(Object object, int position) {
        UploadedAttachment file = (UploadedAttachment) object;


        ImageHelper.renameFile(getActivity(), attachments.get(position).getUploadedName(), file.getFileName());

        Attachment attachmentCopy = (Attachment) dbTransaction.copyFromRealm(attachments.get(position));
//        dbTransaction.deleteRealmObject(Attachment.class, attachments.get(position).getUploadedId(), DatabaseConstants.KEY_UPLOADED_ID );

        attachmentCopy.setUploadedId(String.valueOf(file.getUploadedId()));
        attachmentCopy.setUploadedName(file.getFileName());
        attachmentCopy.setRowId(attachmentCopy.getRowId());
        attachmentCopy.setSync(true);
        dbTransaction.add(attachmentCopy);

        dbTransaction.deleteRealmObject(Attachment.class, attachments.get(position).getUploadedId(), DatabaseConstants.KEY_UPLOADED_ID );

    }

    private void requestSyncAttachment(int newAttachmentPosition){
        if (newAttachmentPosition+1 <= attachments.size()){

            Attachment item = attachments.get(newAttachmentPosition);

            String bgPath = String.format("%s/%s", ImageHelper.getAttachmentPath(getActivity()),
                    item.getUploadedName());

            Request request = CommonAPI.uploadAttachments(APIRequestCode.CODE_UPLOAD_ATTACHMENT,
                    item.getRowId(),
                    bgPath,this);
            request.setTag(APIRequestCode.REQUEST_TAG);
            showLoader(loaderUploadFiles, ivUploadFiles);
            requestQueue.add(request);
        }else {

            hideLoader(loaderUploadFiles, ivUploadFiles);
            bulkDeleteAttachments(); // TODO: 11/10/2017 to be uncomment
        }
    }


    private void requestSyncRemarks(String remarksJSON){
        showLoader(loaderRemarks, ivRemarks);
        Request request = BulkAPI.bulkSyncActions(APIRequestCode.CODE_BULK_DISPOSITION_ITEMS, remarksJSON, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);
    }

    private void handleSyncRemarks(Object object){
        BulkAction obj = (BulkAction) object;
        List<BulkModel> addRemarks = obj.getAdd();
        List<BulkModel> updateRemarks = obj.getUpdate();

        for (BulkModel addModel: addRemarks){
            BulkHelper.addOrUpdateWAlkThruRemark(getActivity(), addModel, true);
        }

        for (BulkModel updateModel: updateRemarks) {
            BulkHelper.addOrUpdateWAlkThruRemark(getActivity(), updateModel, false);
        }

    }



    private void bulkAttachment(){

//        attachments = (List<Attachment>) dbTransaction.getUnsyncObjects(Attachment.class);
        attachments.clear();
        attachments.addAll(dbTransaction.getUnsyncAttachments(Attachment.class));

        if (attachments!=null){
            if (attachments.size()!=0){
                attachmentPosition = 0;
                requestSyncAttachment(attachmentPosition);

            }else {
                hideLoader(loaderUploadFiles, ivUploadFiles);
                bulkDeleteAttachments();

            }
        }else {
            hideLoader(loaderUploadFiles, ivUploadFiles);
            bulkDeleteAttachments();
        }
    }





    private void requestSyncDeletedAttachment(String diagramModelJSON){
        showLoader(loaderDeletedAttachments, ivDeletedAttachments);
        Request request = BulkAPI.bulkDeleteAttachment(APIRequestCode.CODE_BULK_SYNC_DELETE_ATTACHMENTS,
                diagramModelJSON,this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);
    }

    private void bulkDeleteAttachments(){

        deletedAttachments = (List<Attachment>) dbTransaction.getDeletedObjects(Attachment.class);

        if (deletedAttachments!=null){
            if (deletedAttachments.size()!=0){

                bulkJSON = BulkHelper.formatDeleteBulkAttachment(getActivity());
                requestSyncDeletedAttachment(bulkJSON);

            }else {
                hideLoader(loaderDeletedAttachments, ivDeletedAttachments);

            }
        }else {
            hideLoader(loaderDeletedAttachments, ivDeletedAttachments);

        }
    }

    private void handleSyncDeleteAttachments(Object object){
        List<BulkModel> objects = (List<BulkModel>) object;
        if (objects.size()!=0){

            for (BulkModel item: objects){
                Log.v("lester-log-uploaded", item.getId());
                dbTransaction.deleteRealmObject(Attachment.class, item.getId(),DatabaseConstants.KEY_UPLOADED_ID);
            }
        }
    }

}

