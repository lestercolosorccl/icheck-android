package com.rcclpmo.icheck.controllers.reviewList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseViewHolder;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.Supplier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class AdapterOption<T> extends RecyclerView.Adapter<AdapterOption.OptionViewHolder> {

    private List<T> itemList, itemsCopy;
    private RecyclerViewClickListener<Object> listener;
    private Context context;

    private int type;

    public AdapterOption(Context context, List<T> itemList, RecyclerViewClickListener<Object> listener){
        this.itemList = itemList;
        this.itemsCopy =  new ArrayList<>();
        this.itemsCopy.addAll(itemList);
        this.listener = listener;
        this.context = context;
    }

    @Override
    public AdapterOption.OptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_option_item,parent,false);

        return new AdapterOption.OptionViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(AdapterOption.OptionViewHolder holder, int position) {
        holder.setViews(itemList.get(position));
    }

    @Override
    public int getItemCount() {
        return itemList.size();

    }

    public class OptionViewHolder extends BaseViewHolder<Object> {

        private TextView tvItem;

        public OptionViewHolder(View itemView, RecyclerViewClickListener<Object> listener) {
            super(itemView, listener);

            tvItem = (TextView) itemView.findViewById(R.id.tvItemName);

            itemView.setOnClickListener(this);
        }

        @Override
        public Object getObject() {
            return itemList.get(getAdapterPosition());

        }

        @Override
        public void setViews(Object object) {
            String name = "";
            if (object instanceof Project){
                name = ((Project) object).getProjectName();

            }else if (object instanceof Area){
                name = ((Area) object).getAreaName();

            }else if (object instanceof Deck){
                name = ((Deck) object).getDeckName();

            }else if (object instanceof People){
                name = ((People) object).getName();

            }else if (object instanceof Discipline){
                name = ((Discipline) object).getDiscipline();

            }else if (object instanceof Supplier){
                name = ((Supplier) object).getCompany();

            }else if (object instanceof String){
              name = (String) object;
            }

            tvItem.setText(name);


        }

    }

    public void filter(String text) {
        itemList.clear();
        if(text.isEmpty()){
            itemList.addAll(itemsCopy);
        } else{
            text = text.toLowerCase();

            for(T item: itemsCopy){

                if (item instanceof Project){
                    if(((Project) item).getProjectName().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }

                if (item instanceof Area){
                    if(((Area) item).getAreaName().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }

                if (item instanceof People){
                    if(((People) item).getName().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }

                if (item instanceof Deck){
                    if(((Deck) item).getDeckName().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }


                if (item instanceof Discipline){
                    if(((Discipline) item).getDiscipline().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }

                if (item instanceof Supplier){
                    if(((Supplier) item).getCompany().toLowerCase().contains(text)){
                        itemList.add(item);
                    }
                }

                if (item instanceof String){
                    if (((String) item).contains(text)){
                        itemList.add(item);
                    }

                }




            }
        }
        notifyDataSetChanged();
    }
}


