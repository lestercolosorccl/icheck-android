package com.rcclpmo.icheck.controllers.addAction;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.iCheckAPI;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.bases.BaseActivity;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.controllers.reviewList.ActivityICheckList;
import com.rcclpmo.icheck.controllers.reviewList.DialogFragmentOptionSelection;
import com.rcclpmo.icheck.customviews.CustomPhotoGallery;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.helpers.DateHelper;
import com.rcclpmo.icheck.helpers.FileHelper;
import com.rcclpmo.icheck.helpers.ImageHelper;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Login;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.Supplier;
import com.rcclpmo.icheck.models.UploadedAttachment;
import com.rcclpmo.icheck.models.User;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ActivityCheckItem extends BaseActivity implements RecyclerViewClickListener<Object>{

    /****
     * VIEWS
     */
    private RelativeLayout rlLoader;
    private LinearLayout llOptionCOntainer;
    private RecyclerView rvAttachment;
    private EditText etRemarks, etComments;
    private TextView tvArea, tvDeck, tvDiscipline, tvRemarkOwner, tvStatus, tvDueDate,
            tvPriority, tvRemarkType, tvProject;
    private TextView tvLoader;
    private Button btnSaveAction, btnReviewList, btnAddNew;
    private PopupWindow popupWindow;
    private LinearLayout btnAddPhoto, btnTakePhoto, btnAddFile;
    private ScrollView svForms;

    /****
     * CLASSES
     */
    private AdapterAttachment adapterAttachment;
    private ApplicationClass applicationClass;
    private SyncDBTransaction dbTransaction;
    private RequestQueue requestQueue;

    /****
     * POJO AND DATA TYPES
     */
    private List<Attachment> uploadFileList, addedAttachments;
    private List<People> peopleList;
    private List<Area> areaList;
    private List<Deck> deckList;
    private List<Project> projectList;
    private List<Discipline> disciplineList;
    private ArrayList<String> imagesPathList;
    private List<Supplier> contractorList;
    private ICheckItem selectedICheckItem;
    private People selectedRemarkOwner, selectedAreaManager;
    private Area selectedArea;
    private Deck selectedDeck;
    private Discipline selectedDiscipline;
    private Supplier selectedContractor;
    private String selectedDueDate ="";
    private String selectedStatus ="", selectedRemarkType ="", selectedPriority ="", selectedComments ="", selectedRemarks ="";
    private Attachment toBeDeletedFile;
    private User user;
    private ICheckItemData actionData;
    private Project selectedProject;
    private boolean isAddRemark;//flag to check if for update or adding
    private Uri outputFileUri;
    private String newRemarkId;
    private int attachmentPosition = 0, newAttachmentPosition = 0;
    private int currentAttachment = 0;
    private boolean isFirstLoad = true;
    private String DefaultInput = "General";
    List<String> statusList;
    List<String> priorityList;
    List<String> remarkTypeList;



    @Override
    protected void initData(Intent data) {

        uploadFileList = new ArrayList<>();
        addedAttachments = new ArrayList<>();
        imagesPathList = new ArrayList<>();

        projectList = new ArrayList<>();
        areaList = new ArrayList<>();
        deckList = new ArrayList<>();
        peopleList = new ArrayList<>();
        disciplineList = new ArrayList<>();
        contractorList = new ArrayList<>();


        applicationClass = ApplicationClass.getInstance();
        dbTransaction = new SyncDBTransaction(getApplicationContext());
        requestQueue = applicationClass.getRequestQueue();

        getBundleData(data);

        loadLocalPeopleList();
        loadLocalDeckList();
        loadLocalAreaList();
        loadLocalProviderList();
        loadLocalSupplierList();

    }

    private void getBundleData(Intent data){
        /****
         * get bundle data
         */
        if (data!=null){

            isAddRemark = data.getBooleanExtra(CommonConstants.KEY_IS_ADD_REMARK,false);
            selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class, DatabaseConstants.KEY_PROJECT_ID, applicationClass.getShipId());
            /****
             * check if add or edit remark
             */
            if (!isAddRemark){
                newRemarkId = data.getStringExtra(CommonConstants.KEY_REMARK_ID);
                selectedICheckItem = (ICheckItem) dbTransaction.getDynamicRealmObject(ICheckItem.class, DatabaseConstants.KEY_ID, newRemarkId);

            }

        }

        user = (User) dbTransaction.getFirstObject(User.class);


    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_new_action;
    }

    @Override
    protected void initViews() {

        rlLoader = (RelativeLayout) findViewById(R.id.rlLoader);
        llOptionCOntainer = (LinearLayout) findViewById(R.id.llOptionsContainer);
        rvAttachment = (RecyclerView) findViewById(R.id.rvAttachments);
        tvPriority = (TextView) findViewById(R.id.tvPriority);
        tvDiscipline = (TextView) findViewById(R.id.tvDiscipline);
        tvArea = (TextView) findViewById(R.id.tvArea);
        tvDeck = (TextView) findViewById(R.id.tvDeck);
        tvLoader = (TextView) findViewById(R.id.tvLoader);
        tvDueDate = (TextView) findViewById(R.id.tvDueDate);
        tvStatus = (TextView) findViewById(R.id.tvStatus);

        etRemarks = (EditText) findViewById(R.id.etRemarks);
        etComments = (EditText) findViewById(R.id.etComments);

        btnSaveAction = (Button) findViewById(R.id.btnSaveAction);
        btnReviewList = (Button) findViewById(R.id.btnReviewList);
        btnAddNew = (Button) findViewById(R.id.btnAddNew);
        btnAddPhoto = (LinearLayout) findViewById(R.id.btnAddPhoto);
        btnTakePhoto = (LinearLayout) findViewById(R.id.btnTakePhoto);
        tvProject = (TextView) findViewById(R.id.tvProject);
        tvRemarkOwner = (TextView) findViewById(R.id.tvRemarkOwner);
        btnAddFile = (LinearLayout) findViewById(R.id.btnAddFile);
        svForms = (ScrollView) findViewById(R.id.svForms);
        tvRemarkType = (TextView) findViewById(R.id.tvRemarkType);


        statusList = Arrays.asList(getResources().getStringArray(R.array.status));
        priorityList = Arrays.asList(getResources().getStringArray(R.array.priority));
        remarkTypeList = Arrays.asList(getResources().getStringArray(R.array.remark_type));


        initListeners();
        initRecyclerView();

        //move slide to the top

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
        {
            outputFileUri= savedInstanceState.getParcelable("outputFileUri");
            Log.i("outputFileUri",outputFileUri.getPath() + "test instance");
        }

        setViews();
        setActionBar();

        if (!isAddRemark){
            requestGetAttachments();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("outputFileUri", outputFileUri);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //todo add tags
        showLoader(false,"");

        requestQueue.cancelAll(APIRequestCode.REQUEST_TAG);
        requestQueue.cancelAll(APIRequestCode.TAG_DOWNLOAD_IMAGE);

    }

    private void setViews(){

        initRequired(R.id.tvProjectLabel);
        initRequired(R.id.tvDueDateLabel);
        initRequired(R.id.tvPriorityLabel);
        initRequired(R.id.tvDisciplineLabel);
        initRequired(R.id.tvCommentsLabel);
        initRequired(R.id.tvDeckLabel);
        initRequired(R.id.tvAreaLabel);
        initRequired(R.id.tvRemarkTypeLabel);
        initRequired(R.id.tvStatusLabel);
        initRequired(R.id.tvRemarksLabel);
        initRequired(R.id.tvRemarkOwnerLabel);


        if (!isAddRemark){

            if (selectedICheckItem != null) {

                if (!isMyAction()){
                    setViewEnable(false);
                }

                selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class, DatabaseConstants.KEY_PROJECT_ID,
                        selectedICheckItem.getProjectId());
                if (selectedProject!=null){
                    tvProject.setText(selectedProject.getProjectName());

                }

                selectedStatus = selectedICheckItem.getStatus();
                tvStatus.setText(selectedStatus);

                selectedPriority = selectedICheckItem.getPriority();
                tvPriority.setText(selectedPriority);

                selectedRemarkType = selectedICheckItem.getRemarkType();
                tvRemarkType.setText(selectedRemarkType);

                selectedDueDate = selectedICheckItem.getDueDate();
                tvDueDate.setText(selectedDueDate);

                selectedRemarks = selectedICheckItem.getRemark();
                etRemarks.setText(selectedRemarks);

                selectedComments = selectedICheckItem.getComment();
                etComments.setText(selectedComments);

                if (selectedICheckItem.getActionOwner()!=null) {
                    selectedRemarkOwner = (People) dbTransaction.getDynamicRealmObject(People.class,
                            DatabaseConstants.KEY_USER_ID, selectedICheckItem.getActionOwnerId());
                    if (selectedRemarkOwner != null) {
                        tvRemarkOwner.setText(selectedRemarkOwner.getName());
                    }else{
                        selectedRemarkOwner.setUserId(String.valueOf(0));
                        selectedRemarkOwner.setName("");
                    }
                }


                    if (selectedICheckItem.getDisciplineId().equals("-99") || selectedICheckItem.getDisciplineId().equals("") || selectedICheckItem.getDisciplineId().equals("-1")){
                        setGeneralDiscipline();
                    }else{
                        selectedDiscipline = (Discipline) dbTransaction.getDynamicRealmObject(Discipline.class, DatabaseConstants.KEY_DISCIPLINE_ID, selectedICheckItem.getDisciplineId());
                        if (selectedDiscipline != null) {
                            tvDiscipline.setText(selectedDiscipline.getDiscipline());
                        }
                    }


                    if (selectedICheckItem.getDeckId().equals("-99") || selectedICheckItem.getDeckId().equals("") || selectedICheckItem.getDeckId().equals("-1")){
                        setGeneralDeck();
                    }else{
                        selectedDeck = (Deck) dbTransaction.getDynamicRealmObject(Deck.class, "id", selectedICheckItem.getDeckId());
                        if (selectedDeck != null) {
                            tvDeck.setText(selectedDeck.getDeckName());
                        }
                    }


                    if (selectedICheckItem.getAreaId().equals("-99") || selectedICheckItem.getAreaId().equals("") || selectedICheckItem.getAreaId().equals("-1")){
                        setGeneralArea();
                    }else {
                        selectedArea = (Area) dbTransaction.getDynamicRealmObject(Area.class, "id", selectedICheckItem.getAreaId());
                        if (selectedArea != null) {
                            tvArea.setText(selectedArea.getAreaName());
                        }
                    }

//                //todo temp
//                //edit
//
//                etComments.setText(selectedICheckItem.getComment());
//                etRemarks.setText(selectedICheckItem.getRemark());
//
//
//                selectedStatus = selectedICheckItem.getStatus();
//                tvStatus.setText(selectedStatus);
//                selectedDueDate = selectedICheckItem.getDateAdded();
//                tvDueDate.setText(selectedDueDate);
//                selectedPriority = selectedICheckItem.getPriority();
//                selectedRemarkType = selectedICheckItem.getRemarkType();
//                selectedWorkBy = selectedICheckItem.getAddedBy();
//                tvPriority.setText(selectedPriority);
//
            }


        }else {

            if (selectedProject!=null){
                tvProject.setText(selectedProject.getProjectName());
            }

            setGeneralArea();
            setGeneralDeck();
            setGeneralDiscipline();
            setGeneralRemarkOwner();
            setDefaultRemarkType();
            setDefaultStatus();
            setDefaultPriority();
            selectedDueDate = DateHelper.getCurrentFormattedDate();
            tvDueDate.setText(selectedDueDate);
        }

    }

    private boolean isMyAction(){

        //todo temp
        return true;
//        return selectedICheckItem !=null? selectedICheckItem.getAssigneeId().equals(applicationClass.getUserId())||
//                selectedICheckItem.getCreatedById().equals(applicationClass.getUserId()):true;
    }

    private void setViewEnable(boolean isEnable){
        tvProject.setEnabled(isEnable);
        btnTakePhoto.setEnabled(isEnable);
        btnSaveAction.setEnabled(isEnable);
        btnAddPhoto.setEnabled(isEnable);
        btnReviewList.setEnabled(isEnable);
        btnAddFile.setEnabled(isEnable);
        tvDiscipline.setEnabled(isEnable);
        tvRemarkOwner.setEnabled(isEnable);
        tvStatus.setEnabled(isEnable);
        tvDueDate.setEnabled(isEnable);
        tvArea.setEnabled(isEnable);
        etComments.setEnabled(isEnable);
        // TODO: 19/12/2017 add more fields

    }

    private void initRequired(int id){
        TextView textView = findViewById(id);
        Spannable word = new SpannableString("*");

        word.setSpan(new ForegroundColorSpan(Color.RED), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.append(word);
    }

    private void setGeneralArea(){
        Area area = new Area();
        area.setAreaId(CommonConstants.GENERAL_ID);
        area.setAreaName(CommonConstants.GENERAL_VALUE);
//        dbTransaction.add(venue);
        selectedArea = area;
        tvArea.setText(selectedArea.getAreaName());
    }

    private void setGeneralDeck(){
        Deck deck = new Deck();
        deck.setId(CommonConstants.GENERAL_ID);
        deck.setDeckName(CommonConstants.GENERAL_VALUE);
        selectedDeck = deck;
        tvDeck.setText(selectedDeck.getDeckName());
    }

    private void setGeneralDiscipline(){
        Discipline discipline = new Discipline();
        discipline.setDisciplineId(CommonConstants.GENERAL_ID);
        discipline.setDiscipline(CommonConstants.GENERAL_VALUE);
        selectedDiscipline = discipline;
        tvDiscipline.setText(selectedDiscipline.getDiscipline());
    }

    private void setGeneralRemarkOwner(){
        selectedRemarkOwner = (People) dbTransaction.getDynamicRealmObject(People.class,
                DatabaseConstants.KEY_USER_ID, user.getId());
        if (selectedRemarkOwner != null) {
            tvRemarkOwner.setText(selectedRemarkOwner.getName());
        }
    }

    private void setDefaultRemarkType(){
        selectedRemarkType = remarkTypeList.get(0);
        tvRemarkType.setText(selectedRemarkType);
    }
    private void setDefaultStatus(){
        selectedStatus = statusList.get(1);
        tvStatus.setText(selectedStatus);
    }
    private void setDefaultPriority(){
        selectedPriority = priorityList.get(0);
        tvPriority.setText(selectedPriority);
    }



    private void setActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(isAddRemark? getString( R.string.app_name):
                selectedICheckItem.getRemark()==null? getString(R.string.app_name): selectedICheckItem.getRemark());


    }

    private void initRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        adapterAttachment = new AdapterAttachment(getApplicationContext(),uploadFileList, this);
        rvAttachment.setLayoutManager(layoutManager);
        rvAttachment.setAdapter(adapterAttachment);
        rvAttachment.setHasFixedSize(true);
    }

    private void initListeners(){
        // TODO: 19/12/2017 add more item here
        tvProject.setOnClickListener(this);
        tvArea.setOnClickListener(this);
        tvDeck.setOnClickListener(this);
        tvDiscipline.setOnClickListener(this);
        tvPriority.setOnClickListener(this);
        tvStatus.setOnClickListener(this);
        tvDueDate.setOnClickListener(this);
        btnAddPhoto.setOnClickListener(this);
        btnTakePhoto.setOnClickListener(this);
        btnReviewList.setOnClickListener(this);
        btnSaveAction.setOnClickListener(this);
        btnAddFile.setOnClickListener(this);
        tvRemarkType.setOnClickListener(this);
        tvRemarkOwner.setOnClickListener(this);

    }

    private void showLoader(boolean isShown, String loader){
        rlLoader.setVisibility(isShown? View.VISIBLE: View.GONE);
        tvLoader.setText(loader);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    protected void handleRefreshToken() {

    }


    @Override
    public void onRecyclerItemClick(int position, Object object) {

        if (object instanceof Attachment){
            handleSelectedUploadedFile(position, object);
        }
        else if (object instanceof People){
            //position will serve as the option type
            handleSelectedPeople(position,object);
        }
        else if (object instanceof Area){
            handleSelectedArea(object);
        }else if (object instanceof Deck){
            handleSelectedDeck((Deck)object);
        }else if (object instanceof Discipline){
            handleSelectedProvider(object);
        }else if (object instanceof Project){
            handleSelectedProject(object);

        }else if (object instanceof String){
            handleSelectedStatus(object);
        }else {
            dismissPopUp();
        }
    }

    private void deleteTempFile(){
        File tempFile = new File(ImageHelper.getDownloadAttachmentPath(getApplicationContext()));
        FileHelper.deleteFilesAndDirectory(tempFile);
    }

    private void handleSelectedStatus(Object object){


        for (String item:statusList){
            if (item.equalsIgnoreCase((String) object)){
                selectedStatus = (String) object;
                tvStatus.setText(selectedStatus);
                dismissPopUp();
                return;
            }
        }


        for (String item:priorityList){
            if (item.equalsIgnoreCase((String) object)){
                selectedPriority = (String) object;
                tvPriority.setText(selectedPriority);
                dismissPopUp();
                return;
            }
        }

        for (String item:remarkTypeList){
            if (item.equalsIgnoreCase((String) object)){
                selectedRemarkType = (String) object;
                tvRemarkType.setText(selectedRemarkType);
                dismissPopUp();
                return;
            }
        }
    }

    private void handleSelectedArea(Object object){

        selectedArea = (Area) object;
        tvArea.setText(selectedArea.getAreaName());
        dismissPopUp();

    }

    private void handleSelectedPeople(int type,Object object){

        switch (type){
            case CommonConstants.OPTION_TYPE_PEOPLE:
                selectedRemarkOwner = (People) object;
                tvRemarkOwner.setText(selectedRemarkOwner.getName());
                break;

        }
        dismissPopUp();

    }

    private void handleSelectedDeck(Deck object){
        selectedDeck =  object;
        dismissPopUp();
        tvDeck.setText(selectedDeck.getDeckName());

    }

    private void handleSelectedProvider(Object object){
        selectedDiscipline = (Discipline) object;
        tvDiscipline.setText(selectedDiscipline.getDiscipline());
        dismissPopUp();
    }

    private void handleSelectedProject(Object object){
        selectedProject = (Project) object;
//        applicationClass.saveShipId(selectedProject.getId(),selectedProject.getName());
//        applicationClass.saveProjectId(selectedProject.getId());
            tvProject.setText(selectedProject.getProjectName());
        //set the ship name
        //set selected ship in the toolbar

    }


    private void handleSelectedUploadedFile(int position,Object object){

        deleteTempFile();

        if (position == -1) {//-1 serves as marking if function is for delete

            if (isMyAction()){
                toBeDeletedFile = (Attachment) object;
                if (toBeDeletedFile.getUploadedId().contains("-")){
                    addedAttachments.remove(toBeDeletedFile);
                    uploadFileList.remove(toBeDeletedFile);
                    adapterAttachment.notifyDataSetChanged();
                    dbTransaction.deleteRealmObject(Attachment.class, toBeDeletedFile.getUploadedId(), DatabaseConstants.KEY_UPLOADED_ID);

                }else {

                    requestDeleteAttachment(toBeDeletedFile);
                }

            }

        } else {

            String bgPath;
            Attachment uploadFile = (Attachment) object;
            if (!uploadFile.getUploadedName().contains("/")){

                bgPath = String.format("%s/%s/%s", applicationClass.getFilesDir().toString(),
                        getString(R.string.attachment_path), uploadFile.getUploadedName());

            }else {
                bgPath = uploadFile.getUploadedName();

            }

//            if (new File(bgPath).isFile()) {
////                Bitmap bmp = FileHelper.decodeSampledBitmapFromFile(bgPath,500,600);
////
////                zoomImageFromThumb(rvAttachment.getChildAt(position),bmp);
//
//                showZoom(bgPath);
//
//            }


//            Attachment selectedFile = (Attachment) object;
//
            if (bgPath.contains(".doc")|| bgPath.contains(".docx")){

//                String bgPath = String.format("%s/%s/", applicationClass.getFilesDir().toString(),
//                        getString(R.string.attachment_path));

                try {
                    FileHelper.copyFile(getApplicationContext(),ImageHelper.getAttachmentPath(getApplicationContext()),
                            uploadFile.getUploadedName(), uploadFile.getUploadedName(), true);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                viewDocuments(ImageHelper.getDownloadAttachmentPath(getApplicationContext()),uploadFile.getUploadedName());//todo temp bgPath

            }else if (bgPath.contains(".pdf")){
                try {
                    FileHelper.copyFile(getApplicationContext(),ImageHelper.getAttachmentPath(getApplicationContext()),
                            uploadFile.getUploadedName(), uploadFile.getUploadedName(), true);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                viewPdf(ImageHelper.getDownloadAttachmentPath(getApplicationContext()),uploadFile.getUploadedName());


            }else {

                showZoom(bgPath);
            }

        }

    }

    private void showZoom(String bgPath){

        Bundle data = new Bundle();
        data.putString(CommonConstants.IMAGE_FILE_FORMAT, bgPath);
        DialogFragmentZoomImage dialog = DialogFragmentZoomImage.createInstance(data);
        dialog.show(getSupportFragmentManager(), "create dialog");
    }

    private void viewDocuments(String path,String fileName){
        File targetFile = new File(path,fileName);
        if(targetFile.isFile()){

            Uri uripath = Uri.fromFile(targetFile);
            Intent objIntent = new Intent(Intent.ACTION_VIEW);
            objIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_GRANT_READ_URI_PERMISSION);
            objIntent.setDataAndType(ImageHelper.getFileDirectoryDirectory(getApplicationContext(),targetFile),"application/msword");
            startActivityForResult(objIntent,CommonConstants.VIEW_FILE_TARGET_REQUEST_CODE);

        }else {
            Toast.makeText(getApplicationContext(),"Doesn't exist.",Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPdf(String path,String fileName){

        File targetFile = new File(path,fileName);

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(ImageHelper.getFileDirectoryDirectory(getApplicationContext(),targetFile), "application/pdf");
//        Uri u = Uri.fromFile(myFile);
//        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()){

            case R.id.btnSaveAction:
                if (isFieldsValid()){
                    if (isAddRemark){
                        selectedICheckItem = null;
                        saveRemark(getActionData());
                    }else {
                        if (!selectedICheckItem.getId().contains(getString(R.string.dash))){
                            updateRemark(getActionData());
                        }else {
                            saveRemark(getActionData());
                        }
                    }

                }else {

                    Toast.makeText(getApplicationContext(), "Please fill up all fields.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnAddNew:
                setViewEnable(true);
                toggleButtonSavedRemark(true);
                //reset previous selected values
                selectedICheckItem = null;
                isAddRemark = true;
                //clear comment field
                etComments.setText("");
                uploadFileList.clear();
                addedAttachments.clear();
                newAttachmentPosition = 0;
                adapterAttachment.notifyDataSetChanged();
                break;

            case R.id.tvArea:
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_AREA);
                    bundle1.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                    bundle1.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
//                    bundle1.putString(CommonConstants.DATA_KEY_PARENT_ID, selectedProject.getShipId());
                    showOption(bundle1);
                break;

            // TODO: 19/12/2017
            case R.id.tvDeck:
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_DECK);
                    bundle2.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                    bundle2.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
//                    bundle2.putString(CommonConstants.DATA_KEY_PARENT_ID, selectedProject.getShipId());
                    showOption(bundle2);
                break;


            case R.id.tvDiscipline:
                Bundle bundle3 = new Bundle();
                bundle3.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_DISCIPLINE);
                bundle3.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle3.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
                showOption(bundle3);
                break;

            case R.id.tvDueDate:
                showDatePicker();
                break;

            case R.id.tvRemarkOwner:
                Bundle bundle4 = new Bundle();
                bundle4.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PEOPLE);
                bundle4.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle4.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
                showOption(bundle4);
                break;


            case R.id.tvStatus:
                Bundle bundle5 = new Bundle();
                bundle5.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_STATUS);
                bundle5.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle5.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);

                showOption(bundle5);
                break;


            case R.id.tvPriority:
                Bundle bundle6 = new Bundle();
                bundle6.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PRIORITY);
                bundle6.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle6.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
                showOption(bundle6);
                break;

            case R.id.tvRemarkType:
                Bundle bundle7 = new Bundle();
                bundle7.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_REMARK_TYPE);
                bundle7.putBoolean(CommonConstants.DATA_KEY_IS_WITH_ALL_SELECTION, false);
                bundle7.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
                showOption(bundle7);
                break;


            case R.id.btnAddPhoto:
                openGalleryIntent();
                break;

            case R.id.btnTakePhoto:
                takeImageIntent();
                break;

            case R.id.btnAddFile:
                openFileIntent();
                break;

            case R.id.btnReviewList:
                goToListView();
                break;

            case R.id.tvProject:
                Bundle bundle = new Bundle();
                bundle.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PROJECT);
                bundle.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_ADD_ACTION);
                showOption(bundle);
                break;

            default:break;
        }

    }

    //todo initial implementation of disabling save button
    private void toggleButtonSavedRemark(boolean isEnable){
//        btnSaveAction.setEnabled(isEnable);

    }



    private void showOption(Bundle bundle){
        DialogFragmentOptionSelection dialog = DialogFragmentOptionSelection.createInstance(bundle);
        dialog.show(getSupportFragmentManager(),"create dialog");
    }


    private void goToListView(){
        Intent intent = new Intent(getApplicationContext(), ActivityICheckList.class);
        intent.putExtra(CommonConstants.KEY_IS_ADD_REMARK, true);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //to delete temporary created files
        deleteTempFile();

        if (resultCode == RESULT_OK) {


            if (requestCode == CommonConstants.CAMERA_TARGET_REQUEST_CODE) {

                if (ImageHelper.isCamera(data)) {
                    Log.i("outputFileUri",outputFileUri.getEncodedPath());
                    String path = outputFileUri.getPath();
                    addNewAttachment(path);
                    adapterAttachment.notifyDataSetChanged();

                }

            }else if (requestCode == CommonConstants.GALLERY_TARGET_REQUEST_CODE){
                handleGallerySelection(data);

            }else if (requestCode == CommonConstants.FILE_TARGET_REQUEST_CODE){
                if (data != null) {
                    handleFileSelection(data);

                }

            }

            svForms.fullScroll(ScrollView.FOCUS_DOWN);

        }

    }

    public void onDueDateSelected(String date){
        selectedDueDate = date;
        tvDueDate.setText(selectedDueDate);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleFileSelection(Intent data) {

        Uri selectedUriPDF = data.getData();

        if (FileHelper.isGoogleDriveDocument(selectedUriPDF)){

            Toast.makeText(getApplicationContext(), "Google file's not supported.",Toast.LENGTH_SHORT).show();

        }else {

            String path = FileHelper.getPath(getApplicationContext(), selectedUriPDF);

            handleSelectedDoc(path);


        }


    }

    private void handleSelectedDoc(String path){
        //todo generate file name
        String fileName = path.substring(path.lastIndexOf("/")+1);
        String filePath = path.substring(0,path.lastIndexOf("/"));

        try {

            //todo add dash as indicator of new file
            String tempFileName = String.format("-%s%s",String.valueOf(System.currentTimeMillis()), fileName);
            currentAttachment = currentAttachment+1;

            FileHelper.copyFile(getApplicationContext(), filePath, fileName, tempFileName,false);

            addNewAttachment(tempFileName);
            adapterAttachment.notifyDataSetChanged();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void handleGallerySelection(Intent data){

        String[] imagesPath = data.getStringExtra("data").split("\\|");

        for (int i=0;i<imagesPath.length;i++){
            imagesPathList.add(imagesPath[i]);
            Log.i("BITMAP_URL",imagesPath[i]);
            String imgPath = imagesPath[i];
            /****
             *CREATE NEW OBJECT FOR SELECTED FILES
             */
            addNewAttachment(imgPath);
        }

        adapterAttachment.notifyDataSetChanged();
    }


    private void addNewAttachment(String fileName){
        String tempFileName = String.valueOf(System.currentTimeMillis()+currentAttachment);
        currentAttachment = currentAttachment+1;
//        ImageHelper.handleDownloadImage(getApplicationContext(),selectedBitmap,tempFileName);
        Attachment toBeSavedFile = new Attachment();
        toBeSavedFile.setSync(false);
        toBeSavedFile.setUploadedId(String.format("%s%s",getString(R.string.dash), tempFileName));
        toBeSavedFile.setUploadedName(fileName);
        toBeSavedFile.setRowId(newRemarkId);
//        toBeSavedFile.setType();//todo set type here

        uploadFileList.add(0,toBeSavedFile);
        addedAttachments.add(toBeSavedFile);

    }

    private void takeImageIntent() {



//        String tempFileName = String.valueOf(System.currentTimeMillis()+currentAttachment);

//        File photo = new File(Environment.getExternalStorageDirectory(),  String.format("%s.%s", tempFileName, CommonConstants.IMAGE_FILE_FORMAT));
//        intent.putExtra(MediaStore.EXTRA_OUTPUT,
//                Uri.fromFile(photo));
//        outputFileUri = Uri.fromFile(photo);


        outputFileUri = ImageHelper.getImageMainDirectory(getApplicationContext());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                outputFileUri);

        startActivityForResult(intent, CommonConstants.CAMERA_TARGET_REQUEST_CODE);

    }

    private void openGalleryIntent() {

        outputFileUri = ImageHelper.getImageMainDirectory(getApplicationContext());
        final Intent galleryIntent = new Intent(this,CustomPhotoGallery.class);

        startActivityForResult(galleryIntent, CommonConstants.GALLERY_TARGET_REQUEST_CODE);

    }

    private void openFileIntent() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |Intent.FLAG_GRANT_READ_URI_PERMISSION);
        String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/msword", "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(Intent.createChooser(intent, "Select a File "), CommonConstants.FILE_TARGET_REQUEST_CODE);

    }

    private void dismissPopUp(){
        if (popupWindow != null){
            popupWindow.dismiss();
        }

    }

    public void showDatePicker() {
        DialogFragment newFragment = new DatePickerFragment();
        Bundle data = new Bundle();
        data.putString(CommonConstants.KEY_DATE, selectedDueDate);
        newFragment.setArguments(data);
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }


    private boolean isFieldsValid(){

        boolean result = true;
        if (    selectedProject==null ||
                selectedRemarkOwner ==null ||
                selectedDiscipline ==null ||
                selectedRemarkType == null ||
                selectedArea ==null ||
                selectedStatus == null ||
                selectedDueDate == null ||
                etRemarks.getText().toString().equals("") ||
                etComments.getText().toString().equals("")
                ){
            result = false;
        }

        return result;
    }

    private ICheckItemData getActionData(){






        actionData = new ICheckItemData();
        actionData.setProjectId(selectedProject.getProjectId());
        actionData.setArea(selectedArea.getAreaName());
        actionData.setAreaId(selectedArea.getAreaId());
        actionData.setDiscipline(selectedDiscipline.getDiscipline());
        actionData.setDisciplineId(selectedDiscipline.getDisciplineId());
        actionData.setDeckId(selectedDeck.getId());
        actionData.setDeck(selectedDeck.getDeckName());
        actionData.setComment(etComments.getText().toString());
        actionData.setStatus(selectedStatus);
        actionData.setPriority(selectedPriority);
        actionData.setRemarkType(selectedRemarkType);
        actionData.setRemark(etRemarks.getText().toString());
        actionData.setActionOwnerId(selectedRemarkOwner.getUserId());
        actionData.setActionOwner(selectedRemarkOwner.getName());
        actionData.setDateAdded(DateHelper.getCurrentFormattedDate());
        actionData.setDueDate(selectedDueDate);

        if (selectedICheckItem != null){
            newRemarkId = selectedICheckItem.getId();
            actionData.setId(selectedICheckItem.getId());
        }else {
            newRemarkId = (String.format("%s%s",getString(R.string.dash), DateHelper.getMillisDate()));
            actionData.setId(newRemarkId);
        }

        return actionData;

    }


    @Override
    protected void handleCommonAPI(int requestCode) {
        switch (requestCode){

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                adapterAttachment.notifyDataSetChanged();
                attachmentPosition = attachmentPosition+1;
                showLoader(false,"");
                requestImageAttachment(attachmentPosition);
                break;


            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                adapterAttachment.notifyDataSetChanged();
                attachmentPosition = attachmentPosition+1;
                showLoader(false,"");
                requestImageAttachment(attachmentPosition);
                break;

            default:break;
        }
    }

    @Override
    protected void handleNotification(String message) {
        Snackbar snackbar = Snackbar.make(rlLoader,message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_300));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.amber_500));
        snackbar.show();
    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        switch (requestCode){

            case APIRequestCode.CODE_SAVE_ACTION:
//                showMessage((String) object);
                Double obj = (Double) object;
                DecimalFormat decimalFormat=new DecimalFormat("#.#");
                newRemarkId = String.valueOf(decimalFormat.format(obj));
                //assign new remarkId
//                actionData.setRemarkId(newRemarkId);
                handleRemarkOffline(true);
                isAddRemark = false;
                showLoader(false,"");
                requestUploadAttachment(newAttachmentPosition);
                break;

            case APIRequestCode.CODE_UPDATE_ACTION:
                showMessage((String) object);
                showLoader(false,"");
                handleRemarkOffline(true);
                requestUploadAttachment(newAttachmentPosition);
                break;

            case APIRequestCode.CODE_UPLOAD_ATTACHMENT:
                handleUploadAttachment(object, newAttachmentPosition);
                newAttachmentPosition = newAttachmentPosition+1;
                showLoader(false,"");
                requestUploadAttachment(newAttachmentPosition);
                break;

            case APIRequestCode.CODE_DELETE_ATTACHMENT:
                handleDeleteAttachment();
                showLoader(false,"");
                break;

            case APIRequestCode.CODE_GET_ATTACHMENTS:
                handleGetFileByIdModuleType(object);
                loadLocalUploadFile();
                getNewAttachment();
                /****
                 * request the image attachment 1x1
                 */
                attachmentPosition = 0;
                requestImageAttachment(attachmentPosition);
                showLoader(false,"");
                break;


            default:break;

        }

    }

    @Override
    public void onFailed(int requestCode, String message) {
        super.onFailed(requestCode, message);
        Log.i("ICHECK_TAG", String.valueOf(requestCode));
        switch (requestCode){

            case APIRequestCode.CODE_SAVE_ACTION:
                handleRemarkOffline(false);
                handleUploadAttachmentOffline();
                isAddRemark = false;
                toggleButtonSavedRemark(false);
                showMessage(getString(R.string.successfully_saved));
                showLoader(false,"");
                break;

            case APIRequestCode.CODE_UPDATE_ACTION:
                handleRemarkOffline(false);
                handleUploadAttachmentOffline();
                handleDeleteAttachmentOffline();
                if (isAddRemark){
                    toggleButtonSavedRemark(false);

                }
                showMessage(getString(R.string.successfully_saved));
                showLoader(false,"");

                break;

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                adapterAttachment.notifyDataSetChanged();
                attachmentPosition = attachmentPosition+1;
//                showMessage(message);
                showLoader(false,"");
                requestImageAttachment(attachmentPosition);
//                setGAViewData();
                break;

            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                adapterAttachment.notifyDataSetChanged();
                attachmentPosition = attachmentPosition+1;
                showLoader(false,"");
                requestImageAttachment(attachmentPosition);
                break;

            case APIRequestCode.CODE_UPLOAD_ATTACHMENT:
//                showMessage(message);
                showLoader(false,"");
                handleFailedUploadAttachment();
                newAttachmentPosition = newAttachmentPosition+1;
                requestUploadAttachment(newAttachmentPosition);
                break;

            case APIRequestCode.CODE_DELETE_ATTACHMENT:
                handleDeleteAttachmentOffline();
                showLoader(false,"");
                break;

            case APIRequestCode.CODE_GET_ATTACHMENTS:
//                showMessage(message);
                loadLocalUploadFile();
                getNewAttachment();
                /****
                 * request the image attachment 1x1
                 */
                requestImageAttachment(attachmentPosition);
                showLoader(false,"");
                break;

            default:break;

        }
    }


    private void showMessage(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void loadLocalAreaList(){
        areaList.clear();
        areaList.addAll(dbTransaction.getAll(Area.class));
//        areaList.remove(dbTransaction.getDynamicRealmObject(Venue.class, DatabaseConstants.AREA_ID, CommonConstants.GENERAL_ID));
        areaList.add(0, createGeneralAreaSelection());
    }

    private void loadLocalSupplierList(){
        contractorList.clear();
        contractorList.addAll(dbTransaction.getAll(Supplier.class));

    }

    private Area createGeneralAreaSelection(){

        Area venue = new Area();
        venue.setAreaId(CommonConstants.GENERAL_ID);
        venue.setAreaName(CommonConstants.GENERAL_VALUE);

        return venue;
    }

    private void loadLocalDeckList(){
        deckList.clear();
        deckList.addAll(dbTransaction.getAll(Deck.class));
//        deckList.remove(dbTransaction.getDynamicRealmObject(Deck.class, DatabaseConstants.KEY_ID, CommonConstants.GENERAL_ID));
        deckList.add(0, createAGeneralDeckSelection());
    }

    private Deck createAGeneralDeckSelection(){

        Deck deck = new Deck();
        deck.setId(CommonConstants.GENERAL_ID);
        deck.setDeckName(CommonConstants.GENERAL_VALUE);
//        dbTransaction.add(discipline);

        return deck;
    }

    private void loadLocalPeopleList(){
        peopleList.clear();
        peopleList.addAll(dbTransaction.getAll(People.class));
//        peopleList.add(0, createAllPeopleSelection());

    }

    private People createAllPeopleSelection(){

        People people = new People();
        people.setUserId( CommonConstants.GENERAL_ID);
        people.setName(CommonConstants.GENERAL_VALUE);

        return people;
    }

    private void loadLocalProviderList(){
        disciplineList.clear();
        disciplineList.addAll(dbTransaction.getAll(Discipline.class));
//        disciplineList.remove(dbTransaction.getDynamicRealmObject(Discipline.class, DatabaseConstants.KEY_MEETING_TYPE, CommonConstants.GENERAL_VALUE));
        disciplineList.add(0, createGeneralProviderSelection());

    }

    private Discipline createGeneralProviderSelection(){

        Discipline item = new Discipline();
        item.setDisciplineId(CommonConstants.GENERAL_ID);
        item.setDiscipline(CommonConstants.GENERAL_VALUE);

        return item;
    }



    private void saveRemark(ICheckItemData data){

        showLoader(true, CommonConstants.LOADER_UPLOADING_ACTION);
        Request request = iCheckAPI.saveICheckItem(APIRequestCode.CODE_SAVE_ACTION, data,this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void updateRemark(ICheckItemData data){
        showLoader(true, CommonConstants.LOADER_UPLOADING_ACTION);
        Request request = iCheckAPI.updateICheckItem(APIRequestCode.CODE_UPDATE_ACTION, data, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);
    }

    private void handleRemarkOffline(boolean isSuccessful){

        actionData.setSync(isSuccessful);//flag to check if successful or not
        isAddRemark = actionData.getId().contains(getString(R.string.dash));// to check if remark only save from offline
        if (selectedICheckItem !=null){

            dbTransaction.updateIcheckItem(ICheckItem.class, actionData);

            if (isAddRemark){
                ICheckItem remarkCopy = (ICheckItem) dbTransaction.getObjectCopy(ICheckItem.class, DatabaseConstants.KEY_ID, actionData.getId());

                if (isSuccessful){
                    //delete existing remark
                    dbTransaction.deleteRealmObject(ICheckItem.class, actionData.getId(), DatabaseConstants.KEY_ID);
                    actionData.setId(newRemarkId);
                    remarkCopy.setId(actionData.getId());
                    dbTransaction.add(remarkCopy);
                }

            }

        }else {
            ICheckItem toBesaveICheckItem = new ICheckItem();

            //set new remark id if successfully saved or added
            if (isSuccessful){
                actionData.setId(newRemarkId);
            }

            //// TODO: 19/12/2017 add fields to be save guarantee

//            Log.v("lester-log-user", actionData.getActionOwnerId());

            toBesaveICheckItem.setId(actionData.getId());
            toBesaveICheckItem.setProjectId(actionData.getProjectId());
            toBesaveICheckItem.setArea(actionData.getArea());
            toBesaveICheckItem.setAreaId(actionData.getAreaId());
            toBesaveICheckItem.setDeckId(actionData.getDeckId());
            toBesaveICheckItem.setDeck(actionData.getDeck());
            toBesaveICheckItem.setDiscipline(actionData.getDiscipline());
            toBesaveICheckItem.setDisciplineId(actionData.getDisciplineId());
            toBesaveICheckItem.setComment(actionData.getComment());
            toBesaveICheckItem.setRemark(actionData.getRemark());
            toBesaveICheckItem.setRemarkType(actionData.getRemarkType());
            toBesaveICheckItem.setStatus(actionData.getStatus());
            toBesaveICheckItem.setDueDate(actionData.getDueDate());
            toBesaveICheckItem.setActionOwnerId(actionData.getActionOwnerId());
            toBesaveICheckItem.setActionOwner(actionData.getActionOwner());
            toBesaveICheckItem.setDateAdded(actionData.getDateAdded());
            toBesaveICheckItem.setPriority(actionData.getPriority());
            toBesaveICheckItem.setSync(actionData.isSync());

            dbTransaction.add(toBesaveICheckItem);
        }

        selectedICheckItem = (ICheckItem) dbTransaction.getDynamicRealmObject(ICheckItem.class, DatabaseConstants.KEY_ID, actionData.getId());
    }

    private String getRemarkIds(){
        ArrayList<String> mList = new ArrayList<>();

        mList.add(selectedICheckItem.getId());
        JSONArray array = new JSONArray(mList);

        return array.toString();
    }

    private void requestGetAttachments(){
        showLoader(true,CommonConstants.LOADER_DOWNLOADING_ATTACHMENTS);
        Request request = iCheckAPI.getAttachments(APIRequestCode.CODE_GET_ATTACHMENTS,getRemarkIds(), this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void handleGetFileByIdModuleType(Object object){
        dbTransaction.deleteFileList(Attachment.class, getRemarkIds());
        List<Attachment> list = (List<Attachment>) object;
        dbTransaction.add(list);


    }

    private void loadLocalUploadFile(){
        uploadFileList.clear();
        uploadFileList.addAll(dbTransaction.getUploadFileList(Attachment.class,
                getRemarkIds()));
        adapterAttachment.notifyDataSetChanged();

    }

    private void getNewAttachment(){
        addedAttachments.clear();
        for (Attachment item: uploadFileList){
            if (item.getUploadedId().contains(getString(R.string.dash))){
                addedAttachments.add(item);
            }

        }
    }

    /***request for the attachment item 1x1*/
    private void requestImageAttachment(int position){

        if (position+1 <= uploadFileList.size()){

            Attachment item = uploadFileList.get(position);

            String bgPath = String.format("%s/%s", ImageHelper.getAttachmentPath(getApplicationContext()),item.getUploadedName());
            if (!new File(bgPath).isFile()){
                showLoader(true,CommonConstants.LOADER_DOWNLOADING_ATTACHMENTS);
                if(bgPath.contains(CommonConstants.DOC_FILE_TYPE)|| bgPath.contains(CommonConstants.DOCX_FILE_TYPE)||
                        bgPath.contains(CommonConstants.PDF_FILE_TYPE)){

//                    requestQueue.add(downloadDocFile(item.getUploadedName()));
                    requestQueue.add(downloadDocFile(item.getUploadedName()));

                }else {
                    requestQueue.add(downloadImage(item.getUploadedName()));

                }

            }else {

                adapterAttachment.notifyDataSetChanged();
                attachmentPosition = attachmentPosition +1;
                requestImageAttachment(attachmentPosition);
            }
        }

    }

    private void requestUploadAttachment(int newAttachmentPosition){
        if (newAttachmentPosition+1 <= addedAttachments.size()){

            showLoader(true, CommonConstants.LOADER_UPLOADING_ATTACHMENTS);
            Attachment newAttachment = getToBeUploadedAttachment();

            String bgPath = String.format("%s/%s", ImageHelper.getAttachmentPath(getApplicationContext()),
                    newAttachment.getUploadedName());

            Request request = CommonAPI.uploadAttachments(APIRequestCode.CODE_UPLOAD_ATTACHMENT,
                    newAttachment.getRowId(),
                    bgPath,this);
            request.setTag(APIRequestCode.REQUEST_TAG);
            requestQueue.add(request);
        }else {
            if (isAddRemark){
                toggleButtonSavedRemark(false);

            }
            showMessage(getString(R.string.successfully_saved));

        }
    }

    private Attachment getToBeUploadedAttachment(){
        Attachment item = addedAttachments.get(newAttachmentPosition);

        if (item.getUploadedName().contains("/")){
            ImageHelper.handleDownloadImage(getApplicationContext(),item.getUploadedId(), item.getUploadedName(),true);
            item.setUploadedName(String.format("%s.%s",item.getUploadedId(),CommonConstants.IMAGE_FILE_FORMAT)); //

        }
        item.setRowId(newRemarkId);

        return item;
    }

    private void handleUploadAttachment(Object object, int position){
        UploadedAttachment file = (UploadedAttachment) object;
        ImageHelper.renameFile(getApplicationContext(),
                addedAttachments.get(position).getUploadedName(),file.getFileName());

        Attachment toBeSavedItem = addedAttachments.get(position);


        toBeSavedItem.setUploadedId(String.valueOf(file.getUploadedId()));
        toBeSavedItem.setUploadedName(file.getFileName());
        toBeSavedItem.setRowId(newRemarkId);
        toBeSavedItem.setSync(true);
        dbTransaction.add(toBeSavedItem);

    }


    private void handleFailedUploadAttachment(){
        dbTransaction.add(addedAttachments.get(newAttachmentPosition));

    }

    private void handleUploadAttachmentOffline(){

        for (Attachment item: addedAttachments){
            if (item.getUploadedName().contains("/")){
//                Bitmap bmp = BitmapFactory.decodeFile(item.getUploadedName());
                ImageHelper.handleDownloadImage(getApplicationContext(), item.getUploadedId(),item.getUploadedName(), true);
                item.setUploadedName(String.format("%s.%s", item.getUploadedId(), CommonConstants.IMAGE_FILE_FORMAT));
            }
            item.setRowId(newRemarkId);
            dbTransaction.add(item);
        }
    }

    private void requestDeleteAttachment(Attachment attachment) {
        showLoader(true, CommonConstants.LOADER_DELETING_ATTACHMENTS);
        Request request = CommonAPI.deleteUploadFile(APIRequestCode.CODE_DELETE_ATTACHMENT,
                attachment.getUploadedId(), attachment.getUploadedName(), this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void handleDeleteAttachment() {
        uploadFileList.remove(toBeDeletedFile);
        dbTransaction.deleteRealmObject(Attachment.class, toBeDeletedFile.getUploadedId(), DatabaseConstants.KEY_UPLOADED_ID);
        adapterAttachment.notifyDataSetChanged();

    }

    private void handleDeleteAttachmentOffline(){

        if (toBeDeletedFile!=null){
            dbTransaction.deleteUploadFileOffline(Attachment.class, toBeDeletedFile.getUploadedId());
            uploadFileList.remove(toBeDeletedFile);
            adapterAttachment.notifyDataSetChanged();
        }

    }

}

