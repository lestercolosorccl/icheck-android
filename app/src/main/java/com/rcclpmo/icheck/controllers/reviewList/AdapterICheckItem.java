package com.rcclpmo.icheck.controllers.reviewList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseViewHolder;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.FilterDataList;
import com.rcclpmo.icheck.models.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class AdapterICheckItem extends RecyclerView.Adapter<AdapterICheckItem.ActionViewHolder> {

    private List<ICheckItem> ICheckItemList;
    private List<ICheckItem> ICheckItemCopy;
    private RecyclerViewClickListener<Object> listener;
    private Context context;
    private SyncDBTransaction dbTransaction;

    public AdapterICheckItem(Context context, List<ICheckItem> ICheckItemList, RecyclerViewClickListener<Object> listener){
        this.ICheckItemList = ICheckItemList;
        this.ICheckItemCopy = new ArrayList<>();
        ICheckItemCopy.addAll(ICheckItemList);
        this.listener = listener;
        this.context = context;

        dbTransaction = new SyncDBTransaction(context);
    }

    @Override
    public AdapterICheckItem.ActionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_action_item,parent,false);

        return new AdapterICheckItem.ActionViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(AdapterICheckItem.ActionViewHolder holder, int position) {
        holder.setViews(ICheckItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return ICheckItemList.size();

    }


    public void setActionCopy(ICheckItemData selectedFilterParams, List<FilterDataList> selectedFilterParamList) {
        ICheckItemCopy.clear();
        this.ICheckItemCopy.addAll(dbTransaction.getActionsWithFilter(ICheckItem.class,selectedFilterParams,selectedFilterParamList));
    }

    public class ActionViewHolder extends BaseViewHolder<Object> {

        private TextView tvRemarkDetail, tvDate, tvRemarks;
        private View viewStatus;

        public ActionViewHolder(View itemView, RecyclerViewClickListener<Object> listener) {
            super(itemView, listener);

            tvRemarkDetail = (TextView) itemView.findViewById(R.id.tvDetails);
            tvRemarks = (TextView) itemView.findViewById(R.id.tvRemarks);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            viewStatus = (View) itemView.findViewById(R.id.viewStatus);

            itemView.setOnClickListener(this);
        }

        @Override
        public Object getObject() {
            return ICheckItemList.get(getAdapterPosition());

        }

        @Override
        public void setViews(Object object) {
            ICheckItem ICheckItem = (ICheckItem) object;

            tvRemarks.setText(ICheckItem.getRemark());
            // TODO: 20/12/2017 temp setting list item
            String projectname  = "";
            String areaname     = "";
            String deckname     = "";
            String discipline   = "";
            String actionowner  = "";

            Project selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class, DatabaseConstants.KEY_PROJECT_ID, ICheckItem.getProjectId());

                if(selectedProject!=null){
                    projectname = selectedProject.getProjectName();
                }

                if(ICheckItem.getArea()!=null){
                    areaname = ICheckItem.getArea();
                }else{
                    areaname = CommonConstants.GENERAL_VALUE;
                }

                if(ICheckItem.getDeck()!=null){
                    deckname = ICheckItem.getDeck();
                }else{
                    deckname = CommonConstants.GENERAL_VALUE;
                }

                if(ICheckItem.getDiscipline()!=null){
                    discipline = ICheckItem.getDiscipline();
                }else{
                    discipline = CommonConstants.GENERAL_VALUE;
                }

                if(ICheckItem.getActionOwner()!=null){
                    actionowner = ICheckItem.getActionOwner();
                }

            tvRemarkDetail.setText(String.format("%s - %s - %s - %s - %s",
                    projectname,
                    areaname,
                    deckname,
                    discipline,
                    actionowner
            ));

            //todo temp remove created date
            tvDate.setText(ICheckItem.getStatus());
//
//            if (DateHelper.isOvertime(ICheckItem.getDueDate())){
//
//                viewStatus.setVisibility(View.VISIBLE);
//                tvRemarkDetail.setTextColor(ContextCompat.getColor(context,R.color.red_orange));
//            }else {
//                viewStatus.setVisibility(View.GONE);
//                tvRemarkDetail.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
//
//            }
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()){
//            case R.id.ivPin:
//
//                listener.onRecyclerItemClick(-1,getObject());
//                break;

                default:
                    super.onClick(view);
                    break;
            }
        }

    }


    public void filter(String text) {

        ICheckItemList.clear();
        if(text.isEmpty()){
            ICheckItemList.addAll(ICheckItemCopy);
        } else{
            text = text.toLowerCase();

            // TODO: 20/12/2017 add more filter
            for(ICheckItem item: ICheckItemCopy){

                if (item.getArea()!=null){
                    if (item.getArea().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getPriority()!=null){
                    if (item.getPriority().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getDeck()!=null){
                    if (item.getDeck().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getDiscipline()!=null){
                    if (item.getDiscipline().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getActionOwner()!=null){
                    if (item.getActionOwner().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }


                if (item.getDateAdded()!=null){
                    if (item.getDateAdded().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }


                if (item.getRemark()!=null){
                    if (item.getRemark().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }


                if (item.getAddedBy()!=null){
                    if (item.getAddedBy().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                if (item.getComment()!=null){
                    if (item.getComment().toLowerCase().contains(text)){

                        ICheckItemList.add(item);
                        continue;
                    }
                }

                Log.v("lester-log-filter", String.valueOf(item));

            }
        }
        notifyDataSetChanged();
    }

}

