package com.rcclpmo.icheck.controllers.addAction;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.helpers.DateHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar c = Calendar.getInstance();

        Bundle data = getArguments();
        String selectedDate = data.getString(CommonConstants.KEY_DATE);
        if (selectedDate!=null){
            if (!selectedDate.equals("")) {
                c.setTime(DateHelper.covertStringToDate(selectedDate));
            }
//            else {
//                c.add(Calendar.DATE, 20);
//            }

        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }


    public void onDateSet(DatePicker view, int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = format.format(calendar.getTime());

        ActivityCheckItem activity = (ActivityCheckItem) getActivity();
        activity.onDueDateSelected(strDate);
        dismiss();
    }
}