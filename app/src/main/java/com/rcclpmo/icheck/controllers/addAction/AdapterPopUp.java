package com.rcclpmo.icheck.controllers.addAction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseViewHolder;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.People;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class AdapterPopUp<T> extends RecyclerView.Adapter<AdapterPopUp.ItemViewHolder> {

    private List<T> list, listCopy;
    private RecyclerViewClickListener<Object> listener;

    public AdapterPopUp(List<T> list, RecyclerViewClickListener<Object> listener){

        this.list = list;
        this.listCopy = new ArrayList<>();
        this.listCopy.addAll(list);
        this.listener = listener;
    }

    @Override
    public AdapterPopUp.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_pop_up_item,parent,false);
        return new AdapterPopUp.ItemViewHolder(view, listener);

    }

    @Override
    public void onBindViewHolder(AdapterPopUp.ItemViewHolder holder, int position) {
        holder.setViews(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ItemViewHolder extends BaseViewHolder<Object> {
        private TextView tvItem;

        public ItemViewHolder(View itemView, RecyclerViewClickListener<Object> listener) {
            super(itemView, listener);

            tvItem = (TextView) itemView.findViewById(R.id.tvItem);

            itemView.setOnClickListener(this);
        }

        @Override
        public Object getObject() {
            return list.get(getAdapterPosition());
        }

        @Override
        public void setViews(Object object) {

            String item = "";

            if (object instanceof People){

                item = ((People) object).getName();

            } else if (object instanceof Deck){
                item = ((Deck) object).getDeckName();

            }else if (object instanceof Discipline){
                item = ((Discipline) object).getDiscipline();

            }else if (object instanceof Area){
                item = ((Area) object).getAreaName();

            }

            tvItem.setText(item);
        }
    }

    public void filter(String text) {
        list.clear();
        if(text.isEmpty()){
            list.addAll(listCopy);
        } else{
            text = text.toLowerCase();

            for(T item: listCopy){

                if (item instanceof Discipline){
                    if(((Discipline) item).getDiscipline().toLowerCase().contains(text)){
                        list.add(item);
                    }
                }

                if (item instanceof Area){
                    if(((Area) item).getAreaName().toLowerCase().contains(text)){
                        list.add(item);
                    }
                }

                if (item instanceof People){
                    if(((People) item).getName().toLowerCase().contains(text)){
                        list.add(item);
                    }
                }

                if (item instanceof Deck){
                    if(((Deck) item).getDeckName().toLowerCase().contains(text)){
                        list.add(item);
                    }
                }


            }
        }
        notifyDataSetChanged();
    }
}