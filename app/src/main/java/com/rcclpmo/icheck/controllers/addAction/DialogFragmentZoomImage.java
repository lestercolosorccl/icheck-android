package com.rcclpmo.icheck.controllers.addAction;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.bases.BaseDialogFragment;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.customviews.CustomZoomImageView;
import com.rcclpmo.icheck.helpers.FileHelper;

import java.io.File;
import java.io.IOException;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DialogFragmentZoomImage extends BaseDialogFragment {

    private CustomZoomImageView ivPreview;
    private Button btnClose;

    private String bgPath;

    public static DialogFragmentZoomImage createInstance(@Nullable Bundle bundle){
        DialogFragmentZoomImage fragment = new DialogFragmentZoomImage();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void initMain() {
        setLayoutId(R.layout.dialog_fragment_zoom);
    }

    @Override
    public void initData(Bundle bundle) {
        bgPath = bundle.getString(CommonConstants.IMAGE_FILE_FORMAT);


    }

    @Override
    protected boolean hasToolbar() {
        return false;
    }

    @Override
    protected boolean isCustomDialog() {
        return true;
    }

    @Override
    protected void handleCommonAPI(int requestCode) {

    }

    @Override
    public void initViews(View view, Bundle savedInstanceState) {
        View viewToAnimate = view.findViewById(R.id.rlParent);
        setViewToAnimate(viewToAnimate);
        setAnimationEnter(R.anim.anim_slide_up_enter);
        setAnimationExit(R.anim.anim_slide_down_exit);

        ivPreview = (CustomZoomImageView) view.findViewById(R.id.ivPreview);
        btnClose = (Button) view.findViewById(R.id.btnClose);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViews();

    }

    private void setViews(){
        if (new File(bgPath).isFile()) {
//            Bitmap bmp = BitmapFactory.decodeFile(bgPath);
//            Bitmap bmp = FileHelper.decodeSampledBitmapFromFile(bgPath,4200,4200);
//            ivPreview.setImageBitmap(bmp);

            ivPreview.setImageBitmap(rotateImage(bgPath));

        }

    }

    private Bitmap rotateImage(String bgPath){
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        Bitmap bm = FileHelper.decodeSampledBitmapFromFileWithBounds(bgPath,500,600,bounds);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(bgPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);

        return rotatedBitmap;
    }




}

