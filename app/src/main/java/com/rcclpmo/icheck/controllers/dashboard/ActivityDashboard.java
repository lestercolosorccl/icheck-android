package com.rcclpmo.icheck.controllers.dashboard;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.api.UserAPI;
import com.rcclpmo.icheck.bases.BaseActivity;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.CommonConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.controllers.addAction.ActivityCheckItem;
import com.rcclpmo.icheck.controllers.login.ActivityLogin;
import com.rcclpmo.icheck.controllers.reviewList.ActivityICheckList;
import com.rcclpmo.icheck.controllers.reviewList.DialogFragmentOptionSelection;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.helpers.PermissionHelper;
import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.ChartData;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.Supplier;
import com.rcclpmo.icheck.models.User;
import com.rcclpmo.icheck.receivers.ConnectivityReceiver;
import com.rcclpmo.icheck.services.DashboardAPIRequestService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ActivityDashboard extends BaseActivity implements RecyclerViewClickListener<Object> {

    /****
     * CONSTANTS
     */
    private static final int REQUEST_CODE_PERMISSIONS = 2001;
    //// TODO: 04/09/2017 add another permission here

    /****
     * CLASSES
     */
    private ApplicationClass applicationClass;
    private RequestQueue requestQueue;
    private SyncDBTransaction dbTransaction;

    private RelativeLayout rlLoader;
    private Button btnAddAction, btnReviewList;
    private Button btnPriority, btnStatus, btnImpact;
    private RelativeLayout relativeParent;
    private TextView tvUser;
    private TextView btnSelectProject;
    private TextView tvLoader;
    private LinearLayout llShipSelection, llPieToggle;
    private PieChart pieChartStatus;
    private ImageView ivLogout;

    private List<Project> projectList;
    private User user;
    private Project selectedProject;
    private int selectedButtonId;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private int projectPosition=0;
    private List<PieEntry> entryList;
    private List<String> pieStatusLabelList;
    private List<ChartData> dataList;
    private PieDataSet pieDataSet;
    private PieData pieData;
    private String selectedColumn = CommonConstants.COLUMN_TYPE_STATUS;
    private String noColumnData = CommonConstants.STATUS_NO_STATUS;

    @Override
    protected void initData(Intent data) {

        projectList = new ArrayList<>();
        entryList = new ArrayList<>();
        pieStatusLabelList = new ArrayList<>();
        dataList = new ArrayList<>();

        dbTransaction = new SyncDBTransaction(getApplicationContext());
        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();

        user = (User) dbTransaction.getFirstObject(User.class);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dashboard_2;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setViews();
//
//        if (hasToBeSynced()) {
//            if (ConnectivityReceiver.isConnected()) {
//                showSnycDialog(null);
//
//            }
//        }

        }


    @Override
    protected void initViews() {

        rlLoader = (RelativeLayout) findViewById(R.id.rlLoader);
        relativeParent = (RelativeLayout) findViewById(R.id.relativeParent);
        llPieToggle = (LinearLayout) findViewById(R.id.llPieToggle);
        btnAddAction = (Button) findViewById(R.id.btnAddNewAction);
        btnReviewList = (Button) findViewById(R.id.btnReviewList);
        btnSelectProject = (TextView) findViewById(R.id.btnProject);
        btnImpact = (Button) findViewById(R.id.btnImpact);
        btnPriority = (Button) findViewById(R.id.btnPriority);
        btnStatus = (Button) findViewById(R.id.btnStatus);
        llShipSelection = (LinearLayout) findViewById(R.id.llShipSelection);
        tvUser = (TextView) findViewById(R.id.tvUser);
        tvLoader = (TextView) findViewById(R.id.tvLoader);
        pieChartStatus = (PieChart) findViewById(R.id.pieChartStatus);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);

        initListeners();

    }

    private void initListeners(){
        btnAddAction.setOnClickListener(this);
        btnReviewList.setOnClickListener(this);
        btnSelectProject.setOnClickListener(this);
        llShipSelection.setOnClickListener(this);
        btnPriority.setOnClickListener(this);
        btnStatus.setOnClickListener(this);
        btnImpact.setOnClickListener(this);
        ivLogout.setOnClickListener(this);

    }

    private void setViews(){

        tvUser.setText(String.format("%s %s",user.getFirstName(), user.getLastName()));

        Project selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class,
                DatabaseConstants.KEY_PROJECT_ID, applicationClass.getShipId());

        if (selectedProject!=null){

            this.selectedProject = selectedProject;
            btnSelectProject.setText(selectedProject.getProjectName());
            llPieToggle.setVisibility(View.VISIBLE);
            requestGetChartData();

        }

    }

    private void toggleButtons(Button selectedBtn){
        btnImpact.setBackgroundResource(android.R.color.transparent);
        btnImpact.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_text));
        btnStatus.setBackgroundResource(android.R.color.transparent);
        btnStatus.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_text));
        btnPriority.setBackgroundResource(android.R.color.transparent);
        btnPriority.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_text));

        selectedBtn.setBackgroundResource(R.drawable.bg_gradient);
        selectedBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.white));

    }

    private void showSnackbar(){
        Snackbar snackbar = Snackbar
                .make(relativeParent, "Permission Denied!", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkPermission();

                    }
                });

        snackbar.setActionTextColor(Color.GREEN);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    private void checkPermission(){

        Log.i("ICHECK_TAG", String.valueOf(!PermissionHelper.hasPermissions(getApplicationContext(), PERMISSIONS)));

        if(!PermissionHelper.hasPermissions(getApplicationContext(), PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_CODE_PERMISSIONS);

        }else {

            switch (selectedButtonId) {

                case R.id.btnAddNewAction:
                    addNewAction(null);
                    break;

                case R.id.btnReviewList:
                    goToListView();
                    break;

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        Log.i("ICHECK_TAG", String.valueOf(grantResults.length));

        switch (requestCode){
            case REQUEST_CODE_PERMISSIONS: {

                boolean isAllGranted = true;
                if (grantResults.length > 0) {
                    for (int result : grantResults) {
                        if (result == PackageManager.PERMISSION_DENIED) {
                            isAllGranted = false;
                        }

                    }

                }

                if (isAllGranted) {
                    switch (selectedButtonId) {

                        case R.id.btnAddNewAction:
                            addNewAction(null);
                            break;

                        case R.id.btnReviewList:
                            goToListView();
                            break;
                    }

                } else {

                    showSnackbar();
                }

                return;
            }

            default:break;


        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        loadLocalProjectList();
        if (applicationClass.getShipId()!=null || projectList.size()!=0) {

            setSelectedProject();
            requestViaService();

            if (hasToBeSynced()) {
                if (ConnectivityReceiver.isConnected()) {
                    showSnycDialog(null);

                }
            }

        }else {
            requestGetProjects();

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        requestQueue.cancelAll(APIRequestCode.REQUEST_TAG);

    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
//      // TODO: 04/10/2017 to be uncomment
//        if (spnSelectProject.getSelectedItem()==null){
//            return;
//        }else {
//            saveSelectedProject();
//        }

        selectedButtonId = view.getId();
        switch (view.getId()){

            case R.id.btnAddNewAction:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    checkPermission();

                }else {
                    addNewAction(null);

                }
                break;

            case R.id.btnReviewList:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    checkPermission();

                }else {
                    goToListView();

                }
                break;

            case R.id.btnProject:
                Bundle bundle = new Bundle();
                bundle.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PROJECT);
                bundle.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_DASHBOARD);
                showOption(bundle);
                break;

            case R.id.llShipSelection:
                Bundle bundle2 = new Bundle();
                bundle2.putInt(CommonConstants.DATA_KEY_OPTION_TYPE, CommonConstants.OPTION_TYPE_PROJECT);
                bundle2.putInt(CommonConstants.DATA_KEY_ACTIVITY, CommonConstants.ACTIVITY_DASHBOARD);
                showOption(bundle2);
                break;

            case R.id.btnImpact:
                toggleButtons(btnImpact);
                selectedColumn = CommonConstants.COLUMN_TYPE_IMPACT;
                requestGetChartData();
                break;

            case R.id.btnPriority:
                toggleButtons(btnPriority);
                selectedColumn = CommonConstants.COLUMN_TYPE_PRIORITY;
                requestGetChartData();
                break;

            case R.id.btnStatus:
                toggleButtons(btnStatus);
                selectedColumn = CommonConstants.COLUMN_TYPE_STATUS;
                requestGetChartData();
                break;

            case R.id.ivLogout:
                showLogoutDialog();
                break;

            default:break;
        }
    }

    @Override
    public void onRecyclerItemClick(int position, Object object) {

        if (object instanceof Project){

            saveSelectedProject(object);
            btnSelectProject.setText(selectedProject.getProjectName());
            llPieToggle.setVisibility(View.VISIBLE);
            requestGetChartData();

        }

    }

    private void showLogoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.logout));
        builder.setMessage("Are you sure? All of your data will be deleted.");

        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteDB();
                logout();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteDB(){
        dbTransaction.deleteAll(Project.class);
        dbTransaction.deleteAll(Discipline.class);
        dbTransaction.deleteAll(Attachment.class);
        dbTransaction.deleteAll(People.class);
        dbTransaction.deleteAll(Deck.class);
        dbTransaction.deleteAll(User.class);
        dbTransaction.deleteAll(ICheckItem.class);
        dbTransaction.deleteAll(Supplier.class);
        dbTransaction.deleteAll(Area.class);
    }

    private void logout(){
        applicationClass.deleteTokens();
        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);
        finish();
    }

    private void showOption(Bundle bundle){
        DialogFragmentOptionSelection dialog = DialogFragmentOptionSelection.createInstance(bundle);
        dialog.show(getSupportFragmentManager(),"create dialog");
    }


    private void showSnycDialog(@Nullable Bundle bundle){
        Log.i("SHOW_DialogFragmentSync","DialogFragmentSync" );
        DialogFragmentSync dialog = DialogFragmentSync.createInstance(bundle);
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(),"create dialog");
    }

    private boolean hasToBeSynced(){
        boolean toBeSynced = false;

        List<ICheckItem> remarks = dbTransaction.getUnsyncObjects(ICheckItem.class);
        if (remarks!=null){
            if (remarks.size()>0){
                toBeSynced = true;
            }
        }

        List<Attachment> attachments = (List<Attachment>) dbTransaction.getUnsyncAttachments(Attachment.class);
        if (attachments!=null){
            if (attachments.size()>0){
                toBeSynced = true;
            }
        }

        List<Attachment> deletedAttachments = (List<Attachment>) dbTransaction.getDeletedObjects(Attachment.class);
        if (deletedAttachments!=null){
            if (deletedAttachments.size()>0){
                toBeSynced = true;
            }
        }

        return toBeSynced;
    }

    private void requestViaService(){
        Intent i= new Intent(this, DashboardAPIRequestService.class);
        startService(i);
    }

    private void saveSelectedProject(Object object){
        selectedProject = (Project) object;
        applicationClass.saveShipId(selectedProject.getProjectId(),selectedProject.getProjectName());
//        applicationClass.saveProjectId(selectedProject.getShipId());
        //set the ship name
        //set selected ship in the toolbar

    }

    private void addNewAction(@Nullable String id){
        Intent intent = new Intent(getApplicationContext(), ActivityCheckItem.class);
        intent.putExtra(CommonConstants.KEY_IS_ADD_REMARK, true);
        startActivity(intent);
    }

    private void goToListView(){
        Intent intent = new Intent(getApplicationContext(), ActivityICheckList.class);
        startActivity(intent);
    }

    @Override
    protected void handleRefreshToken() {

    }

    @Override
    protected void handleCommonAPI(int requestCode) {

    }

    @Override
    protected void handleNotification(String message) {
        Snackbar snackbar = Snackbar.make(rlLoader,message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.grey_300));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.amber_500));
        snackbar.show();
    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        Log.v("lester-request-code", String.valueOf(requestCode));

        switch (requestCode) {

            case APIRequestCode.CODE_GET_PROJECT_LIST:
                handleListResponse(object);
                loadLocalProjectList();
                showLoader(false,"");

                projectPosition = 0;
                  requestGetAreas();
                break;

            case APIRequestCode.CODE_GET_AREAS:
                handleListResponse(object);
                showLoader(false,"");
                requestGetAllDisciplines();
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_DECKS:
                handleListResponse(object);
                showLoader(false,"");
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_ALL_DISCIPLINES:
                handleListResponse(object);
                showLoader(false,"");
                requestGetUsers();
                break;

            case APIRequestCode.CODE_GET_USERS:
                handleListResponse(object);
                showLoader(false,"");
                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_SUPPLIERS:
                handleListResponse(object);
                showLoader(false,"");
                break;

            case APIRequestCode.CODE_GET_CHART_DATA:
                handleGetPieData(object);
                generatePieChart();
                showLoader(false,"");
                break;


            default:
                break;

        }
    }


    @Override
    public void onFailed(int requestCode, String message) {
        super.onFailed(requestCode, message);

        Log.v("lester-request-code", String.valueOf(requestCode));


        switch (requestCode) {

            case APIRequestCode.CODE_GET_PROJECT_LIST:
                showLoader(false,"");
                loadLocalProjectList();

                projectPosition = 0;
                requestGetAreas();
                break;


            case APIRequestCode.CODE_GET_AREAS:
                showLoader(false,"");
                requestGetAllDisciplines();
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_DECKS:
                showLoader(false,"");
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_ALL_DISCIPLINES:
                showLoader(false,"");
                requestGetUsers();
                break;

            case APIRequestCode.CODE_GET_USERS:
                showLoader(false,"");
                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_SUPPLIERS:
                showLoader(false,"");
                break;

            case APIRequestCode.CODE_GET_CHART_DATA:
                handlePieDataOffline();
                generatePieChart();
                showLoader(false,"");
                break;


            default:
                break;

        }
    }

    private void showLoader(boolean isShown, String loading){
        rlLoader.setVisibility(isShown? View.VISIBLE: View.GONE);
        tvLoader.setText(loading);
    }

    private void requestGetProjects(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_PROJECT);
        Request requestGetAllProjects = CommonAPI.getProjectList(APIRequestCode.CODE_GET_PROJECT_LIST,this);
        requestGetAllProjects.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(requestGetAllProjects);

    }

    private void loadLocalProjectList(){
        projectList.clear();
        projectList.addAll(dbTransaction.getAll(Project.class));

    }

    private void setSelectedProject(){

        if (applicationClass.getShipId()!=null){
            selectedProject = (Project) dbTransaction.getDynamicRealmObject(Project.class,
                    DatabaseConstants.KEY_PROJECT_ID, applicationClass.getShipId());

            btnSelectProject.setText(selectedProject.getProjectName());

        }

    }

    private void requestGetAreas(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_AREAS);
        Request request = CommonAPI.getAreaVenues(APIRequestCode.CODE_GET_AREAS,this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void handleListResponse(Object object){
        dbTransaction.add((List) object);

    }

    private void requestGetDecks(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_DECKS);
        Request request = CommonAPI.getDecks(APIRequestCode.CODE_GET_DECKS, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void requestGetAllDisciplines(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_DISCIPLINES);
        Request request = CommonAPI.getAllDisciplines(APIRequestCode.CODE_GET_ALL_DISCIPLINES, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }

    private void requestGetSuppliers(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_SUPPLIERS);
        Request request = CommonAPI.getSuppliers(APIRequestCode.CODE_GET_SUPPLIERS, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }


    private void requestGetUsers(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_OWNER_LIST);
        Request request = UserAPI.getUsers(APIRequestCode.CODE_GET_USERS, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);
    }

    private void requestGetChartData(){
        showLoader(true, CommonConstants.LOADER_DOWNLOADING_CHART_DATA);
        Request request = CommonAPI.getChartData(APIRequestCode.CODE_GET_CHART_DATA,selectedProject.getProjectId(), selectedColumn, this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        requestQueue.add(request);

    }


    //todo temp
    private void handleGetPieData(Object object){
        dataList = (List<ChartData>) object;
        if (selectedColumn.equals(CommonConstants.COLUMN_TYPE_STATUS)){
            noColumnData = CommonConstants.STATUS_NO_STATUS;

        }else if (selectedColumn.equals(CommonConstants.COLUMN_TYPE_PRIORITY)){
            noColumnData = CommonConstants.NO_PRIORITY;

        }else{
            noColumnData = CommonConstants.NO_IMPACT;

        }


    }

    //todo temp
    private void handlePieDataOffline(){

        dataList.clear();
        // TODO: 05/12/2017 get all actions by status
//        List<Action> completedAction = dbTransaction.getActionsByStatus()

        List<String> columnList = new ArrayList<>();

        if (selectedColumn.equals(CommonConstants.COLUMN_TYPE_STATUS)){

            columnList = Arrays.asList(getResources().getStringArray(R.array.status));
            noColumnData = CommonConstants.STATUS_NO_STATUS;

        }else if (selectedColumn.equals(CommonConstants.COLUMN_TYPE_PRIORITY)){

            columnList = Arrays.asList(getResources().getStringArray(R.array.priority));
            noColumnData = CommonConstants.NO_PRIORITY;

        }else{

            columnList = Arrays.asList(getResources().getStringArray(R.array.impact));
            noColumnData = CommonConstants.NO_IMPACT;
        }

        int noStatus = 0;

        for (String item: columnList){

            List<ICheckItem> itemList = dbTransaction.getItemsByColumnAndShipId(ICheckItem.class,
                    selectedProject.getProjectId(), selectedColumn, item);

            if (itemList.size()>0){
                if (item.equals(noColumnData)){
                    noStatus = noStatus + itemList.size();

                }else {
                    ChartData dataItem = new ChartData();
                    dataItem.setCount(itemList.size());
                    dataItem.setItem(item);
                    this.dataList.add(dataItem);

                }

            }

        }

        //for empty and null status
        List<ICheckItem> actionNull = dbTransaction.getItemsByColumnAndShipId(ICheckItem.class,
                selectedProject.getProjectId(), selectedColumn, null);
        List<ICheckItem> actionEmpty = dbTransaction.getItemsByColumnAndShipId(ICheckItem.class,
                selectedProject.getProjectId(), selectedColumn, "");

        if (actionNull.size()>0 || actionEmpty.size()>0){
            noStatus = noStatus + actionNull.size() + actionEmpty.size();
        }

        if (noStatus > 0){
            ChartData status = new ChartData();
            status.setCount(noStatus);
            status.setItem(noColumnData);
            this.dataList.add(status);

        }

    }

    private void generatePieChart(){

        entryList.clear();
        int x =0;
        int noStatusCount = 0;
        for (ChartData dataItem: dataList){
            //todo temp
            if (dataItem.getItem()==null){
                noStatusCount = noStatusCount + dataItem.getCount();
            }else if (dataItem.getItem().equals("")){
                noStatusCount = noStatusCount + dataItem.getCount();

            }else {
                entryList.add(new PieEntry(dataItem.getCount(),dataItem.getItem(), x));

            }
            x= x+1;

        }
        if (noStatusCount!=0){
            entryList.add(new PieEntry(noStatusCount,noColumnData, 20 ));

        }

        //return if no data
        if (entryList.isEmpty()){
            return;
        }

        pieDataSet = new PieDataSet(entryList, "");


        ArrayList<Integer> colors = new ArrayList<Integer>();
        Random rnd;
        //temp colors
        for (PieEntry pieEntry: entryList){

            rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            if (pieEntry.getLabel().equalsIgnoreCase("Cancelled")){

                colors.add(Color.parseColor("#9E9E9E"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Completed")){
                colors.add(Color.parseColor("#4CAF50"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("In Progress")){
                colors.add(Color.parseColor("#FFEB3B"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Info Only")){
                colors.add(Color.parseColor("#BDBDBD"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Not Started")){
                colors.add(Color.parseColor("#2196F3"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Overdue")){
                colors.add(Color.parseColor("#F44336"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Done")){
                colors.add(Color.parseColor("#CDDC39"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Not Reported")){
                colors.add(Color.parseColor("#3F51B5"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("WIP")){
                colors.add(Color.parseColor("#FFC107"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("low")){
                colors.add(Color.parseColor("#FFEB3B"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("Medium")){
                colors.add(Color.parseColor("#FF9800"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("High")){
                colors.add(Color.parseColor("#F44336"));

            }else if (pieEntry.getLabel().equalsIgnoreCase("N/A")){
                colors.add(Color.parseColor("#607D8B"));
            }else{
                colors.add(color);
            }

            //TODO add COLOR OTHER TYPES
        }

        pieDataSet.setColors(colors);

        pieData = new PieData(pieDataSet);
        pieData.setValueTextColor(Color.parseColor("#000000"));
        pieData.setValueTextSize(11f);
        pieDataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        Legend legend = pieChartStatus.getLegend();
        legend.setEnabled(true);
        legend.setTextColor(Color.parseColor("#000000"));
        legend.setWordWrapEnabled(true);

        pieChartStatus.getDescription().setEnabled(false);
        pieChartStatus.setData(pieData);
        pieChartStatus.setDrawHoleEnabled(false);
        pieChartStatus.setDrawCenterText(true);
        pieChartStatus.setCenterTextColor(Color.WHITE);
        pieChartStatus.setCenterText(String.format("Total per %s",selectedColumn.toUpperCase()));
        pieChartStatus.setCenterTextSize(15f);
        pieChartStatus.setEntryLabelColor(Color.parseColor("#000000"));
        pieChartStatus.setData(pieData);
        pieChartStatus.setDrawHoleEnabled(false);
        pieChartStatus.animateY(1000);


    }

}



