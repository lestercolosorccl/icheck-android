package com.rcclpmo.icheck.controllers.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.FirebaseAPI;
import com.rcclpmo.icheck.api.UserAPI;
import com.rcclpmo.icheck.bases.BaseActivity;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.controllers.dashboard.ActivityDashboard;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.models.Login;
import com.rcclpmo.icheck.models.OAuthentication;
import com.rcclpmo.icheck.models.User;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ActivityLogin extends BaseActivity {

    private Button btnLogin;
    private EditText etEmail;
    private EditText etPassword;

    private RequestQueue requestQueue;
    private ApplicationClass applicationClass;
    private SyncDBTransaction dbTransaction;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();
        dbTransaction = new SyncDBTransaction(this);

        if (applicationClass.isLoggedIn()){
            goToDashboard();

//            if (applicationClass.isFirebaseTokenSavedToServer()){
//                goToDashboard();
//
//            }else {
//                requestSaveFirebaseToken();
//
//            }
        }

    }

    @Override
    protected void initData(Intent data) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {


        btnLogin = (Button) findViewById(R.id.btnLogin);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        setListeners();
        initBroadcastReceiver();
    }

    private void initBroadcastReceiver(){

    }

    private void setListeners(){
        btnLogin.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestQueue.cancelAll(APIRequestCode.TAG_LOGIN);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrationBroadcastReceiver);

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()){

            case R.id.btnLogin:
                if (isValid()){
                    requestLogin();
                }
                break;

            default:
                break;

        }
    }

    private void goToDashboard(){
        Intent intent = new Intent(this, ActivityDashboard.class);
        startActivity(intent);
        finish();
    }

    private boolean isValid(){
        if (etEmail.getText().toString().equals("") ||
                etPassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Fields cannot be empty.", Toast.LENGTH_SHORT).show();
            return false;

        }else {
            return true;

        }
        // TODO: 12/7/2016 add more validations if needed
    }

    private void showLoader(boolean isLoading){
        View view= findViewById(R.id.viewBackground);
        view.setVisibility(isLoading? View.VISIBLE:View.GONE);
        btnLogin.setText(isLoading? getString(R.string.login_logging_in): getString(R.string.login));

    }

    private void requestLogin(){
        OAuthentication oAuth = new OAuthentication();
        oAuth.setEmail(etEmail.getText().toString());
        oAuth.setPassword(etPassword.getText().toString());


        showLoader(true);
        Request requestLogin = UserAPI.login(APIRequestCode.CODE_LOGIN, oAuth, this);
        requestLogin.setTag(APIRequestCode.TAG_LOGIN);
        requestQueue.add(requestLogin);
    }


    private void handleSuccessfulLogin(Object object){
        Login login = (Login) object;
        saveToken(login);
        User user = login.getUser();
        dbTransaction.add(user);

        saveUserId(user.getId());
        saveDeviceId();

    }

    private void saveToken(Login login){
        applicationClass.saveToken(login.getAccessToken());
    }

    private void saveUserId(String userId){
        applicationClass.saveUserId(userId);
    }

    private void saveDeviceId(){
        String deviceId = Build.SERIAL != Build.UNKNOWN ?
                Build.SERIAL : Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        applicationClass.saveDeviceId(deviceId);
    }

    private void handleError(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT ).show();

    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        super.onSuccess(requestCode, object);

        switch (requestCode){

            case APIRequestCode.CODE_LOGIN:
                handleSuccessfulLogin(object);
//                requestSaveFirebaseToken();
                goToDashboard();

                break;

            case APIRequestCode.CODE_FIREBASE_SAVE_TOKEN:
                showLoader(false);

                Log.i("FIREBASE_TAG", "sendRegistrationToServer: " +object);
                applicationClass.setFirebaseTokenSaved("true");
                goToDashboard();
                break;

            default:
                break;

        }
    }

    @Override
    public void onFailed(int requestCode, String message) {
        super.onFailed(requestCode, message);

        switch (requestCode){

            case APIRequestCode.CODE_LOGIN:
                handleError(message);
                showLoader(false);
                break;

            case APIRequestCode.CODE_FIREBASE_SAVE_TOKEN:
                handleError(message);
                Log.i("FIREBASE_TAG", "sendRegistrationToServer: " +message);
                showLoader(false);

                break;
        }
    }

    @Override
    protected void handleRefreshToken() {
        requestLogin();
    }

    @Override
    protected void handleCommonAPI(int requestCode) {

    }

    @Override
    protected void handleNotification(String message) {

    }

    private void requestSaveFirebaseToken(){
        Request request = FirebaseAPI.saveFirebaseToken(APIRequestCode.CODE_FIREBASE_SAVE_TOKEN,this);
        request.setTag(APIRequestCode.REQUEST_TAG);
        showLoader(true);
        requestQueue.add(request);
    }


}
