package com.rcclpmo.icheck.api;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.helpers.VolleyPostRequestHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.APIResponse;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.utilities.GSONUtility;
import com.rcclpmo.icheck.utilities.SocketTimeOut;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class iCheckAPI {

    public static Request getICheckItems(final int requestCode, final ICheckItemData remarkData, final ResponseHandler responseHandler){

        /****
         * note: change the endpoint to getAllWalkThruRemarks
         */
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.ICHECK_URL,
                APIConstants.API_GET_ITEMS);

        //// TODO: 20/12/2017 add more params
        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.PARAM_PROJECT_ID, remarkData.getProjectId());
        params.put(APIConstants.BULK_PARAM_DECK_ID, remarkData.getClaimOwner());
        params.put(APIConstants.PARAM_AREA_ID, remarkData.getAreaId());
        params.put(APIConstants.PARAM_DISCIPLINE_ID, remarkData.getDisciplineId());
        params.put(APIConstants.PARAM_STATUS, remarkData.getStatus());
        params.put(APIConstants.PARAM_PRIORITY, remarkData.getPriority());
        params.put(APIConstants.PARAM_ACTION_OWNER, remarkData.getClaimOwner());


        Log.v("lester-log-getall", String.valueOf(params));
        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){
                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<ICheckItem>>() {
                    }.getType();

                    List<ICheckItem> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request saveICheckItem(final int requestCode, ICheckItemData item, final ResponseHandler responseHandler){

        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.ICHECK_URL,
                APIConstants.SAFETY_FIRST_SAVE_ICHECK_ITEM);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.PARAM_PROJECT_ID, item.getProjectId());
        params.put(APIConstants.PARAM_REMARK, item.getRemark());
        params.put(APIConstants.PARAM_AREA_ID, item.getAreaId());
        params.put(APIConstants.PARAM_DECK_ID, item.getDeckId());
        params.put(APIConstants.BULK_PARAM_REMARKS_TYPE, item.getRemarkType());
        params.put(APIConstants.PARAM_PRIORITY, item.getPriority());
        params.put(APIConstants.PARAM_STATUS, item.getStatus());
        params.put(APIConstants.PARAM_DISCIPLINE_ID, item.getDisciplineId());
        params.put(APIConstants.PARAM_ACTION_OWNER, item.getActionOwnerId());
        params.put(APIConstants.PARAM_COMMENT, item.getComment());
        params.put(APIConstants.BULK_PARAM_DUE_DATE, item.getDueDate());



        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    responseHandler.onSuccess(requestCode, apiResponse.getData());

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request updateICheckItem(final int requestCode, ICheckItemData item, final ResponseHandler responseHandler){

        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.ICHECK_URL,
                APIConstants.UPDATE_ICHECK_ITEM);


        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.PARAM_PROJECT_ID, item.getProjectId());
        params.put(APIConstants.PARAM_ID, item.getId());
        params.put(APIConstants.PARAM_REMARK, item.getRemark());
        params.put(APIConstants.PARAM_AREA_ID, item.getAreaId());
        params.put(APIConstants.PARAM_DECK_ID, item.getDeckId());
        params.put(APIConstants.BULK_PARAM_REMARKS_TYPE, item.getRemarkType());
        params.put(APIConstants.PARAM_PRIORITY, item.getPriority());
        params.put(APIConstants.PARAM_STATUS, item.getStatus());
        params.put(APIConstants.PARAM_DISCIPLINE_ID, item.getDisciplineId());
        params.put(APIConstants.PARAM_ACTION_OWNER, item.getActionOwnerId());
        params.put(APIConstants.PARAM_COMMENT, item.getComment());
        params.put(APIConstants.BULK_PARAM_DUE_DATE, "2018-04-28");

        Log.v("lester-log-url-update", String.valueOf(url));
        Log.v("lester-log-url-update", String.valueOf(params));

        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    responseHandler.onSuccess(requestCode, apiResponse.getMessage());

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getAttachments(final int requestCode, final String id, final ResponseHandler responseHandler){


        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?" +
                        "%s=%s" +
                        "&%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.DISPOSITION_LIST_GET_ATTACHMENTS,
                APIConstants.DISPOSITION_LIST_PARAM_IDS, id,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());




        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Attachment>>() {
                    }.getType();

                    List<Attachment> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

}
