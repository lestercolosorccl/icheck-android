package com.rcclpmo.icheck.api;

import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.helpers.InputStreamRequestHelper;
import com.rcclpmo.icheck.helpers.VolleyMultiPartRequestHelper;
import com.rcclpmo.icheck.helpers.VolleyPostRequestHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.APIResponse;
import com.rcclpmo.icheck.models.Area;
import com.rcclpmo.icheck.models.ChartData;
import com.rcclpmo.icheck.models.Deck;
import com.rcclpmo.icheck.models.Discipline;
import com.rcclpmo.icheck.models.Project;
import com.rcclpmo.icheck.models.Supplier;
import com.rcclpmo.icheck.models.UploadedAttachment;
import com.rcclpmo.icheck.utilities.GSONUtility;
import com.rcclpmo.icheck.utilities.SocketTimeOut;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class CommonAPI {

    public static Request getProjectList(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_PROJECT,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());


//        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
//                APIConstants.COMMON_MOBILE,APIConstants.API_GET_SHIP_LIST,
//                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());

        Log.v("lester-log", url);
        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Project>>() {
                    }.getType();

                    List<Project> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getChartData(final int requestCode, final String projectId, final String column, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s&%s=%s&%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_CHART_DATA,
                APIConstants.PARAM_PROJECT_ID, projectId,
                APIConstants.PARAM_COLUMN, column,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());
        Log.v("lester-log", url);
        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<ChartData>>() {
                    }.getType();

                    List<ChartData> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getAreaVenues(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_AREAS,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());

        Log.v("lester-log-area", url);

        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Area>>() {
                    }.getType();

                    List<Area> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }


    public static Request getDecks(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_DECKS,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());

        Log.v("lester-log-decks", url);
        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Deck>>() {
                    }.getType();

                    List<Deck> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getAllDisciplines(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_DISCIPLINES,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());

        Log.v("lester-log-discipline", url);

        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Discipline>>() {
                    }.getType();

                    List<Discipline> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getSuppliers(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.COMMON_MOBILE,APIConstants.API_GET_SUPPLIERS,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());

        Log.v("lester-log-getsuppliers", url);

        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<Supplier>>() {
                    }.getType();

                    List<Supplier> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request downloadImageFromServer(final int requestCode, String fileName, final ResponseHandler responseHandler){

        final String url = String.format("%s/%s", APIConstants.DOWNLOAD_IMAGE_URL,fileName);



        ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {

                responseHandler.onSuccess(requestCode,response);
            }

        },0,0,null,null,new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("lester-log-img", String.valueOf(error));
                if (error.getMessage()!=null){
                    Log.i("Done Failed",error.getMessage());

                }
                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {

                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });


        request.setRetryPolicy(SocketTimeOut.getRetryPolicyExtended());
        return request;
    }

    public static Request downloadFileFromServer(final int requestCode, String fileName, final ResponseHandler responseHandler){
        final String url = String.format("%s/%s",APIConstants.DOWNLOAD_IMAGE_URL,fileName);

        InputStreamRequestHelper request = new InputStreamRequestHelper(Request.Method.GET, url,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        // TODO handle the response
                        try {
                            if (response!=null) {

                                responseHandler.onSuccess(requestCode, response);
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                            e.printStackTrace();
                        }
                    }
                } ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        }, null);

        request.setRetryPolicy(SocketTimeOut.getRetryPolicyExtended());
        return request;
    }

    public static Request uploadAttachments(final int requestCode,final String parentId, final String imagePath, final ResponseHandler responseHandler) {

        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s", APIConstants.DOMAIN, APIConstants.ICHECK_URL,
                APIConstants.ATTACHMENT_API_UPLOAD_ATTACHMENT);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.ATTACHMENT_PARAM_ID, parentId);//todo temp value


        VolleyMultiPartRequestHelper request = new VolleyMultiPartRequestHelper(url, imagePath, APIResponse.class,
                params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                Log.i("ICHECK_TAG",apiResponse.isSuccess()?"Successful":"not");
                if (apiResponse.isSuccess()) {
                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    UploadedAttachment obj = gson.fromJson(jsonString, UploadedAttachment.class);

                    Log.i("ICHECK_TAG",apiResponse.getMessage());
                    responseHandler.onSuccess(requestCode, obj);
                } else {

                    Log.i("ICHECK_TAG",apiResponse.getMessage());
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError) {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                } else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });
        request.setRetryPolicy(SocketTimeOut.getRetryPolicyExtended());
        return request;
    }

    public static Request deleteUploadFile(final int requestCode, String id, String fileName, final ResponseHandler responseHandler){

        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s", APIConstants.DOMAIN, APIConstants.ICHECK_URL,
                APIConstants.ATTACHMENT_API_DELETE_ATTACHMENT);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.ATTACHMENT_PARAM_ID, id);
        params.put(APIConstants.ATTACHMENT_PARAM_FILENAME, fileName);

        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){
                    responseHandler.onSuccess(requestCode, apiResponse.getMessage());
                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });
        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

}
