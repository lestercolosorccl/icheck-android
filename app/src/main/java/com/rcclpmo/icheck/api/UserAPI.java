package com.rcclpmo.icheck.api;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.helpers.VolleyPostRequestHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.APIResponse;
import com.rcclpmo.icheck.models.Login;
import com.rcclpmo.icheck.models.OAuthentication;
import com.rcclpmo.icheck.models.People;
import com.rcclpmo.icheck.utilities.GSONUtility;
import com.rcclpmo.icheck.utilities.SocketTimeOut;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class UserAPI {

    public static Request login(final int requestCode, OAuthentication oAuth, final ResponseHandler responseHandler){

        String url = String.format("%s/%s/%s", APIConstants.DOMAIN, APIConstants.LOGIN_API_AUTH, APIConstants.LOGIN_API_LOGIN);

        Map<String,String> params = new HashMap<String,String>();
        params.put(APIConstants.LOGIN_PARAM_EMAIL, oAuth.getEmail());
        params.put(APIConstants.LOGIN_PARAM_PASSWORD, oAuth.getPassword());



        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){
                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Login obj = gson.fromJson(jsonString, Login.class);

                    responseHandler.onSuccess(requestCode, obj);
                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if(error.networkResponse == null){

                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                    return;
                }

                int statusCode = error.networkResponse.statusCode;

                if(statusCode == HttpStatus.SC_UNAUTHORIZED){

                    responseHandler.onFailed(requestCode, APIConstants.LOGIN_ERROR_WRONG_USERNAME_PASSWORD);

                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);

                }


            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request getUsers(final int requestCode, final ResponseHandler responseHandler){
        ApplicationClass applicationClass = ApplicationClass.getInstance();

        String url = String.format("%s/%s/%s?%s=%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL,APIConstants.API_GET_USERS,
                APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());
        Request request = new JsonObjectRequest( url,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    gson = GSONUtility.createGsonBuilder().create();
                    String jsonString = new Gson().toJson(apiResponse.getData());
                    Type listType = new TypeToken<ArrayList<People>>() {
                    }.getType();

                    List<People> obj = gson.fromJson(jsonString, listType);

                    responseHandler.onSuccess(requestCode, obj);

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }
}
