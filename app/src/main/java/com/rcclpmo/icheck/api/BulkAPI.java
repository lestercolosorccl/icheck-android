package com.rcclpmo.icheck.api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.helpers.VolleyStringRequestHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.APIResponse;
import com.rcclpmo.icheck.models.BulkAction;
import com.rcclpmo.icheck.models.BulkModel;
import com.rcclpmo.icheck.utilities.GSONUtility;
import com.rcclpmo.icheck.utilities.SocketTimeOut;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class BulkAPI {

    public static Request bulkSyncActions(final int requestCode, final String json, final ResponseHandler responseHandler) {

        String url = String.format("%s/%s/%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL, APIConstants.BULK_API_SYNC_ICHECK);


        VolleyStringRequestHelper request = new VolleyStringRequestHelper(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.v("lester-gson", response);
                        Gson gson = GSONUtility.createGsonBuilder().create();
                        APIResponse apiResponse = gson.fromJson(response, APIResponse.class);

                        if (apiResponse.isSuccess()){

                            gson = GSONUtility.createGsonBuilder().create();
                            String jsonString = new Gson().toJson(apiResponse.getData());
                            BulkAction obj = gson.fromJson(jsonString, BulkAction.class);

                            responseHandler.onSuccess(requestCode, obj);
                        }else {
                            responseHandler.onFailed(requestCode, apiResponse.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError) {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                } else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request bulkDeleteAttachment(final int requestCode, final String json, final ResponseHandler responseHandler) {

        String url = String.format("%s/%s/%s", APIConstants.DOMAIN,
                APIConstants.ICHECK_URL, APIConstants.BULK_API_SYNC_DELETE_ATTACHMENTS);

        Log.v("lester-json", url);
        Log.v("lester-json", json);

        VolleyStringRequestHelper request = new VolleyStringRequestHelper(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = GSONUtility.createGsonBuilder().create();
                        APIResponse apiResponse = gson.fromJson(response, APIResponse.class);


                        if (apiResponse.isSuccess()){

                            gson = GSONUtility.createGsonBuilder().create();
                            String jsonString = new Gson().toJson(apiResponse.getData());
                            Type listType = new TypeToken<ArrayList<BulkModel>>() {
                            }.getType();

                            List<BulkModel> obj = gson.fromJson(jsonString, listType);

                            responseHandler.onSuccess(requestCode, obj);
                        }else {
                            responseHandler.onFailed(requestCode, apiResponse.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError) {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                } else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }

            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return json.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        request.setRetryPolicy(SocketTimeOut.getRetryPolicyExtended());
        return request;
    }
}
