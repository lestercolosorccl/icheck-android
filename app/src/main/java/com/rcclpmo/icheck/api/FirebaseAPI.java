package com.rcclpmo.icheck.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.helpers.VolleyPostRequestHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.APIResponse;
import com.rcclpmo.icheck.utilities.GSONUtility;
import com.rcclpmo.icheck.utilities.SocketTimeOut;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class FirebaseAPI {

    public static Request updateFirebaseToken(final int requestCode, final ResponseHandler responseHandler){

        /****
         * note: change the endpoint to getAllWalkThruRemarks
         */
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.FIREBASE_MOBILE,
                APIConstants.FIREBASE_API_UPDATE_FIREBASE_TOKEN);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.FIREBASE_PARAM_MODULE, APIConstants.FIREBASE_VALUE_MODULE_VALUE);
        params.put(APIConstants.FIREBASE_PARAM_NEW_TOKEN, applicationClass.getNewFirebaseToken());
        params.put(APIConstants.FIREBASE_PARAM_OLD_TOKEN, applicationClass.getOldFirebaseToken());

        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    responseHandler.onSuccess(requestCode, apiResponse.getMessage());

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }


    public static Request saveFirebaseToken(final int requestCode, final ResponseHandler responseHandler){

        /****
         * note: change the endpoint to getAllWalkThruRemarks
         */
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.FIREBASE_MOBILE,
                APIConstants.FIREBASE_API_SAVE_FIREBASE_TOKEN);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.FIREBASE_PARAM_MODULE, APIConstants.FIREBASE_VALUE_MODULE_VALUE);
        params.put(APIConstants.FIREBASE_PARAM_TOKEN, applicationClass.getNewFirebaseToken());

        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    responseHandler.onSuccess(requestCode, apiResponse.getMessage());

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }

    public static Request deleteFirebaseToken(final int requestCode, final ResponseHandler responseHandler){

        /****
         * note: change the endpoint to getAllWalkThruRemarks
         */
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        String url = String.format("%s/%s/%s",
                APIConstants.DOMAIN, APIConstants.FIREBASE_MOBILE,
                APIConstants.FREBASE_API_DELETE_FIREBASE_TOKEN);

        Map<String,String> params = new HashMap<>();
        params.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN, applicationClass.getAccessToken());
        params.put(APIConstants.FIREBASE_PARAM_MODULE, APIConstants.FIREBASE_VALUE_MODULE_VALUE);
        params.put(APIConstants.FIREBASE_PARAM_TOKEN, applicationClass.getNewFirebaseToken());

        VolleyPostRequestHelper request = new VolleyPostRequestHelper(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = GSONUtility.createGsonBuilder().create();
                APIResponse apiResponse = gson.fromJson(response.toString(), APIResponse.class);

                if (apiResponse.isSuccess()){

                    responseHandler.onSuccess(requestCode, apiResponse.getMessage());

                }else {
                    responseHandler.onFailed(requestCode, apiResponse.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof ServerError){
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_OFFLINE_MODE);
                }else {
                    responseHandler.onFailed(requestCode, APIConstants.ERROR_CONNECTION_PROBLEM);
                }
            }
        });

        request.setRetryPolicy(SocketTimeOut.getRetryPolicy());
        return request;
    }


}
