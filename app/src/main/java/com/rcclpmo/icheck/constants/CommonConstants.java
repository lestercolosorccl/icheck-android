package com.rcclpmo.icheck.constants;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class CommonConstants {
    /****
     * FILE TYPE
     */
    public static final String IMAGE_FILE_FORMAT = "png";
    public static final String DOC_FILE_FORMAT = "doc";


    public static final String DOC_FILE_TYPE = ".doc";
    public static final String DOCX_FILE_TYPE = ".docx";
    public static final String PDF_FILE_TYPE = ".pdf";


    /****
     * FILTER KEYS
     */

    public static final String KEY_FILTER_DATA_LIST = "filter-data-list";
    public static final String KEY_FILTER_PARAM = "key-filter-param";
    public final static String KEY_REMARK_ID  = "remark-id";
    public final static String KEY_IS_ADD_REMARK = "is-add-remark";
    public static final String KEY_DATE = "selected-date";
    public static final String KEY_SEARCH_TEXT = "search-text";

    /****
     * FILTER OPTIONS
     */
    public static final String DATA_KEY_OPTION_TYPE = "option_type";
    public static final String DATA_KEY_IS_WITH_ALL_SELECTION = "is-full-screen";
    public static final String DATA_KEY_ACTIVITY = "activity-type";
    public static final String DATA_KEY_PARENT_ID = "parent-id";
    public static final String DATA_KEY_PROJECT_ID = "projectId";
    public static final int OPTION_TYPE_PROJECT = 1001;
    public static final int OPTION_TYPE_AREA = 1002;
    public static final int OPTION_TYPE_DISCIPLINE = 1003;
    public static final int OPTION_TYPE_DECK = 1004;
    public static final int OPTION_TYPE_MEETING_TYPE = 1005;
    public static final int ACTIVITY_ADD_ACTION = 1006;
    public static final int ACTIVITY_REVIEW_LIST = 1007;
    public static final int ACTIVITY_DASHBOARD = 1008;
    public static final int OPTION_TYPE_STATUS = 1009;
    public static final int OPTION_TYPE_STATUS_FILTER = 1909;
    public static final int OPTION_TYPE_PRIORITY_FILTER = 1912;
    public static final int OPTION_TYPE_PEOPLE = 1010;
    public static final int OPTION_TYPE_IMPACT = 1011;
    public static final int OPTION_TYPE_PRIORITY = 1012;
    public static final int OPTION_TYPE_REMARK_TYPE = 1018;
    public static final int OPTION_TYPE_WORK_BY = 1013;
    public static final int OPTION_TYPE_AREA_MANAGER = 1014;
    public static final int OPTION_TYPE_SUPPLIER = 1015;

    public static final int ACTIVITY_ADD_ACTION_STATUS = 1106;
    public static final int ACTIVITY_ADD_ACTION_PRIORITY = 1206;

    public static final String GENERAL_VALUE = "General";
    public static final String GENERAL_ID = "-1";

    /****
     * REQUEST CODE
     *
     */
    public static final int VIEW_FILE_TARGET_REQUEST_CODE = 111;
    public static final int GALLERY_TARGET_REQUEST_CODE = 222;
    public static final int CAMERA_TARGET_REQUEST_CODE = 333;
    public static final int FILE_TARGET_REQUEST_CODE = 444;

    public static final String LOADER_DOWNLOADING_PROJECT = "Downloading Projects ...";
    public static final String LOADER_DOWNLOADING_DISCIPLINES = "Downloading Disciplines ...";
    public static final String LOADER_DOWNLOADING_DECKS = "Downloading Decks ...";
    public static final String LOADER_DOWNLOADING_AREAS = "Downloading Areas ...";
    public static final String LOADER_DOWNLOADING_MEETING_TYPES = "Downloading Providers ...";
    public static final String LOADER_DOWNLOADING_ATTACHMENTS = "Downloading Attachments ...";
    public static final String LOADER_DOWNLOADING_ACTION_LIST = "Downloading ICheckItem List ...";
    public static final String LOADER_DOWNLOADING_OWNER_LIST = "Downloading People List ...";
    public static final String LOADER_UPLOADING_ACTION = "Saving ICheck Item ...";
    public static final String LOADER_UPLOADING_ATTACHMENTS = "Uploading Attachments ...";
    public static final String LOADER_DELETING_ATTACHMENTS = "Deleting Attachment ...";
    public static final String LOADER_DOWNLOADING_CHART_DATA = "Downloading Pie Data ...";
    public static final String LOADER_DOWNLOADING_SUPPLIERS = "Downloading Suppliers ...";

    /****
     * FOR STATUS
     */
    public static final String STATUS_NO_STATUS = "No Status";
    public static final String NO_IMPACT = "No Impact";
    public static final String NO_PRIORITY = "No Priority";

    /***
     * COLUMN TYPE
     */
    public static final String COLUMN_TYPE_PRIORITY = "priority";
    public static final String COLUMN_TYPE_STATUS = "status";
    public static final String COLUMN_TYPE_IMPACT = "area";

}
