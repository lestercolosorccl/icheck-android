package com.rcclpmo.icheck.constants;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class APIRequestCode {

    public static final String REQUEST_TAG = "UNIVERSAL-REQUEST-TAG";
    public static final String REQUEST_TAG_SERVICE  = "REQUEST_TAG_SERVICE";

    /****
     * LOGIN
     */
    public final static int CODE_LOGIN = 100;
    public final static String TAG_LOGIN = "login-tag";


    /****
     * COMMON API
     */
    public static final int CODE_DOWNLOAD_IMAGE = 201;
    public static final int CODE_DOWNLOAD_DOC_FILE = 202;
    public static final String TAG_DOWNLOAD_IMAGE = "download-image";
    public static final String TAG_DOWNLOAD_DOC_FILE = "download-doc-file";

    /****
     * DASHBOARD
     */
    public static final int CODE_GET_PROJECT_LIST = 301;
    public static final String TAG_GET_PROJECT_LIST = "get-project-list";
    public static final int CODE_GET_USERS = 302;
    public static final String TAG_GET_USERS = "get-users";
    public static final int CODE_GET_AREAS = 303;
    public static final String TAG_GET_VENUES = "get-venues";
    public static final int CODE_GET_DECKS = 304;
    public static final String TAG_GET_DISCIPLINES = "get-disciplines";
    public static final int CODE_GET_ALL_DISCIPLINES = 305;
    public static final String TAG_GET_MEETING_TYPE = "get-meeting-types";
    public static final int CODE_GET_CHART_DATA = 306;
    public static final String TAG_GET_SUPPLIERS = "get-suppliers";
    public static final int CODE_GET_SUPPLIERS = 307;

    /****
     * REVIEW LIST
     */
    public static final int CODE_GET_GUARANTEE_ITEMS = 401;
    public static final String TAG_GET_ACTION_LOGS = "get-action-logs";

    /*****
     * ADD ACTION
     */
    public static final int CODE_SAVE_ACTION = 501;
    public static final int CODE_UPDATE_ACTION = 502;
    public static final int CODE_UPLOAD_ATTACHMENT = 503;
    public static final int CODE_DELETE_ATTACHMENT = 504;
    public static final int CODE_GET_ATTACHMENTS = 506;

    /****
     * BULK
     */
    public static final int CODE_BULK_DISPOSITION_ITEMS = 601;
    public static final int CODE_BULK_SYNC_DELETE_ATTACHMENTS = 602;

    /****
     * FIREBASE
     */
    public static final int CODE_FIREBASE_SAVE_TOKEN = 701;
    public static final int CODE_FIREBASE_UPDATE_TOKEN = 703;
    public static final int CODE_FIREBASE_DELETE_TOKEN = 704;


}

