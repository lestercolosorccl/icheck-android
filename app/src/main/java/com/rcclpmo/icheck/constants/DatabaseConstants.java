package com.rcclpmo.icheck.constants;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DatabaseConstants {



    //todo temp comment
//    public static final String KEY_PROJECT_ID                   = "projectId";
    public static final String KEY_ID                           = "id";

    /****
     * FOR ACTIONS
     */
    public static final String KEY_AREA                         = "area";
    public static final String KEY_DECK                         = "deck";
    public static final String KEY_AREA_ID                      = "areaId";
    public static final String KEY_DECK_ID                      = "deckId";
    public static final String KEY_PROVIDER                     = "provider";
    public static final String KEY_DISCIPLINE                   = "discipline";
    public static final String KEY_DISCIPLINE_ID                = "disciplineId";
    public static final String KEY_MEETING_TYPE                 = "meetingType";
    public static final String KEY_DUE_DATE                     = "dueDate";
    public static final String KEY_ASSIGNEE_ID                  = "assigneeId";
    public static final String KEY_CREATED_DATE                 = "dateAdded";
    public static final String KEY_UPLOADED_ID                  = "id";
    public static final String KEY_ROW_ID                       = "rowId";
    public static final String KEY_IS_SYNC                      = "isSync";
    public static final String KEY_IS_DELETED                   = "isDeleted";
    public static final String KEY_EXECUTED_BY                  = "executedBy";
    public static final String KEY_SHIP_ID                      = "shipId";
    public static final String KEY_PROJECT_ID                   = "projectId";
    public static final String KEY_SAFETY_OWNER                 = "safetyOwner";
    public static final String KEY_USER_ID                      = "id";
    public static final String KEY_STATUS                       = "status";
    public static final String KEY_PRIORITY                     = "priority";
    public static final String KEY_CLAIM_OWNER                  = "claimOwner";
    public static final String KEY_ACTION_OWNER_ID              = "actionOwnerId";

    public static final String ALL_VALUE                        = "-1000";
    public static final String ALL                              = "All";

    public static final int SCHEMA_VERSION                      = 1;



}

