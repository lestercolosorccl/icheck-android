package com.rcclpmo.icheck.constants;

import com.rcclpmo.icheck.business.ApplicationClass;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class APIConstants {


    /***
     * DOMAIN
     */
    public static final String DOMAIN = ApplicationClass.getDomain();
    public static final String DOWNLOAD_IMAGE_URL = ApplicationClass.getImageDomain();

    /****
     * Endpoint
     */
    public static final String GUARANTEE_CLAIM_URL = "GuaranteeClaimMobile";
    public static final String ICHECK_URL = "ICheckMobile";
    public static final String COMMON_MOBILE = "CommonMobile";
    public static final String FIREBASE_MOBILE = "FireBaseMobile";


    /***
     * SERVER ERROR
     */
    public static final String ERROR_CONNECTION_PROBLEM = "Problem Connecting to Server";
    public static final String LOGIN_ERROR_WRONG_USERNAME_PASSWORD = "WRONG USERNAME/PASSWORD";
    public static final String ERROR_OFFLINE_MODE = "No Internet Connection.";

    /****
     * HEADERS
     */
    public static final String HEADER_GLOBAL_PROJECT_ID = "Global_project_id";
    public static final String HEADER_MODULE_NAME = "Module_name";
    public static final String HEADER_GLOBAL_USER_ID = "Global_user_id";
    public static final String HEADER_IS_I_95_MOBILE = "Isi95mobile";
    public static final String HEADER_PLATFORM = "Platform";
    public static final String HEADER_DEVICE_ID = "Device_id";
    /***
     * HEADER VALUE
     */
    public static final String HEADER_VALUE_MODULE_NAME = "module.action_log";
    public static final String HEADER_VALUE_IS_MOBILE = "true";
    public static final String HEADER_VALUE_PLATFORM = "Android";

    /***
     * Login API note: *contains reserved params
     */
    public static final String LOGIN_API_AUTH = "auth";
    public static final String LOGIN_API_LOGIN = "login";
    public static final String LOGIN_PARAM_PASSWORD = "password";
    public static final String LOGIN_PARAM_EMAIL = "email";
    public static final String LOGIN_PARAM_ACCESS_TOKEN = "access_token";

    /*****
     * dashboard API /COMMON API
     */
    public static final String API_GET_SHIP_LIST = "getShipList";
    public static final String API_GET_PROJECT = "getProjects";
    public static final String API_GET_USERS = "getUsers";
    public static final String API_GET_AREAS = "getAreas";
    public static final String API_GET_DISCIPLINES = "getDisciplines";
    public static final String API_GET_DECKS = "getDecks";
    public static final String API_GET_SAFETY_ITEMS = "getGuranteeClaims";
    public static final String API_GET_CHART_DATA = "getChartData";
    public static final String API_GET_SUPPLIERS = "getSuppliers";
    public static final String API_GET_ITEMS = "getICheckItems";

    /****
     * ACTION LOG API
     *
     */
    public static final String SAFETY_FIRST_SAVE_ICHECK_ITEM = "saveICheckData";
    public static final String UPDATE_ICHECK_ITEM = "updateICheckData";
    public static final String SAFETY_FIRST_SAVE_SAFETY_ITEM = "saveGuaranteeClaim";
    public static final String DISPOSITION_LIST_UPDATE_DISPOSITION_ITEM = "updateGuaranteeClaim";
    public static final String DISPOSITION_LIST_GET_ATTACHMENTS = "getAttachments";
    public static final String DISPOSITION_LIST_PARAM_IDS = "ids";
    public static final String DISPOSITION_LIST_PARAM_SHIP_ID = "ship_id";
    public static final String PARAM_COLUMN = "column";
    public static final String PARAM_DATE_OF_CLAIM = "date_of_claim";
    public static final String PARAM_SHIP_ID = "ship_id";
    public static final String PARAM_PROJECT_ID = "project_id";
    public static final String PARAM_CLAIM_OWNER = "claim_owner";
    public static final String PARAM_ACTION_OWNER = "action_owner";
    public static final String PARAM_ACTION_OWNER_ID = "action_owner_id";
    public static final String PARAM_AREA_ID = "area_id";
    public static final String PARAM_DECK_ID = "deck_id";
    public static final String PARAM_DISCIPLINE_ID = "discipline_id";
    public static final String PARAM_PRIORITY = "priority";
    public static final String PARAM_IMPACT = "impact";
    public static final String PARAM_DESCRIPTION_OF_DEFECT = "description_of_defect";
    public static final String PARAM_REQUESTED_ACTION = "requested_action";
    public static final String PARAM_WORK_BY = "work_by";
    public static final String PARAM_MAKER_PART_NO = "maker_part_no";
    public static final String PARAM_MAKER_NAME = "maker_name";
    public static final String PARAM_MAKER_PARTNAME = "maker_partname";
    public static final String PARAM_MAKER_SERIAL_NO = "maker_serial_no";
    public static final String PARAM_MAKER_MODEL_NO = "maker_model_no";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_ACTION_TAKEN = "action_taken";
    public static final String PARAM_COMMENTS = "comments";
    public static final String PARAM_ID = "id";
    public static final String PARAM_CREATED_DATE = "created_date";
    public static final String PARAM_CONTRACTOR = "contractor";
    public static final String PARAM_CONTRACTOR_EMAIL = "contractor_email";
    public static final String PARAM_AREA_MANAGER = "area_manager";
    public static final String PARAM_COMMENT = "comment";
    public static final String PARAM_REMARK = "remark";

    /****
     * ATTACHMENT API
     *
     */
    public static final String ATTACHMENT_API_UPLOAD_ATTACHMENT = "uploadAttachment";
    public static final String ATTACHMENT_API_DELETE_ATTACHMENT = "deleteAttachment";
    public static final String ATTACHMENT_PARAM_ID = "id";
    public static final String ATTACHMENT_PARAM_FILE = "file";
    public static final String ATTACHMENT_PARAM_FILENAME = "filename";

    /****
     * BULK
     */
    public static final String BULK_API_SYNC_GUARANTEE_CLAIMS = "bulkSyncGuaranteeClaim";
    public static final String BULK_API_SYNC_ICHECK = "bulkSyncICheckData";
    public static final String BULK_API_SYNC_DELETE_ATTACHMENTS = "bulkDeleteAttachments";
    public static final String BULK_PARAM_ATTACHMENTS = "attachments";
    public static final String BULK_PARAM_ACTION_LOGS = "acction_logs";
    public static final String BULK_PARAM_ID = "id";
    public static final String BULK_PARAM_ITEMS = "items";
    public static final String BULK_PARAM_PROJECT_ID = "project_id";
    public static final String BULK_PARAM_SHIP_ID = "ship_id";
    public static final String BULK_PARAM_AREA_ID = "area_id";
    public static final String BULK_PARAM_DECK_ID = "deck_id";
    public static final String BULK_PARAM_QUANTITY = "quantity";
    public static final String BULK_PARAM_PROVIDER = "provider";
    public static final String BULK_PARAM_DESCRIPTION = "description";
    public static final String BULK_PARAM_COMMENTS = "comments";
    public static final String BULK_PARAM_FILENAME = "filename";
    public static final String BULK_PARAM_AREA = "area";
    public static final String BULK_PARAM_DECK = "deck";
    public static final String BULK_PARAM_DISCIPLINE = "discipline";
    public static final String BULK_PARAM_STATUS = "status";
    public static final String BULK_PARAM_SAFETY_OWNER = "safety_owner";
    public static final String BULK_PARAM_DUE_DATE = "due_date";
    public static final String BULK_PARAM_REMARKS = "remarks";
    public static final String BULK_PARAM_REMARKS_TYPE = "remark_type";
    public static final String BULK_ADDED_BY = "added_by";
    public static final String BULK_EDITED_BY = "edited_by";
    public static final String BULK_DATE_ADDED = "date_added";
    public static final String BULK_DATE_EDITED = "date_edited";



    /****
     * FIREBASE API
     */
    public static final String FIREBASE_API_SAVE_FIREBASE_TOKEN = "saveFirebaseToken";
    public static final String FIREBASE_API_UPDATE_FIREBASE_TOKEN = "updateFirebaseToken";
    public static final String FREBASE_API_DELETE_FIREBASE_TOKEN = "deleteFirebaseToken";
    public static final String FIREBASE_PARAM_MODULE = "module";
    public static final String FIREBASE_PARAM_OLD_TOKEN = "old_token";
    public static final String FIREBASE_PARAM_NEW_TOKEN = "new_token";
    public static final String FIREBASE_PARAM_TOKEN = "token";
    public static final String FIREBASE_VALUE_MODULE_VALUE = "ICheckItem Log";

}

