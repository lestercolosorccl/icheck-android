package com.rcclpmo.icheck.business;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.rcclpmo.icheck.BuildConfig;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.dao.DatabaseMigration;
import com.rcclpmo.icheck.receivers.ConnectivityReceiver;
import com.rcclpmo.icheck.receivers.FirebaseReceiver;
import com.rcclpmo.icheck.server.NANOServer;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ApplicationClass extends Application {

    public static final int REQUEST_CODE_REFRESH_TOKEN = 404;
    private static final String REALM_NAME ="myrealm.realm";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String REFRESH_TOKEN = "refreshToken";
    private static final String NEW_FIREBASE_TOKEN = "newFirebaseToken";
    private static final String OLD_FIREBASE_TOKEN = "oldFirebaseToken";
    private static final String IS_TOKEN_SAVED = "isTokenSaved";
    private static final String SHIP_ID = "selectedShipId";
    private static final String SHIP_NAME = "selectedShipName";
    private static final String USER_ID = "userId";
    private static final String DEVICE_ID = "device_id";
    private static final String PROJECT_ID = "project_id";

    private static ApplicationClass instance;
    private RequestQueue requestQueue;
    private RequestQueue requestQueueStack;
    private NANOServer myServerInstance;
    private FirebaseReceiver firebaseReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        Realm.init(getApplicationContext());
        setRealmConfiguration();
    }

    public static ApplicationClass getInstance(){

        return instance;
    }

    private void setRealmConfiguration(){
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name(REALM_NAME)
                .schemaVersion(DatabaseConstants.SCHEMA_VERSION)
                .migration(new DatabaseMigration())
                .build();

        Realm.setDefaultConfiguration(configuration);
    }

    public FirebaseReceiver getFirebaseReceiver(){
        if (firebaseReceiver==null){
            firebaseReceiver = new FirebaseReceiver();
        }

        return firebaseReceiver;
    }

    public static String getDomain(){
        return BuildConfig.SERVER_URL;
    }

    public static String getImageDomain(){
        return BuildConfig.IMAGE_URL;
    }


    public static String getWweatherDomain(){
        return BuildConfig.WEATHER_URL;
    }

    public RequestQueue getRequestQueue(){
        if (requestQueue == null){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return requestQueue;
    }

    public NANOServer getServerInstance() throws IOException {
        if (myServerInstance==null){
            myServerInstance = new NANOServer(getApplicationContext());
        }
        return myServerInstance;
    }


    public RequestQueue getRequestQueueStack(){
        if (requestQueueStack == null){
            requestQueueStack = Volley.newRequestQueue(getApplicationContext(),new HurlStack());
        }

        return requestQueueStack;
    }

    public void saveToken(String accessToken) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(ACCESS_TOKEN, accessToken);
//        editor.putString(REFRESH_TOKEN, refreshToken);

        editor.commit();
    }

    public String getAccessToken(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String token = pref.getString(ACCESS_TOKEN, null);

        return token;
    }

    public void saveNewFirebaseToken(String accessToken) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(NEW_FIREBASE_TOKEN, accessToken);

        editor.commit();
    }

    public String getNewFirebaseToken(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String token = pref.getString(NEW_FIREBASE_TOKEN, null);

        return token;
    }

    public void saveOldFirebaseToken(String accessToken) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(OLD_FIREBASE_TOKEN, accessToken);

        editor.commit();
    }

    public String getOldFirebaseToken(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String token = pref.getString(OLD_FIREBASE_TOKEN, null);

        return token;
    }

//    public void saveProjectId(String projectId) {
//
//        SharedPreferences pref = PreferenceManager
//                .getDefaultSharedPreferences(getApplicationContext());
//
//        SharedPreferences.Editor editor = pref.edit();
//
//        editor.putString(PROJECT_ID, projectId);
//
//        editor.commit();
//    }
//
//    public String getShipId(){
//
//        SharedPreferences pref = PreferenceManager
//                .getDefaultSharedPreferences(getInstance());
//
//        String projectId = pref.getString(PROJECT_ID, null);
//
//        return projectId;
//    }

    public void saveDeviceId(String deviceId) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(DEVICE_ID, deviceId);

        editor.commit();
    }

    public String getDeviceId(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String deviceId = pref.getString(DEVICE_ID, null);

        return deviceId;
    }

    public void saveUserId(String userId) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(USER_ID, userId);

        editor.commit();
    }

    public String getUserId(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String userId = pref.getString(USER_ID, null);

        return userId;
    }


    public void saveShipId(String shipId, String shipName) {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(SHIP_ID, shipId);
        editor.putString(SHIP_NAME, shipName);

        editor.commit();
    }

    public String getShipId(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String token = pref.getString(SHIP_ID, null);

        return token;
    }

    public String getShipName(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        String token = pref.getString(SHIP_NAME, null);

        return token;
    }

    public String getRefreshToken(){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        String token = pref.getString(REFRESH_TOKEN, null);

        return token;
    }

    public void setFirebaseTokenSaved(String isSaved){

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getInstance());

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(IS_TOKEN_SAVED, isSaved);

        editor.commit();
    }

    public void deleteTokens() {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        if (pref.contains(ACCESS_TOKEN)) {
            SharedPreferences.Editor editor = pref.edit();

            editor.remove(ACCESS_TOKEN);

            editor.commit();
        }

        if (pref.contains(SHIP_ID)) {
            SharedPreferences.Editor editor = pref.edit();

            editor.remove(SHIP_ID);

            editor.commit();
        }


        if (pref.contains(SHIP_NAME)) {
            SharedPreferences.Editor editor = pref.edit();

            editor.remove(SHIP_NAME);

            editor.commit();
        }

        if (pref.contains(PROJECT_ID)) {
            SharedPreferences.Editor editor = pref.edit();

            editor.remove(PROJECT_ID);

            editor.commit();
        }

        if (pref.contains(USER_ID)) {
            SharedPreferences.Editor editor = pref.edit();

            editor.remove(USER_ID);

            editor.commit();
        }
    }

    public boolean isUpdateFirebaseToken() {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        return pref.contains(OLD_FIREBASE_TOKEN);

    }


    public boolean isWithOldFirebaseToken() {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        return pref.contains(NEW_FIREBASE_TOKEN);

    }

    public boolean isFirebaseTokenSavedToServer() {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        return pref.contains(IS_TOKEN_SAVED);

    }

    public boolean isLoggedIn() {

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        return pref.contains(ACCESS_TOKEN);

    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;

    }

    public void setFirebaseReceiverListener(FirebaseReceiver.FirebaseNotificationListener listener){
        firebaseReceiver.listener = listener;

    }

//    public void setShipSelectionListener(GoToOfflineDialogFragment.ShipSelectionListener listener) {
//        GoToOfflineDialogFragment.shipSelectionListener = listener;
//
//    }

}

