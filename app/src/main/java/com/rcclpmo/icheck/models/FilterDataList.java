package com.rcclpmo.icheck.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */


public class FilterDataList implements Parcelable {

    private String fieldName;
    private String value;

    public FilterDataList(){

    }

    protected FilterDataList(Parcel in) {
        fieldName = in.readString();
        value = in.readString();
    }

    public static final Creator<FilterDataList> CREATOR = new Creator<FilterDataList>() {
        @Override
        public FilterDataList createFromParcel(Parcel in) {
            return new FilterDataList(in);
        }

        @Override
        public FilterDataList[] newArray(int size) {
            return new FilterDataList[size];
        }
    };

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fieldName);
        parcel.writeString(value);
    }
}
