package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class UploadedAttachment {

    @SerializedName("fileName")
    private String fileName;
    @SerializedName("fileType")
    private String fileType;
    @SerializedName("uploadedId")
    private Integer uploadedId;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Integer getUploadedId() {
        return uploadedId;
    }

    public void setUploadedId(Integer uploadedId) {
        this.uploadedId = uploadedId;
    }

}
