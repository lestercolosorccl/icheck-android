package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;
import com.rcclpmo.icheck.constants.APIConstants;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */
public class ICheckItem extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private String id;
    @SerializedName("project_id")
    private String projectId;
    @SerializedName("remark")
    private String remark;
    @SerializedName("area_id")
    private String areaId;
    @SerializedName("deck_id")
    private String deckId;
    @SerializedName("remark_type")
    private String remarkType;
    @SerializedName("priority")
    private String priority;
    @SerializedName("status")
    private String status;
    @SerializedName("discipline_id")
    private String disciplineId;
    @SerializedName("discipline")
    private String discipline;
    @SerializedName("area")
    private String area;
    @SerializedName("deck")
    private String deck;
    @SerializedName("action_owner")
    private String actionOwner;
    @SerializedName("action_owner_id")
    private String actionOwnerId;
    @SerializedName("comment")
    private String comment;
    @SerializedName("due_date")
    private String dueDate;
    @SerializedName("added_by")
    private String addedBy;
    @SerializedName("edited_by")
    private String editedBy;
    @SerializedName("date_added")
    private String dateAdded;
    @SerializedName("date_edited")
    private String dateEdited;
    @SerializedName("attachments")
    private String attachments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public String getRemarkType() {
        return remarkType;
    }

    public void setRemarkType(String remarkType) {
        this.remarkType = remarkType;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(String disciplineId) {
        this.disciplineId = disciplineId;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getActionOwner() {
        return actionOwner;
    }

    public void setActionOwner(String actionOwner) {
        this.actionOwner = actionOwner;
    }

    public String getActionOwnerId() {
        return actionOwnerId;
    }

    public void setActionOwnerId(String actionOwnerId) {
        this.actionOwnerId = actionOwnerId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDueDate() {
        if (dueDate.matches("\\d{2}/\\d{2}/\\d{4}")) {
            String m = dueDate.substring(0,2);
            String d = dueDate.substring(3,5);
            String y = dueDate.substring(6,10);;
            return y+"-"+m+"-"+d;
        }else{
            return dueDate;
        }
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateEdited() {
        return dateEdited;
    }

    public void setDateEdited(String dateEdited) {
        this.dateEdited = dateEdited;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }


    private boolean isSync = true;

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {

            jsonObject.put(APIConstants.BULK_PARAM_ID, getId());
            jsonObject.put(APIConstants.PARAM_PROJECT_ID, getProjectId());
            jsonObject.put(APIConstants.PARAM_AREA_ID, getAreaId());
            jsonObject.put(APIConstants.PARAM_DISCIPLINE_ID, getDisciplineId());
            jsonObject.put(APIConstants.BULK_PARAM_DECK_ID, getDeckId());
            jsonObject.put(APIConstants.BULK_PARAM_REMARKS_TYPE, getRemarkType());
            jsonObject.put(APIConstants.PARAM_PRIORITY, getPriority());
            jsonObject.put(APIConstants.BULK_PARAM_STATUS, getStatus());
            jsonObject.put(APIConstants.PARAM_ACTION_OWNER, getActionOwnerId());
            jsonObject.put(APIConstants.PARAM_COMMENT, getComment());
            jsonObject.put(APIConstants.PARAM_REMARK, getRemark());
            jsonObject.put(APIConstants.BULK_PARAM_DUE_DATE, getDueDate());
            jsonObject.put(APIConstants.BULK_ADDED_BY, getAddedBy());
            jsonObject.put(APIConstants.BULK_EDITED_BY, getEditedBy());
            jsonObject.put(APIConstants.BULK_DATE_ADDED, getDateAdded());
            jsonObject.put(APIConstants.BULK_DATE_EDITED, getDateEdited());
            jsonObject.put(APIConstants.BULK_PARAM_ATTACHMENTS, getAttachments());

            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return jsonObject;
        }

    }


}
