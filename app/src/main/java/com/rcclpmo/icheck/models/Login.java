package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class Login {

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("user")
    private User user;

    /**
     *
     * @return
     * The accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     *
     * @param accessToken
     * The access_token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
