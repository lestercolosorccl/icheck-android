package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 21/03/2018.
 */

public class Supplier extends RealmObject {

    @SerializedName("company")
    private String company;
    @PrimaryKey
    @SerializedName("id")
    private String id;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
