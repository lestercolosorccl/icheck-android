package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class APIResponse<T> {

    @SerializedName("success")
    private boolean success;
    @SerializedName("data")
    private T data;
    @SerializedName("message")
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
