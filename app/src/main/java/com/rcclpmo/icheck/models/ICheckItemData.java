package com.rcclpmo.icheck.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ICheckItemData implements Parcelable {

    private String id;
    private String length;
    private String start;
    private String projectId;
    private String sort;
    private String filter;
    private String area;
    private String areaId;
    private boolean isSync;
    private String discipline;
    private String disciplineId;
    private String deck;
    private String deckId;
    private String remark;
    private String comment;
    private String status;
    private String shipId;
    private String dueDate;
    //for guarantee claim
    private String dateOfClaim;
    private String priority;
    private String impact;
    private String descriptionOfDefect;
    private String requestedAction;
    private String workBy;
    private String claimOwner;
    private String claimOwnerName;
    private String makerPartNo;
    private String makerName;
    private String makerPartName;
    private String makerSerialNo;
    private String makerModelNo;
    private String actionTaken;
    private String createdDate;
    private String shipName;
    //added fields for version 2
    private String contractor;
    private String areaManager;
    private String contractorEmail;
    private String actionOwner;
    private String actionOwnerId;
    private String remarkType;
    private String addedBy;
    private String editedBy;
    private String dateAdded;
    private String dateEdited;
    private String attachments;

    public ICheckItemData(){

    }

    protected ICheckItemData(Parcel in) {
        id = in.readString();
        length = in.readString();
        start = in.readString();
        projectId = in.readString();
        sort = in.readString();
        filter = in.readString();
        area = in.readString();
        areaId = in.readString();
        isSync = in.readByte() != 0;
        discipline = in.readString();
        disciplineId = in.readString();
        deck = in.readString();
        deckId = in.readString();
        remark = in.readString();
        comment = in.readString();
        status = in.readString();
        shipId = in.readString();
        dueDate = in.readString();
        dateOfClaim = in.readString();
        priority = in.readString();
        impact = in.readString();
        descriptionOfDefect = in.readString();
        requestedAction = in.readString();
        workBy = in.readString();
        claimOwner = in.readString();
        claimOwnerName = in.readString();
        makerPartNo = in.readString();
        makerName = in.readString();
        makerPartName = in.readString();
        makerSerialNo = in.readString();
        makerModelNo = in.readString();
        actionTaken = in.readString();
        createdDate = in.readString();
        shipName = in.readString();
        contractor = in.readString();
        areaManager = in.readString();
        contractorEmail = in.readString();
        actionOwner = in.readString();
        actionOwnerId = in.readString();
        remarkType = in.readString();
        addedBy = in.readString();
        editedBy = in.readString();
        dateAdded = in.readString();
        dateEdited = in.readString();
        attachments = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(length);
        dest.writeString(start);
        dest.writeString(projectId);
        dest.writeString(sort);
        dest.writeString(filter);
        dest.writeString(area);
        dest.writeString(areaId);
        dest.writeByte((byte) (isSync ? 1 : 0));
        dest.writeString(discipline);
        dest.writeString(disciplineId);
        dest.writeString(deck);
        dest.writeString(deckId);
        dest.writeString(remark);
        dest.writeString(comment);
        dest.writeString(status);
        dest.writeString(shipId);
        dest.writeString(dueDate);
        dest.writeString(dateOfClaim);
        dest.writeString(priority);
        dest.writeString(impact);
        dest.writeString(descriptionOfDefect);
        dest.writeString(requestedAction);
        dest.writeString(workBy);
        dest.writeString(claimOwner);
        dest.writeString(claimOwnerName);
        dest.writeString(makerPartNo);
        dest.writeString(makerName);
        dest.writeString(makerPartName);
        dest.writeString(makerSerialNo);
        dest.writeString(makerModelNo);
        dest.writeString(actionTaken);
        dest.writeString(createdDate);
        dest.writeString(shipName);
        dest.writeString(contractor);
        dest.writeString(areaManager);
        dest.writeString(contractorEmail);
        dest.writeString(actionOwner);
        dest.writeString(actionOwnerId);
        dest.writeString(remarkType);
        dest.writeString(addedBy);
        dest.writeString(editedBy);
        dest.writeString(dateAdded);
        dest.writeString(dateEdited);
        dest.writeString(attachments);


    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ICheckItemData> CREATOR = new Creator<ICheckItemData>() {
        @Override
        public ICheckItemData createFromParcel(Parcel in) {
            return new ICheckItemData(in);
        }

        @Override
        public ICheckItemData[] newArray(int size) {
            return new ICheckItemData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(String disciplineId) {
        this.disciplineId = disciplineId;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public String getRemarkType() {
        return remarkType;
    }

    public void setRemarkType(String remarkType) {
        this.remarkType = remarkType;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShipId() {
        return shipId;
    }

    public void setShipId(String shipId) {
        this.shipId = shipId;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }


    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }


    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }


    public String getDateEdited() {
        return dateEdited;
    }

    public void setDateEdited(String dateEdited) {
        this.dateEdited = dateEdited;
    }


    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getDateOfClaim() {
        return dateOfClaim;
    }

    public void setDateOfClaim(String dateOfClaim) {
        this.dateOfClaim = dateOfClaim;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getImpact() {
        return impact;
    }

    public void setImpact(String impact) {
        this.impact = impact;
    }

    public String getDescriptionOfDefect() {
        return descriptionOfDefect;
    }

    public void setDescriptionOfDefect(String descriptionOfDefect) {
        this.descriptionOfDefect = descriptionOfDefect;
    }

    public String getRequestedAction() {
        return requestedAction;
    }

    public void setRequestedAction(String requestedAction) {
        this.requestedAction = requestedAction;
    }

    public String getWorkBy() {
        return workBy;
    }

    public void setWorkBy(String workBy) {
        this.workBy = workBy;
    }

    public String getClaimOwner() {
        return claimOwner;
    }

    public void setClaimOwner(String claimOwner) {
        this.claimOwner = claimOwner;
    }

    public String getClaimOwnerName() {
        return claimOwnerName;
    }

    public void setClaimOwnerName(String claimOwnerName) {
        this.claimOwnerName = claimOwnerName;
    }

    public String getMakerPartNo() {
        return makerPartNo;
    }

    public void setMakerPartNo(String makerPartNo) {
        this.makerPartNo = makerPartNo;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getMakerPartName() {
        return makerPartName;
    }

    public void setMakerPartName(String makerPartName) {
        this.makerPartName = makerPartName;
    }

    public String getMakerSerialNo() {
        return makerSerialNo;
    }

    public void setMakerSerialNo(String makerSerialNo) {
        this.makerSerialNo = makerSerialNo;
    }

    public String getMakerModelNo() {
        return makerModelNo;
    }

    public void setMakerModelNo(String makerModelNo) {
        this.makerModelNo = makerModelNo;
    }

    public String getActionOwner() {
        return actionOwner;
    }

    public void setActionOwner(String actionOwner) {
        this.actionOwner = actionOwner;
    }

    public String getActionOwnerId() {
        return actionOwnerId;
    }

    public void setActionOwnerId(String actionOwnerId) {
        this.actionOwnerId = actionOwnerId;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getAreaManager() {
        return areaManager;
    }

    public void setAreaManager(String areaManager) {
        this.areaManager = areaManager;
    }

    public String getContractorEmail() {
        return contractorEmail;
    }

    public void setContractorEmail(String contractorEmail) {
        this.contractorEmail = contractorEmail;
    }
}