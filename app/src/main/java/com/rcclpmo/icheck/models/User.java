package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class User extends RealmObject {

    @SerializedName("id")
    @PrimaryKey
    private String id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("position")
    private String position;
    @SerializedName("email")
    private String email;
    @SerializedName("created")
    private String created;
    @SerializedName("created_by")
    private String createdBy;
    @SerializedName("modified")
    private String modified;
    @SerializedName("modified_by")
    private String modifiedBy;
    @SerializedName("activated")
    private String activated;
    @SerializedName("banned")
    private String banned;
    @SerializedName("ban_reason")
    private String banReason;
    @SerializedName("new_password_key")
    private String newPasswordKey;
    @SerializedName("new_password_requested")
    private String newPasswordRequested;
    @SerializedName("new_email")
    private String newEmail;
    @SerializedName("new_email_key")
    private String newEmailKey;
    @SerializedName("last_ip")
    private String lastIp;
    @SerializedName("last_login")
    private String lastLogin;
    @SerializedName("is_super_admin")
    private String isSuperAdmin;
    @SerializedName("phone")
    private String phone;
    @SerializedName("expire_date")
    private String expireDate;
    @SerializedName("company")
    private String company;
    @SerializedName("company_type")
    private String companyType;
    @SerializedName("department_id")
    private String departmentId;
    @SerializedName("department_name")
    private String departmentName;
    @SerializedName("supplier_id")
    private String supplierId;
    @SerializedName("is_user_nvo")
    private String isUserNvo;
    @SerializedName("jde_code")
    private String jdeCode;
    @SerializedName("jde_ship_code")
    private String jdeShipCode;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The position
     */
    public String getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The created
     */
    public String getCreated() {
        return created;
    }

    /**
     *
     * @param created
     * The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     *
     * @return
     * The createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * The created_by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     *
     * @return
     * The modified
     */
    public String getModified() {
        return modified;
    }

    /**
     *
     * @param modified
     * The modified
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     *
     * @return
     * The modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     *
     * @param modifiedBy
     * The modified_by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     *
     * @return
     * The activated
     */
    public String getActivated() {
        return activated;
    }

    /**
     *
     * @param activated
     * The activated
     */
    public void setActivated(String activated) {
        this.activated = activated;
    }

    /**
     *
     * @return
     * The banned
     */
    public String getBanned() {
        return banned;
    }

    /**
     *
     * @param banned
     * The banned
     */
    public void setBanned(String banned) {
        this.banned = banned;
    }

    /**
     *
     * @return
     * The banReason
     */
    public String getBanReason() {
        return banReason;
    }

    /**
     *
     * @param banReason
     * The ban_reason
     */
    public void setBanReason(String banReason) {
        this.banReason = banReason;
    }

    /**
     *
     * @return
     * The newPasswordKey
     */
    public String getNewPasswordKey() {
        return newPasswordKey;
    }

    /**
     *
     * @param newPasswordKey
     * The new_password_key
     */
    public void setNewPasswordKey(String newPasswordKey) {
        this.newPasswordKey = newPasswordKey;
    }

    /**
     *
     * @return
     * The newPasswordRequested
     */
    public String getNewPasswordRequested() {
        return newPasswordRequested;
    }

    /**
     *
     * @param newPasswordRequested
     * The new_password_requested
     */
    public void setNewPasswordRequested(String newPasswordRequested) {
        this.newPasswordRequested = newPasswordRequested;
    }

    /**
     *
     * @return
     * The newEmail
     */
    public String getNewEmail() {
        return newEmail;
    }

    /**
     *
     * @param newEmail
     * The new_email
     */
    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    /**
     *
     * @return
     * The newEmailKey
     */
    public String getNewEmailKey() {
        return newEmailKey;
    }

    /**
     *
     * @param newEmailKey
     * The new_email_key
     */
    public void setNewEmailKey(String newEmailKey) {
        this.newEmailKey = newEmailKey;
    }

    /**
     *
     * @return
     * The lastIp
     */
    public String getLastIp() {
        return lastIp;
    }

    /**
     *
     * @param lastIp
     * The last_ip
     */
    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    /**
     *
     * @return
     * The lastLogin
     */
    public String getLastLogin() {
        return lastLogin;
    }

    /**
     *
     * @param lastLogin
     * The last_login
     */
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     *
     * @return
     * The isSuperAdmin
     */
    public String getIsSuperAdmin() {
        return isSuperAdmin;
    }

    /**
     *
     * @param isSuperAdmin
     * The is_super_admin
     */
    public void setIsSuperAdmin(String isSuperAdmin) {
        this.isSuperAdmin = isSuperAdmin;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The expireDate
     */
    public String getExpireDate() {
        return expireDate;
    }

    /**
     *
     * @param expireDate
     * The expire_date
     */
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    /**
     *
     * @return
     * The company
     */
    public String getCompany() {
        return company;
    }

    /**
     *
     * @param company
     * The company
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     *
     * @return
     * The companyType
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     *
     * @param companyType
     * The company_type
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    /**
     *
     * @return
     * The departmentId
     */
    public String getDepartmentId() {
        return departmentId;
    }

    /**
     *
     * @param departmentId
     * The department_id
     */
    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    /**
     *
     * @return
     * The departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     *
     * @param departmentName
     * The department_name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     *
     * @return
     * The supplierId
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     *
     * @param supplierId
     * The supplier_id
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     *
     * @return
     * The isUserNvo
     */
    public String getIsUserNvo() {
        return isUserNvo;
    }

    /**
     *
     * @param isUserNvo
     * The is_user_nvo
     */
    public void setIsUserNvo(String isUserNvo) {
        this.isUserNvo = isUserNvo;
    }

    /**
     *
     * @return
     * The jdeCode
     */
    public String getJdeCode() {
        return jdeCode;
    }

    /**
     *
     * @param jdeCode
     * The jde_code
     */
    public void setJdeCode(String jdeCode) {
        this.jdeCode = jdeCode;
    }

    /**
     *
     * @return
     * The jdeShipCode
     */
    public String getJdeShipCode() {
        return jdeShipCode;
    }

    /**
     *
     * @param jdeShipCode
     * The jde_ship_code
     */
    public void setJdeShipCode(String jdeShipCode) {
        this.jdeShipCode = jdeShipCode;
    }

}
