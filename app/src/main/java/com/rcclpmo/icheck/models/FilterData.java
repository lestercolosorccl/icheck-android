package com.rcclpmo.icheck.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class FilterData implements Parcelable {

    private int id;
    private String filterLabel;
    private String fieldName;
    private String value;

    public FilterData(){

    }

    protected FilterData(Parcel in) {
        id = in.readInt();
        filterLabel = in.readString();
        fieldName = in.readString();
        value = in.readString();
    }

    public static final Creator<FilterData> CREATOR = new Creator<FilterData>() {
        @Override
        public FilterData createFromParcel(Parcel in) {
            return new FilterData(in);
        }

        @Override
        public FilterData[] newArray(int size) {
            return new FilterData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilterLabel() {
        return filterLabel;
    }

    public void setFilterLabel(String filterLabel) {
        this.filterLabel = filterLabel;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(filterLabel);
        parcel.writeString(fieldName);
        parcel.writeString(value);
    }
}
