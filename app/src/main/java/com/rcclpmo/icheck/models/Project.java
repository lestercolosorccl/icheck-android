package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class Project extends RealmObject {

    @PrimaryKey
    @SerializedName("project_id")
    private String projectId;
    @SerializedName("project_name")
    private String projectName;;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String shipName) {
        this.projectName = projectName;
    }

}
