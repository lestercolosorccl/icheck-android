package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class BulkAction {

    @SerializedName("add")
    private List<BulkModel> add = null;
    @SerializedName("update")
    private List<BulkModel> update = null;

    public List<BulkModel> getAdd() {
        return add;
    }

    public void setAdd(List<BulkModel> add) {
        this.add = add;
    }

    public List<BulkModel> getUpdate() {
        return update;
    }

    public void setUpdate(List<BulkModel> update) {
        this.update = update;
    }
}
