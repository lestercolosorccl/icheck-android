package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class Area extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private String id;
    @SerializedName("area")
    private String area;
    @SerializedName("ship_id")
    private String shipId;
    @SerializedName("project_id")
    private String projectId;
    @SerializedName("project_name")
    private String projectName;
    @SerializedName("deck_id")
    private String deckId;
    @SerializedName("deck")
    private String deck;

    public String getAreaId() {
        return id;
    }

    public void setAreaId(String id) {
        this.id = id;
    }

    public String getAreaName() {
        return area;
    }

    public void setAreaName(String area) {
        this.area = area;
    }

    public String getShipId() {
        return shipId;
    }

    public void setShipId(String shipId) {
        this.shipId = shipId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

}
