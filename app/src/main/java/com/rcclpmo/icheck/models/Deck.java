package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class Deck extends RealmObject {


    @SerializedName("id")
    private String id;
    @PrimaryKey
    @SerializedName("deck")
    private String deckName;
    @SerializedName("project_id")
    private String projectId;
    @SerializedName("brand_id")
    private String brandId;
    @SerializedName("ship_id")
    private String shipId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getShipId() {
        return shipId;
    }

    public void setShipId(String shipId) {
        this.shipId = shipId;
    }

    public String getDeckName() {
        return deckName;
    }

    public void setDeckName(String deckName) {
        this.deckName = deckName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }
}