package com.rcclpmo.icheck.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class BulkModel implements Parcelable {

    @SerializedName("temp_id")
    private String tempId;
    @SerializedName("id")
    private String id;
    @SerializedName("filename")
    private String filename;

    protected BulkModel(Parcel in) {
        tempId = in.readString();
        id = in.readString();
        filename = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tempId);
        dest.writeString(id);
        dest.writeString(filename);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BulkModel> CREATOR = new Creator<BulkModel>() {
        @Override
        public BulkModel createFromParcel(Parcel in) {
            return new BulkModel(in);
        }

        @Override
        public BulkModel[] newArray(int size) {
            return new BulkModel[size];
        }
    };

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
