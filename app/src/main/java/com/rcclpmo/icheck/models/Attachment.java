package com.rcclpmo.icheck.models;

import com.google.gson.annotations.SerializedName;
import com.rcclpmo.icheck.constants.APIConstants;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class Attachment extends RealmObject{

    @PrimaryKey
    @SerializedName("id")
    private String id;
    @SerializedName("row_id")
    private String rowId;
    @SerializedName("uploaded_filename")
    private String uploadedName;
    @SerializedName("module_name")
    private String moduleName;
    @SerializedName("file_type")
    private String type;
    @SerializedName("date_added")
    private String datecreated;
    @SerializedName("added_by")
    private String userId;
    @SerializedName("uploaded_by")
    private String uploadedBy;
    private boolean isSync = true;
    private boolean isDeleted = false;

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getUploadedId() {
        return id;
    }

    public void setUploadedId(String id) {
        this.id = id;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getUploadedName() {
        return uploadedName;
    }

    public void setUploadedName(String uploadedName) {
        this.uploadedName = uploadedName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put(APIConstants.BULK_PARAM_ID,getUploadedId());
            jsonObject.put(APIConstants.BULK_PARAM_FILENAME,getUploadedName());

            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return jsonObject;
        }

    }

}