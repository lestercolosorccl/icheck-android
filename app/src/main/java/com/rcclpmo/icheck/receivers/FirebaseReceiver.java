package com.rcclpmo.icheck.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class FirebaseReceiver extends BroadcastReceiver {

    public static FirebaseNotificationListener listener;

    public FirebaseReceiver(){
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (listener!=null){
            listener.onReceive(intent);
        }
    }

    public interface FirebaseNotificationListener {
        void onReceive(Intent intent);
    }

}
