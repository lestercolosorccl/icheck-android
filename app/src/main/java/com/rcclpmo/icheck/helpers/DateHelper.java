package com.rcclpmo.icheck.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DateHelper {


    /****
     * SERVED AS TEMP_ID
     * @return
     */
    public static String getMillisDate() {
        Calendar c = Calendar.getInstance();
        String date = String.valueOf(System.currentTimeMillis());

        return date;
    }

    public static boolean isOvertime(String date) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        String str2 = getCurrentFormattedDate();
        Date meetingDate = null;
        Date date2 = null;
        try {
            meetingDate = dateFormatter.parse(date);
            date2 = dateFormatter.parse(str2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return meetingDate.compareTo(date2) < 0;

    }

    public static final boolean compareDate(Date startDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date endDate = null;
        long numberOfDays = 0;
        try {
            endDate = dateFormatter.parse(getCurrentFormattedDate());
            long timeDiff = endDate.getTime() - startDate.getTime();
            numberOfDays = TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return numberOfDays > 21;
    }

    public static String getCurrentFormattedDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormatter.format(c.getTime());

        return formattedDate;

    }

    public static String convertDate(Date date, boolean isForReport) {
        String newDateFormat = !isForReport ? "MMM-dd-yyyy" : "MM-dd-yyyy";
        SimpleDateFormat timeFormatter;
        timeFormatter = new SimpleDateFormat(newDateFormat);
        newDateFormat = timeFormatter.format(date);

        return newDateFormat;
    }

    public static String convertDateToStringForBulk(Date date) {
        SimpleDateFormat timeFormatter;
        timeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String newDateFormat = timeFormatter.format(date);

        return newDateFormat;
    }

    public static Date covertStringToDate(String mDate) {

        Date formattedDate = null;
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(mDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            mDate = dateFormatter.format(date);

            formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(mDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedDate;
    }

    /****
     * CURRENT DATE USE IN OFFLINE
     * @return
     */
    public static String getOfflineCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    public static String getCurrentFormattedDateForPDF() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MMM-dd");
        String formattedDate = dateFormatter.format(c.getTime());

        return formattedDate;

    }

}
