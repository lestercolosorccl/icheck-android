package com.rcclpmo.icheck.helpers;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class VolleyPostRequestHelper extends Request<JSONObject> {

    private Response.Listener<JSONObject> listener;
    private Map<String, String> params;

    public VolleyPostRequestHelper(int method, String url, Map<String, String> params,
                                   Response.Listener<JSONObject> reponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return params;
    };

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        // TODO Auto-generated method stub
        listener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        Map<String, String>  params = new HashMap<String, String>();

        ApplicationClass applicationClass = ApplicationClass.getInstance();

        //todo check if header value is null
        params.put(APIConstants.HEADER_GLOBAL_PROJECT_ID, applicationClass.getShipId()!=null?
                applicationClass.getShipId():"");
        params.put(APIConstants.HEADER_MODULE_NAME, APIConstants.HEADER_VALUE_MODULE_NAME);
        params.put(APIConstants.HEADER_GLOBAL_USER_ID, applicationClass.getUserId()!=null?
                applicationClass.getUserId():"" );
        params.put(APIConstants.HEADER_IS_I_95_MOBILE, APIConstants.HEADER_VALUE_IS_MOBILE);
        params.put(APIConstants.HEADER_PLATFORM, APIConstants.HEADER_VALUE_PLATFORM);
        params.put(APIConstants.HEADER_DEVICE_ID,applicationClass.getDeviceId()!=null?
                applicationClass.getDeviceId():"" );

        return params;
    }
}
