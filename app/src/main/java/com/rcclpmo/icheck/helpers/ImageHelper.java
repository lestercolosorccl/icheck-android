package com.rcclpmo.icheck.helpers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.rcclpmo.icheck.BuildConfig;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.CommonConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class ImageHelper {



    public static void handleDownloadImage(Context context, Object object, String fileName, boolean isFromLocal){
        Bitmap image = (Bitmap) object;

        /****
         * saving bitmap image to file storage
         */
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        if (!root.isDirectory()){
            root.mkdirs();
        }
        String path = String.format("%s/%s/", ApplicationClass.getInstance().getFilesDir().toString(),
                context.getString(R.string.attachment_path));

        //check if from local
        String imageFileName = isFromLocal? String.format("%s.%s",fileName,
                CommonConstants.IMAGE_FILE_FORMAT): fileName;

        FileOutputStream out = null;
        File file = new File(path, imageFileName);
        try {

            File filePath = new File(path);
            if (!filePath.isDirectory()){
                filePath.mkdirs();
            }


            out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 70, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void handleDownloadImage(Context context, String fileName,String bgPath, boolean isFromLocal){
//        Bitmap image = (Bitmap) object;
//        Bitmap image = FileHelper.decodeSampledBitmapFromFile(bgPath,800,900);

        Bitmap image = rotateImage(bgPath);


        /****
         * saving bitmap image to file storage
         */
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        if (!root.isDirectory()){
            root.mkdirs();
        }
        String path = String.format("%s/%s/", ApplicationClass.getInstance().getFilesDir().toString(),
                context.getString(R.string.attachment_path));

        //check if from local
        String imageFileName = isFromLocal? String.format("%s.%s",fileName,
                CommonConstants.IMAGE_FILE_FORMAT): fileName;

        FileOutputStream out = null;
        File file = new File(path, imageFileName);
        try {

            File filePath = new File(path);
            if (!filePath.isDirectory()){
                filePath.mkdirs();
            }


            out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 70, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static Bitmap rotateImage(String bgPath){
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        Bitmap bm = FileHelper.decodeSampledBitmapFromFileWithBounds(bgPath,500,600,bounds);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(bgPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);

        return rotatedBitmap;
    }

    public static String getAttachmentPath(Context context){
        String path = String.format("%s/%s", ApplicationClass.getInstance().getFilesDir().toString(),
                context.getString(R.string.attachment_path));

        return path;
    }

    public static String getDownloadAttachmentPath(Context context){
        String path = String.format("%s/%s/", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString(),
                context.getString(R.string.attachment_path));

        return path;
    }

    public static boolean isCamera(Intent data){
        final boolean isCamera;
        if (data == null) {
            isCamera = true;
        } else {
            final String action = data.getAction();
            if (action == null) {
                isCamera = false;
            } else {
                isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            }
        }

        return isCamera;
    }

    public static List<Intent> getCameraIntent(Context context, Uri outputFileUri){
        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        return cameraIntents;
    }

    public static Uri getImageMainDirectory(Context context){

        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        if (!root.isDirectory()){
            root.mkdirs();
        }
        final String fname = DateHelper.getMillisDate();
        final File sdImageMainDirectory = new File(root, fname);


        if (Build.VERSION.SDK_INT > 21) {

            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider",
                    sdImageMainDirectory);
        }else {
            return Uri.fromFile(sdImageMainDirectory);
        }

    }

    public static void renameFile(Context context, String oldFileName,String newFileName){
        File dir = new File(getAttachmentPath(context));
        if(dir.exists()){
            File from = new File(dir,oldFileName);
            File to = new File(dir,newFileName);
            from.renameTo(to);

        }

    }

    public static Uri getFileDirectoryDirectory(Context context, File myFile){

        if (Build.VERSION.SDK_INT > 21) {

            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider",
                    myFile);
        }else {
            return Uri.fromFile(myFile);
        }

    }
}
