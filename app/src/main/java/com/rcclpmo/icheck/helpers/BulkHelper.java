package com.rcclpmo.icheck.helpers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;
import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.BulkModel;
import com.rcclpmo.icheck.models.ICheckItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class BulkHelper {

    public static String formatBulkActions(Context context){
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        SyncDBTransaction dbTransaction = new SyncDBTransaction(context);

        List<ICheckItem> ICheckItems = dbTransaction.getUnsyncObjects(ICheckItem.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());
            JSONArray jsonArray = new JSONArray();

            for (ICheckItem ICheckItem : ICheckItems){
                jsonArray.put(ICheckItem.toJSON());
            }

            jsonObject.put(APIConstants.BULK_PARAM_ITEMS, jsonArray);

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

    public static void addOrUpdateWAlkThruRemark(Context context, BulkModel model, boolean isAdd){
        SyncDBTransaction dbTransaction = new SyncDBTransaction(context);

        if (!isAdd){
            Log.v("lester-log-request", model.getId());
            ICheckItem remarkCopy = (ICheckItem) dbTransaction.getObjectCopy(ICheckItem.class, DatabaseConstants.KEY_ID, model.getId());


            remarkCopy.setSync(true);
//            updateUploadFileRowId(context, model.getTempId(), model.getId(), null);
            dbTransaction.update(remarkCopy);
            //            if (!remarkCopy.getIsDeleted().equals("0")){
//                remarkCopy.setId(model.getId());
//                remarkCopy.setSync(true);
//                dbTransaction.add(remarkCopy);
//
//            }else {
//                dbTransaction.deleteRealmObject(ICheckItem.class, model.getId(), DatabaseConstants.KEY_ID);
//
//            }

        }else {

            ICheckItem remarkCopy = (ICheckItem) dbTransaction.getObjectCopy(ICheckItem.class, DatabaseConstants.KEY_ID, model.getTempId());

            remarkCopy.setId(model.getId());
            remarkCopy.setSync(true);

            /****update attachments row id*/
            updateUploadFileRowId(context, model.getTempId(), model.getId(), null);

            dbTransaction.add(remarkCopy);
            dbTransaction.deleteRealmObject(ICheckItem.class, model.getTempId(), DatabaseConstants.KEY_ID);

        }

    }

    private static void updateUploadFileRowId(Context context, String oldRowId, String newRowId, @Nullable String inspectionId){
        SyncDBTransaction dbTransaction = new SyncDBTransaction(context);

        List<Attachment> files = dbTransaction.getUploadFileList(Attachment.class, oldRowId);

        if (files!=null){
            if (files.size()!=0){
                for(Attachment toBeUpdate: files){

                    Attachment item = (Attachment) dbTransaction.copyFromRealm(toBeUpdate);
                    item.setRowId(newRowId);
                    //todo check if uploaded file is from newly added inspection

                    //todo update attachment parent id
                    if (inspectionId!=null){
                        item.setRowId(inspectionId);
                    }
                    dbTransaction.deleteRealmObject(Attachment.class, item.getUploadedId(), DatabaseConstants.KEY_UPLOADED_ID);
                    dbTransaction.add(item);

                }
            }

        }
    }


    public static String formatDeleteBulkAttachment(Context context){
        ApplicationClass applicationClass = ApplicationClass.getInstance();
        SyncDBTransaction dbTransaction = new SyncDBTransaction(context);

        List<Attachment> files = dbTransaction.getDeletedObjects(Attachment.class);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(APIConstants.LOGIN_PARAM_ACCESS_TOKEN,applicationClass.getAccessToken());
            JSONArray jsonArray = new JSONArray();

            for (Attachment file: files){
                Log.v("lester-log-attachement", "id:"+file.getUploadedId()+"   | filename:"+file.getUploadedName());
                jsonArray.put(file.toJSON());
            }

            jsonObject.put(APIConstants.BULK_PARAM_ATTACHMENTS, jsonArray);

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }
}
