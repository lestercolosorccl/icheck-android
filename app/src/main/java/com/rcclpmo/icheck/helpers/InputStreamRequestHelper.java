package com.rcclpmo.icheck.helpers;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class InputStreamRequestHelper extends Request<byte[]> {


    private final Response.Listener<byte[]> mListener;
    private Map<String, String> mParams;

    //create a static map for directly accessing headers
    public Map<String, String> responseHeaders;

    public InputStreamRequestHelper(int method, String mUrl, Response.Listener<byte[]> listener,
                                    Response.ErrorListener errorListener, HashMap<String, String> params) {
        // TODO Auto-generated constructor stub

        super(method, mUrl, errorListener);
        // this request would never use cache.
        setShouldCache(false);
        mListener = listener;
        mParams = params;
    }

    @Override
    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return mParams;
    }

    ;


    @Override
    protected void deliverResponse(byte[] response) {
        mListener.onResponse(response);
    }

    @Override
    protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {

        //Initialise local responseHeaders map with response headers received
        responseHeaders = response.headers;

        //Pass the response data here
        return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        Map<String, String> params = new HashMap<String, String>();

        ApplicationClass applicationClass = ApplicationClass.getInstance();

        //todo check if header value is null
        params.put(APIConstants.HEADER_GLOBAL_PROJECT_ID, applicationClass.getShipId() != null ?
                applicationClass.getShipId() : "");
        params.put(APIConstants.HEADER_MODULE_NAME, APIConstants.HEADER_VALUE_MODULE_NAME);
        params.put(APIConstants.HEADER_GLOBAL_USER_ID, applicationClass.getUserId() != null ?
                applicationClass.getUserId() : "");
        params.put(APIConstants.HEADER_IS_I_95_MOBILE, APIConstants.HEADER_VALUE_IS_MOBILE);
        params.put(APIConstants.HEADER_PLATFORM, APIConstants.HEADER_VALUE_PLATFORM);
        params.put(APIConstants.HEADER_DEVICE_ID, applicationClass.getDeviceId() != null ?
                applicationClass.getDeviceId() : "");

        return params;
    }
}