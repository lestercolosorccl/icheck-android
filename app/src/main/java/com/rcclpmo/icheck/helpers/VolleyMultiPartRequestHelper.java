package com.rcclpmo.icheck.helpers;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.rcclpmo.icheck.constants.APIConstants;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class VolleyMultiPartRequestHelper extends Request {

    private HttpEntity mHttpEntity;
    private final Class mClass;
    private Response.Listener mListener;
    private Map<String, String> mHeaders;

    private final Gson gson;

    public VolleyMultiPartRequestHelper(String url, String path, Class clazz,
                                        Map<String, String> headers,
                                        Response.Listener listener,
                                        Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, errorListener);
        mHeaders = headers;
        mClass = clazz;
        mListener = listener;
        gson = new Gson();
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        File file  = new File(path);
        String fileName = file.getName();
        //todo temp remove file type
//        builder.addBinaryBody(APIConstants.ATTACHMENT_PARAM_FILE, file, ContentType.create("image/*"), fileName);
        builder.addBinaryBody(APIConstants.ATTACHMENT_PARAM_FILE, file);

        for (Map.Entry<String, String> entry : mHeaders.entrySet()) {

            builder.addTextBody(entry.getKey(), entry.getValue());

        }

        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary("BOUNDARY");

        mHttpEntity = builder.build();

    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
    @Override
    protected void deliverResponse(Object response) {
        mListener.onResponse(response);
    }

    @Override
    public int compareTo(Object object) {
        return 0;
    }

//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//
//        mHeaders.put("Content-Type","multipart/form-data; charset=utf-8");
//        return mHeaders != null ? mHeaders : super.getHeaders();
//    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//
//        Map<String, String>  params = new HashMap<String, String>();
//
//        ApplicationClass applicationClass = ApplicationClass.getInstance();
//
//        mHeaders.put("Content-Type","multipart/form-data; charset=utf-8");
//        //todo check if header value is null
//        params.put(APIConstants.HEADER_GLOBAL_PROJECT_ID, applicationClass.getShipId()!=null?
//                applicationClass.getShipId():"");
//        params.put(APIConstants.HEADER_MODULE_NAME, APIConstants.HEADER_VALUE_MODULE_NAME);
//        params.put(APIConstants.HEADER_GLOBAL_USER_ID, applicationClass.getUserId()!=null?
//                applicationClass.getUserId():"" );
//        params.put(APIConstants.HEADER_IS_I_95_MOBILE, APIConstants.HEADER_VALUE_IS_MOBILE);
//        params.put(APIConstants.HEADER_PLATFORM, APIConstants.HEADER_VALUE_PLATFORM);
//        params.put(APIConstants.HEADER_DEVICE_ID,applicationClass.getDeviceId()!=null?
//                applicationClass.getDeviceId():"" );
//
//        return params;
//    }
}
