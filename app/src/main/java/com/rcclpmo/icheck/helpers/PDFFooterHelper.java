package com.rcclpmo.icheck.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.rcclpmo.icheck.R;

import java.io.ByteArrayOutputStream;

/**
 * Created by Patrick Villanueva on 05/01/2018.
 */

public class PDFFooterHelper extends PdfPageEventHelper {

    private String headerTitle, headerReporter;
    private String footer;
    private Image imageHeader;
    private Context context;

    public PDFFooterHelper(Context context, String headerTitle,String headerReporter, String footer){
        this.headerTitle = headerTitle;
        this.headerReporter = headerReporter;
        this.footer = footer;
        this.context = context;
    }


    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();

        FontSelector selector = new FontSelector();
        FontSelector selector2 = new FontSelector();
        FontSelector selector3 = new FontSelector();


        Font mFont = FontFactory.getFont(FontFactory.defaultEncoding, 20, BaseColor.DARK_GRAY);
        selector.addFont(mFont);
        Phrase headerTitle = selector.process(this.headerTitle);

        mFont = FontFactory.getFont(FontFactory.defaultEncoding, 14, BaseColor.DARK_GRAY);
        selector2.addFont(mFont);
        Phrase headerReporter = selector2.process(this.headerReporter);

        mFont = FontFactory.getFont(FontFactory.defaultEncoding, 12, BaseColor.DARK_GRAY);
        selector3.addFont(mFont);
        Phrase footer = selector3.process(this.footer);

        ColumnText.showTextAligned(cb, Element.ALIGN_BASELINE,
                headerTitle,
                document.leftMargin(),
                document.top() + 30, 0);
        ColumnText.showTextAligned(cb, Element.ALIGN_BASELINE,
                headerReporter,
                document.leftMargin(),
                document.top() +15, 0);

        try {
            Drawable d = context.getResources().getDrawable(R.drawable.ic_rcl_logo);
            BitmapDrawable bitDw = ((BitmapDrawable) d);
            Bitmap bmp = bitDw.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            imageHeader = Image.getInstance(stream.toByteArray());
            imageHeader.scaleToFit(30,30);
            imageHeader.setAbsolutePosition(document.right()-20,document.top()+20);
            document.add(imageHeader);

        } catch (Exception x) {
            x.printStackTrace();
        }

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                footer,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 20, 0);

    }
}
