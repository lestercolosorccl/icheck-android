package com.rcclpmo.icheck.helpers;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class VolleyStringRequestHelper extends StringRequest {


    private Response.Listener<JSONObject> listener;

    public VolleyStringRequestHelper(int method, String url, Response.Listener<String> listener,
                                     Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        Map<String, String>  params = new HashMap<>();

        ApplicationClass applicationClass = ApplicationClass.getInstance();

        //todo check if header value is null
        params.put(APIConstants.HEADER_GLOBAL_PROJECT_ID, applicationClass.getShipId()!=null?
                applicationClass.getShipId():"");
        params.put(APIConstants.HEADER_MODULE_NAME, APIConstants.HEADER_VALUE_MODULE_NAME);
        params.put(APIConstants.HEADER_GLOBAL_USER_ID, applicationClass.getUserId()!=null?
                applicationClass.getUserId():"" );
        params.put(APIConstants.HEADER_IS_I_95_MOBILE, APIConstants.HEADER_VALUE_IS_MOBILE);
        params.put(APIConstants.HEADER_PLATFORM, APIConstants.HEADER_VALUE_PLATFORM);
        params.put(APIConstants.HEADER_DEVICE_ID,applicationClass.getDeviceId()!=null?
                applicationClass.getDeviceId():"" );

        return params;
    }
}
