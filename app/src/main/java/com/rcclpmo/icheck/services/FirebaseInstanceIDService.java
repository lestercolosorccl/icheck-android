package com.rcclpmo.icheck.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.rcclpmo.icheck.api.FirebaseAPI;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.FirebaseConstants;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.ResponseHandler;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class FirebaseInstanceIDService extends FirebaseInstanceIdService implements ResponseHandler {

    private static final String TAG = FirebaseInstanceIDService.class.getSimpleName();

    private ApplicationClass applicationClass;
    private SyncDBTransaction dbTransaction;
    private RequestQueue requestQueue;


    @Override
    public void onCreate() {
        super.onCreate();

        applicationClass = ApplicationClass.getInstance();
        dbTransaction = new SyncDBTransaction(this);
        requestQueue = applicationClass.getRequestQueue();

    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(FirebaseConstants.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.i("FIREBASE_TAG", "sendRegistrationToServer: " + token);
        if (applicationClass.isUpdateFirebaseToken()){
            requestUpdateFirebaseToken();
            // TODO: 18/10/2017 request for update token
        }

    }

    private void storeRegIdInPref(String token) {

        if (applicationClass.isWithOldFirebaseToken() && applicationClass.isLoggedIn()){

            String oldToken = applicationClass.getNewFirebaseToken();
            applicationClass.saveOldFirebaseToken(oldToken);
        }

        applicationClass.saveNewFirebaseToken(token);

//        SharedPreferences pref = getApplicationContext().getSharedPreferences(FirebaseConstants.SHARED_PREF, 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString(FirebaseConstants.FIREBASE_ID, token);
//        editor.commit();

    }

    @Override
    public void onSuccess(int requestCode, Object object) {

        switch (requestCode){

            case APIRequestCode.CODE_FIREBASE_UPDATE_TOKEN:
                Log.i("FIREBASE_TAG", "sendRegistrationToServer: " +object);
                break;
        }

    }

    @Override
    public void onFailed(int requestCode, String message) {

        switch (requestCode){

            case APIRequestCode.CODE_FIREBASE_UPDATE_TOKEN:

                Log.i("FIREBASE_TAG", "sendRegistrationToServer: " +message);
                break;
        }
    }


    private void requestUpdateFirebaseToken(){

        Request request = FirebaseAPI.updateFirebaseToken(APIRequestCode.CODE_FIREBASE_UPDATE_TOKEN,this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);
    }

}
