package com.rcclpmo.icheck.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.api.UserAPI;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.models.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DashboardAPIRequestService extends Service implements ResponseHandler {

    private RequestQueue requestQueue;
    private ApplicationClass applicationClass;
    private SyncDBTransaction dbTransaction;

    private List<Project> projectList;
    private int projectPosition = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationClass = ApplicationClass.getInstance();
        requestQueue = applicationClass.getRequestQueue();
        dbTransaction = new SyncDBTransaction(this);

        projectList = new ArrayList<>();

        requestGetProjects();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("SERVICE TAG","ONDESTROY");

    }


    @Override
    public void onSuccess(int requestCode, Object object) {

        switch (requestCode) {

            case APIRequestCode.CODE_GET_PROJECT_LIST:
                handleListResponse(object);
                projectList.addAll(dbTransaction.getAll(Project.class));

                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_PROJECT_LIST);
//                projectPosition = 0;
//                if (projectList.size()>0){
//                    requestGetVenues(projectList.get(projectPosition).getShipId());
//                    projectPosition = projectPosition +1;
//                }else {
//                    requestGetDisciplines();
//                }
                requestGetVenues();
                break;


            case APIRequestCode.CODE_GET_AREAS:
                handleListResponse(object);
                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_VENUES);
//                if (projectPosition < projectList.size()){
//                    requestGetVenues(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//
//                }else {
//                    projectPosition = 0;
//                    requestGetDecks(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//                }
                requestGetDisciplines();
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_DECKS:
                handleListResponse(object);
                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_DISCIPLINES);
//                if (projectPosition < projectList.size()){
//                    requestGetDecks(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//
//                }else {
//                    projectPosition = 0;
//                    requestGetDisciplines();
//                }
                requestGetDisciplines();
                break;

            case APIRequestCode.CODE_GET_ALL_DISCIPLINES:
                handleListResponse(object);
                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_MEETING_TYPE);
                requestGetUsers();
                break;

            case APIRequestCode.CODE_GET_USERS:
                handleListResponse(object);
                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_USERS);
//                requestGetSuppliers();
                break;


            case APIRequestCode.CODE_GET_SUPPLIERS:
                handleListResponse(object);
                Log.i("SERVICE TAG",APIRequestCode.TAG_GET_SUPPLIERS);
                this.stopSelf();
                break;

            default:
                break;

        }

    }

    @Override
    public void onFailed(int requestCode, String message) {

        switch (requestCode) {

            case APIRequestCode.CODE_GET_PROJECT_LIST:
                projectPosition = 0;
//                if (projectList.size()>0){
//                    requestGetVenues(projectList.get(projectPosition).getShipId());
//                    projectPosition = projectPosition +1;
//                }else {
//                    requestGetDisciplines();
//                }
                requestGetVenues();
                break;


            case APIRequestCode.CODE_GET_AREAS:
//                if (projectPosition < projectList.size()){
//                    requestGetVenues(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//
//                }else {
//                    projectPosition = 0;
//                    requestGetDecks(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//                }
                requestGetDisciplines();
//                requestGetDecks();
                break;

            case APIRequestCode.CODE_GET_DECKS:
//                if (projectPosition < projectList.size()){
//                    requestGetDecks(projectList.get(projectPosition).getShipId());
//                    projectPosition= projectPosition+1;
//
//                }else {
//                    projectPosition = 0;
//                    requestGetDisciplines();
//                }
                requestGetDisciplines();
                break;

            case APIRequestCode.CODE_GET_ALL_DISCIPLINES:
                requestGetUsers();
                break;

            case APIRequestCode.CODE_GET_USERS:
                requestGetSuppliers();
                break;

            case APIRequestCode.CODE_GET_SUPPLIERS:
                this.stopSelf();
                break;

            default:
                break;

        }

    }

    private void requestGetProjects(){
        Request requestGetAllProjects = CommonAPI.getProjectList(APIRequestCode.CODE_GET_PROJECT_LIST,this);
        requestGetAllProjects.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(requestGetAllProjects);

    }

    private void requestGetVenues(){
        Request request = CommonAPI.getAreaVenues(APIRequestCode.CODE_GET_AREAS, this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);

    }

    private void handleListResponse(Object object){
        dbTransaction.add((List) object);


    }

    private void requestGetDecks(){
        Request request = CommonAPI.getDecks(APIRequestCode.CODE_GET_DECKS, this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);

    }

    private void requestGetDisciplines(){
        Request request = CommonAPI.getAllDisciplines(APIRequestCode.CODE_GET_ALL_DISCIPLINES, this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);

    }

    private void requestGetSuppliers(){
        Request request = CommonAPI.getSuppliers(APIRequestCode.CODE_GET_SUPPLIERS, this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);

    }


    private void requestGetUsers(){
        Request request = UserAPI.getUsers(APIRequestCode.CODE_GET_USERS, this);
        request.setTag(APIRequestCode.REQUEST_TAG_SERVICE);
        requestQueue.add(request);

    }

}
