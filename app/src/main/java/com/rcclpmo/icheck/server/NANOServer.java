package com.rcclpmo.icheck.server;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class NANOServer extends NanoHTTPD {
    public Context ctx = null;

    private static NANOServer myInstance;

    public static final String
            MIME_PLAINTEXT = "text/plain",
            MIME_HTML = "text/html",
            MIME_JS = "application/javascript",
            MIME_CSS = "text/css",
            MIME_PNG = "image/png",
            MIME_DEFAULT_BINARY = "application/octet-stream",
            MIME_XML = "text/xml",
            MIME_JPEG = "image/jpeg";

    public NANOServer(Context ctx) throws IOException {
        super(8080);
        this.ctx = ctx;
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        System.out.println("\nRunning! Point your browsers to http://localhost:8080/ \n");
    }

//    public static void main(String[] args) {
//        try {
//            new MyServer();
//        } catch (IOException ioe) {
//            System.err.println("Couldn't start server:\n" + ioe);
//        }
//    }

    @Override
    public Response serve(IHTTPSession session) {
        String sessionUri = session.getUri();

        final StringBuilder buf = new StringBuilder();
        for (Map.Entry<String, String> kv : session.getHeaders().entrySet())
            buf.append(kv.getKey() + " : " + kv.getValue() + "\n");
        InputStream mbuffer = null;

        try {
            if(sessionUri!=null) {

                if (sessionUri.contains("canvas_drawing")) {

                    if (sessionUri.contains(".js")) {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_JS, mbuffer);
                    } else if (sessionUri.contains(".php")) {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_HTML, mbuffer);
                    } else if (sessionUri.contains(".css")) {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_CSS, mbuffer);

                    } else if (sessionUri.contains(".png")) {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_PNG, mbuffer);

                    } else {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_HTML, mbuffer);
                    }
                }else {
                    if (sessionUri.contains(".jpg")||sessionUri.contains(".JPG")
                            ||sessionUri.contains(".jpeg")||sessionUri.contains(".JPEG")){
                        File request = new File(sessionUri.substring(1));
                        mbuffer = new FileInputStream(request);
                        FileNameMap fileNameMap = URLConnection.getFileNameMap();
                        String mimeType = fileNameMap.getContentTypeFor(sessionUri);
                        return newChunkedResponse(Response.Status.OK, mimeType, mbuffer);

                    }else if (sessionUri.contains(".png")||sessionUri.contains(".PNG")){
                        File request = new File(sessionUri.substring(1));
                        mbuffer = new FileInputStream(request);
                        FileNameMap fileNameMap = URLConnection.getFileNameMap();
                        String mimeType = fileNameMap.getContentTypeFor(sessionUri);
                        return newChunkedResponse(Response.Status.OK, mimeType, mbuffer);

                    }else {
                        mbuffer = ctx.getAssets().open(sessionUri.substring(1));
                        return newChunkedResponse(Response.Status.OK, MIME_HTML, mbuffer);
                    }

                }
            }

        } catch (IOException e) {
            Log.d("SERVER","Error opening file"+sessionUri.substring(1));
            e.printStackTrace();
        }

        return null;

    }

}
