package com.rcclpmo.icheck.bases;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.interfaces.ResponseHandler;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public abstract class BaseDialogFragment extends DialogFragment implements View.OnClickListener, ResponseHandler {

    /**
     * Reference of ApplicationClass
     */
    public ApplicationClass applicationClass;

    /**
     * Integer for layoutID
     */
    private int layoutId = 0;

    /**
     * View to be animated
     */
    private View view;

    /**
     * Integer for entrance of animation
     */
    private int animationEnter = R.anim.anim_slide_in_right;

    /**
     * Integer for exit of animation
     */
    private int animationExit = R.anim.anim_slide_out_right;

    /***toolbar views*/
    private TextView tvTitle;
    private ImageView ivBack;


    /****
     * Container of image filename to be downloaded
     */
    private String imageFileName;


    public BaseDialogFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isCustomDialog()){
            setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);

        }else {

            // Hide title of the dialog
            setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        }

        //initialization of layout id, toolbar, and request tags
        initMain();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(layoutId, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        //passing of data through arguments
        initData(getArguments());

        //for initialization of views
        initViews(view, savedInstanceState);

        if (hasToolbar()){
            setUpToolbar(view);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        showParent();
    }

    /**
     * This method initializes layout id, toolbar, and request tag for requests
     */
    public abstract void initMain();
    /**
     *This method provides data passed through arguments
     */
    public abstract void initData(Bundle bundle);
    /**
     * This method initializes views included in the layout
     * @param view
     * @param savedInstanceState
     */
    /****/
    protected abstract boolean hasToolbar();

    protected abstract boolean isCustomDialog();


    /*****
     * This method handle common API implementation in all dialog fragment
     * @param requestCode
     */
    protected abstract void handleCommonAPI(int requestCode);

    public abstract void initViews(View view, Bundle savedInstanceState);

    public void setToolbarTitle(String title){
        tvTitle.setText(title);
    }

    public void setUpToolbar(View view){
//        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//        ivBack = (ImageView) view.findViewById(R.id.ivBackButton);
//        ivBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideParent();
//            }
//        });
    }

    /**
     * Animates dialogFragment onShow
     */
    private void showParent() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.startAnimation(AnimationUtils.loadAnimation(getActivity(), animationEnter));
                view.setVisibility(View.VISIBLE);
                viewDidLoad();
            }
        }, 100);

    }

    public void viewDidLoad() {

    }

    /**
     * Animates dialogFragment onStop
     */
    public void hideParent() {

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
                view.startAnimation(AnimationUtils.loadAnimation(getActivity(), animationExit));
                view.setVisibility(View.GONE);
                dismiss();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
////
//                    }
//                }, 100);
//            }
//        }, 100);

    }


    /**
     * Sets the layout id of the activity
     * @param layoutId
     */
    public void setLayoutId(int layoutId) {

        this.layoutId = layoutId;

    }

    /**
     * Sets view to be animated
     * @param view
     */
    public void setViewToAnimate(View view) {

        this.view = view;

    }

    /**
     * Sets entrance of animation
     * @param animationEnter
     */
    public void setAnimationEnter(int animationEnter) {

        this.animationEnter = animationEnter;

    }

    /**
     * Sets exit of animation
     * @param animationExit
     */
    public void setAnimationExit(int animationExit) {

        this.animationExit = animationExit;

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        switch (requestCode){

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                handleDownloadImage(object);
                handleCommonAPI(requestCode);
                break;

            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                handleDownloadDocuments(object);
                handleCommonAPI(requestCode);
                break;

            default:break;
        }
    }

    @Override
    public void onFailed(int requestCode, String message) {

    }

    public Request downloadDocFile(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadFileFromServer(APIRequestCode.CODE_DOWNLOAD_DOC_FILE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_DOC_FILE);
        return request;

    }

    public Request downloadImage(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadImageFromServer(APIRequestCode.CODE_DOWNLOAD_IMAGE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_IMAGE);
        return request;

    }

    private void handleDownloadImage(Object object){
        Bitmap image = (Bitmap) object;

        /****
         * saving bitmap image to file storage
         */
        String path = String.format("%s/%s/", ApplicationClass.getInstance().getFilesDir().toString(),
                getString(R.string.attachment_path));

        FileOutputStream out = null;
        File file = new File(path, imageFileName);
        try {

            File filePath = new File(path);
            if (!filePath.isDirectory()){
                filePath.mkdirs();
            }


            out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void handleDownloadDocuments(Object object){

        String path = String.format("%s/%s/",ApplicationClass.getInstance().getFilesDir().toString(),
                getString(R.string.attachment_path));

        FileOutputStream outputStream = null;
        byte[] fileResponse = (byte[]) object;

        File file = new File(path, imageFileName);//todo note: doc FILENAME
        try {

            //covert reponse to input stream
            InputStream input = new ByteArrayInputStream(fileResponse);

            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
            byte data[] = new byte[1024];

            long total = 0;
            int count = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

//            File filePath = new File(path);
//            if (!filePath.isDirectory()){
//                filePath.mkdirs();
//            }
//
//
//            outputStream = new FileOutputStream(file);
//            outputStream.write(fileResponse);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}

