package com.rcclpmo.icheck.bases;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.business.ApplicationClass;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.dao.SyncDBTransaction;
import com.rcclpmo.icheck.interfaces.ResponseHandler;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public abstract class BaseFragmentActivity extends Fragment implements ResponseHandler {

    private SyncDBTransaction dbTransaction;

    private String imageFileName;

    public static Fragment createInstance(@Nullable Bundle args){

        Fragment fragment = new Fragment();
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbTransaction = new SyncDBTransaction(getActivity());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view, savedInstanceState);
    }

    /**
     * Initializes views included in the layout. This is called in onViewCreated method
     * @param view
     * @param savedInstanceState
     */
    protected abstract void initViews(View view, Bundle savedInstanceState);

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("Fragment", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("Fragment", "Error setting mChildFragmentManager field", e);
            }
        }
    }

    public abstract void handleCommonAPI(int requestCode);


    public Request downloadImage(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadImageFromServer(APIRequestCode.CODE_DOWNLOAD_IMAGE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_IMAGE);
        return request;

    }

    public Request downloadDocFile(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadFileFromServer(APIRequestCode.CODE_DOWNLOAD_DOC_FILE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_DOC_FILE);
        return request;

    }


    @Override
    public void onSuccess(int requestCode, Object object) {

        switch (requestCode){

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                handleDownloadImage(object);
                handleCommonAPI(requestCode);
                break;

            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                handleDownloadDocuments(object);
                handleCommonAPI(requestCode);

            default:break;
        }

    }

    @Override
    public void onFailed(int requestCode, String message) {
        handleCommonAPI(requestCode);
    }


    private void handleDownloadImage(Object object){
        Bitmap image = (Bitmap) object;

        /****
         * saving bitmap image to file storage
         */
        String path = String.format("%s/%s/", ApplicationClass.getInstance().getFilesDir().toString(),
                getString(R.string.attachment_path));

        FileOutputStream out = null;
        File file = new File(path, imageFileName);
        try {

            File filePath = new File(path);
            if (!filePath.isDirectory()){
                filePath.mkdirs();
            }


            out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void handleDownloadDocuments(Object object){

        String path = String.format("%s/%s/",ApplicationClass.getInstance().getFilesDir().toString(),
                getString(R.string.attachment_path));

        FileOutputStream outputStream = null;
        byte[] fileResponse = (byte[]) object;

        File file = new File(path, imageFileName);//todo note: doc FILENAME
        try {

            //covert reponse to input stream
            InputStream input = new ByteArrayInputStream(fileResponse);

            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
            byte data[] = new byte[1024];

            long total = 0;
            int count = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

//            File filePath = new File(path);
//            if (!filePath.isDirectory()){
//                filePath.mkdirs();
//            }
//
//
//            outputStream = new FileOutputStream(file);
//            outputStream.write(fileResponse);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}


