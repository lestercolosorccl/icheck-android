package com.rcclpmo.icheck.bases;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.rcclpmo.icheck.interfaces.RecyclerViewClickListener;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener{

    private RecyclerViewClickListener<T> listener;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public BaseViewHolder(View itemView, RecyclerViewClickListener<T> listener) {
        super(itemView);
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (this.listener!=null){
            this.listener.onRecyclerItemClick(getAdapterPosition(), getObject());
        }
    }

    public abstract T getObject();

    /**
     * Binds an object with the ViewHolder
     * @param object
     */
    public abstract void setViews(T object);

}