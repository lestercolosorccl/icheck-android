package com.rcclpmo.icheck.bases;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.android.volley.Request;
import com.rcclpmo.icheck.R;
import com.rcclpmo.icheck.api.CommonAPI;
import com.rcclpmo.icheck.constants.APIRequestCode;
import com.rcclpmo.icheck.constants.FirebaseConstants;
import com.rcclpmo.icheck.helpers.FileHelper;
import com.rcclpmo.icheck.helpers.ImageHelper;
import com.rcclpmo.icheck.interfaces.ResponseHandler;
import com.rcclpmo.icheck.utilities.NotificationUtils;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, ResponseHandler {

    //abstract methods
    protected abstract void initData(Intent data);
    protected abstract int getLayoutId();
    protected abstract void initViews();
    protected abstract void handleRefreshToken();
    protected abstract void handleCommonAPI(int requestCode);
    protected abstract void handleNotification(String message);


    private String imageFileName;
    private BroadcastReceiver registrationBroadcastReceiver;

    /**
     * Integer for entrance of animation
     */
    private int animationEnter = R.anim.anim_slide_in_left;

    /**
     * Integer for exit of animation
     */
    private int animationExit = R.anim.anim_slide_out_right;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getData();
        setContentView(getLayoutId());
        initViews();

        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);

        initBroadCast();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(registrationBroadcastReceiver,
                new IntentFilter(FirebaseConstants.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(registrationBroadcastReceiver,
                new IntentFilter(FirebaseConstants.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

    }

    private void initBroadCast(){


        registrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(FirebaseConstants.REGISTRATION_COMPLETE)) {
                    //// TODO: 20/10/2017 check registered firebase token (optional)

                } else if (intent.getAction().equals(FirebaseConstants.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    handleNotification(message);

                }
            }
        };

    }

    private void getData(){
        Intent data = getIntent();
        initData(data);
    }

    @Override
    public void onClick(View view) {

        // TODO: 10/5/2016 catch all common click listeners
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(animationEnter, animationExit);
    }

    @Override
    public void onSuccess(int requestCode, Object object) {
        switch (requestCode){

            case APIRequestCode.CODE_DOWNLOAD_IMAGE:
                handleDownloadImage(object);
                handleCommonAPI(requestCode);
                break;

            case APIRequestCode.CODE_DOWNLOAD_DOC_FILE:
                handleDownloadDocuments(object);
                handleCommonAPI(requestCode);
                break;


            default:break;
        }
    }

    @Override
    public void onFailed(int requestCode, String message) {
        handleCommonAPI(requestCode);

    }

    public Request downloadImage(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadImageFromServer(APIRequestCode.CODE_DOWNLOAD_IMAGE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_IMAGE);
        return request;

    }

    public Request downloadDocFile(String fileName){

        imageFileName = fileName;

        Request request = CommonAPI.downloadFileFromServer(APIRequestCode.CODE_DOWNLOAD_DOC_FILE, fileName, this);
        request.setTag(APIRequestCode.TAG_DOWNLOAD_DOC_FILE);
        return request;

    }

    private void handleDownloadDocuments(Object object){
        FileHelper.handleDownloadDocuments(getApplicationContext(), object, imageFileName, false);

    }

    private void handleDownloadImage(Object object){

        ImageHelper.handleDownloadImage(getApplicationContext(), object, imageFileName, false);

    }

}
