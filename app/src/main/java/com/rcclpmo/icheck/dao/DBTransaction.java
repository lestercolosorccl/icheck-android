package com.rcclpmo.icheck.dao;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public abstract class DBTransaction <T extends RealmObject> {


    public abstract boolean add(T object);

    public abstract boolean add(List<T> objects);

    public abstract boolean delete(Class<T> className, T object);

    public abstract boolean update(T object);

    public abstract boolean isExisting(Class<T> className, Object object);

    public abstract List<T> getAll(Class<T> className);

    public abstract void deleteAll(Class<T> className);

}