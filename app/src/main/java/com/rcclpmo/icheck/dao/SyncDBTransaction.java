package com.rcclpmo.icheck.dao;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rcclpmo.icheck.constants.DatabaseConstants;
import com.rcclpmo.icheck.helpers.StringHelper;
import com.rcclpmo.icheck.models.ICheckItem;
import com.rcclpmo.icheck.models.ICheckItemData;
import com.rcclpmo.icheck.models.Attachment;
import com.rcclpmo.icheck.models.FilterDataList;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class SyncDBTransaction<T extends RealmObject> extends DBTransaction<T> {
    private Context context;

    public SyncDBTransaction(Context context) {
        this.context = context;
    }

    /**
     * Adds record to realm database
     *
     * @param object
     * @return
     */
    @Override
    public boolean add(final T object) {

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.copyToRealmOrUpdate(object);
            }
        });
        realm.close();

        return false;
    }

    @Override
    public boolean add(final List<T> objects) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.copyToRealmOrUpdate(objects);
            }
        });
        realm.close();

        return false;
    }

    /**
     * Deletes specified object from realm database
     *
     * @param object
     * @return
     */

    @Override
    public boolean delete(final Class<T> className, final T object) {
        Realm realm = Realm.getDefaultInstance();


        return false;
    }

    /**
     * Same function as add, only using update for
     * proper wording
     *
     * @param object
     * @return
     */
    @Override
    public boolean update(final T object) {

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.copyToRealmOrUpdate(object);
            }
        });

        realm.close();
        return false;
    }

    @Override
    public boolean isExisting(Class<T> className, Object object) {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<T> results = null;

        if (object instanceof String) {

            String value = (String) object;
//            results = realm.where(className).equalTo("",value).findAll();

        }

        realm.close();
        return results.size() > 0;

    }

    /**
     * Returns a list of objects queried
     *
     * @param className
     * @return
     */
    @Override
    public List<T> getAll(final Class<T> className) {

        Realm realm = Realm.getDefaultInstance();

        return realm.where(className).findAll();

    }

    /**
     * Delete all objects from given class
     *
     * @param className
     */
    @Override
    public void deleteAll(final Class<T> className) {

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.delete(className);
            }
        });

        realm.close();
    }

    public T getFirstObject(final Class<T> className) {

        Realm realm = Realm.getDefaultInstance();

        return realm.where(className).findFirst();
    }



    public void deleteRealmObject(final Class<T> className, final String id, final String key) {

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                T obj = realm.where(className).equalTo(key, id).findFirst();
                if (obj != null) {
                    obj.deleteFromRealm();
                }
            }
        });

        realm.close();
    }

    public T getDynamicRealmObject(final Class<T> className, final String key, final String id){
        Realm realm = Realm.getDefaultInstance();


        return realm.where(className).equalTo(key, id).findFirst();
    }


    public List<T> getDynamicRealmListObject(final Class<T> className, final String key, final String id){
        Realm realm = Realm.getDefaultInstance();


        return realm.where(className).equalTo(key, id).findAll();
    }

    public T copyFromRealm(T object){
        Realm realm = Realm.getDefaultInstance();

        return realm.copyFromRealm(object);
    }


    public List<T> getAllByShipId(final Class<T> className, String shipId) {

        Realm realm = Realm.getDefaultInstance();

        return realm.where(className).equalTo(DatabaseConstants.KEY_ID,shipId).
                findAll();

    }

    public List<T> getActionsWithFilter(final  Class<T> className, ICheckItemData queryData, List<FilterDataList> dataList){

        Realm realm = Realm.getDefaultInstance();

        RealmQuery<T> query = realm.where(className);

        if (queryData!=null){


            if (queryData.getProjectId() != ""){
                query.equalTo(DatabaseConstants.KEY_PROJECT_ID, queryData.getProjectId());
            }
            if (queryData.getAreaId() != ""){
                query.equalTo(DatabaseConstants.KEY_AREA_ID, queryData.getAreaId());
            }

            if (queryData.getClaimOwner() != ""){
                query.equalTo(DatabaseConstants.KEY_ACTION_OWNER_ID, queryData.getClaimOwner());
            }

            if (queryData.getStatus() != "" && !"All".equals(queryData.getStatus())){
                query.equalTo(DatabaseConstants.KEY_STATUS, queryData.getStatus());
            }

            if (queryData.getPriority() != ""  && !"All".equals(queryData.getPriority())){
                query.equalTo(DatabaseConstants.KEY_PRIORITY, queryData.getPriority());
            }

            if (queryData.getDisciplineId() != ""){
                query.equalTo(DatabaseConstants.KEY_DISCIPLINE_ID, queryData.getDisciplineId());
            }

        }
        //todo this is a temp sorting, fitler sort not working
//        query.findAllSorted(DatabaseConstants.KEY_DATE_ADDED, Sort.ASCENDING);

        RealmResults<T> results = query.findAllSorted(DatabaseConstants.KEY_CREATED_DATE, Sort.DESCENDING);
//        RealmResults<T> results = query.findAll();

        return (List<T>) results;

    }

    public List<T> getItemsByColumnAndShipId(final  Class<T> className, String projectId,String column, @Nullable String columnValue){
        Realm realm = Realm.getDefaultInstance();

        RealmQuery<T> query = realm.where(className);

        query.equalTo(DatabaseConstants.KEY_PROJECT_ID, projectId);
//
//        if (status==null){
//            query.isNull(DatabaseConstants.KEY_STATUS);
//
//        }else {
//            query.equalTo(DatabaseConstants.KEY_STATUS, status);
//
//        }


        if (columnValue==null){
            query.isNull(column);

        }else {
            query.equalTo(column, columnValue);

        }

        return (List<T>) query.findAll();

    }

    public T getObjectCopy(final  Class<T> className, String key, String id){

        Realm realm = Realm.getDefaultInstance();

        return realm.copyFromRealm(realm.where(className).equalTo(key, id).findFirst());

    }

    public void updateIcheckItem(final Class<T> className, final ICheckItemData actionData) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                ICheckItem ICheckItem = (ICheckItem) getDynamicRealmObject(className, DatabaseConstants.KEY_ID, actionData.getId());

                // TODO: 20/12/2017 add fields here
//                ICheckItem.setId(actionData.getId());
                ICheckItem.setProjectId(actionData.getProjectId());
                ICheckItem.setRemark(actionData.getRemark());
                ICheckItem.setAreaId(actionData.getAreaId());
                ICheckItem.setDeckId(actionData.getDeckId());
                ICheckItem.setRemarkType(actionData.getRemarkType());
                ICheckItem.setPriority(actionData.getPriority());
                ICheckItem.setStatus(actionData.getStatus());
                ICheckItem.setDisciplineId(actionData.getDisciplineId());
                ICheckItem.setDiscipline(actionData.getDiscipline());
                ICheckItem.setArea(actionData.getArea());
                ICheckItem.setDeck(actionData.getDeck());
                ICheckItem.setActionOwner(actionData.getActionOwner());
                ICheckItem.setActionOwnerId(actionData.getActionOwnerId());
                ICheckItem.setComment(actionData.getComment());
                ICheckItem.setDueDate(actionData.getDueDate());
                ICheckItem.setAddedBy(actionData.getAddedBy());
                ICheckItem.setEditedBy(actionData.getEditedBy());
                ICheckItem.setDateAdded(actionData.getDateAdded());
                ICheckItem.setDateEdited(actionData.getDateEdited());
                ICheckItem.setAttachments(actionData.getAttachments());
                /****
                 * TODO IMPORTANT: DON'T FORGET TO SET ISSYNC
                 */
                ICheckItem.setSync(actionData.isSync());

            }
        });

        realm.close();
    }

    public List<T> getUploadFileList(Class<T> className, String rowId){

        Realm realm = Realm.getDefaultInstance();

        boolean hasFirstRowQuery = false;

        RealmQuery<Attachment> query = realm.where(Attachment.class);

        String[] items = StringHelper.splitStringArrayToArray(rowId);

        for (String item:items){
            if (!hasFirstRowQuery){
                query.equalTo(DatabaseConstants.KEY_ROW_ID, item);
                hasFirstRowQuery = true;
            }
            else {
                query.or().equalTo(DatabaseConstants.KEY_ROW_ID, item);
            }

        }

        RealmResults<Attachment> results = query.findAll();

        return (List<T>) results;

    }

    public void deleteFileList(Class<T> className, String rowId){

        Realm realm = Realm.getDefaultInstance();

        boolean hasFirstRowQuery = false;

        RealmQuery<Attachment> query = realm.where(Attachment.class);

        String[] items = StringHelper.splitStringArrayToArray(rowId);

        for (String item:items){
            if (!hasFirstRowQuery){
                query.equalTo(DatabaseConstants.KEY_ROW_ID, item);
                hasFirstRowQuery = true;
            }
            else {
                query.or().equalTo(DatabaseConstants.KEY_ROW_ID, item);
            }

        }

        final RealmResults<Attachment> results = query.findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();

            }
        });

        realm.close();

    }

    public void deleteUploadFileOffline(Class<T> className, String uplaodedId){

        Realm realm = Realm.getDefaultInstance();

        final Attachment uploadFile = (Attachment) getDynamicRealmObject(
                className, DatabaseConstants.KEY_UPLOADED_ID, uplaodedId);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                uploadFile.setDeleted(true);
                uploadFile.setSync(false);
            }
        });

        realm.close();
    }

    public List<T> getUnsyncObjects(final  Class<T> className){

        Realm realm = Realm.getDefaultInstance();

        return realm.where(className).equalTo(DatabaseConstants.KEY_IS_SYNC, false).findAll();
    }

    public List<T> getUnsyncAttachments(final  Class<T> className){

        Realm realm = Realm.getDefaultInstance();

        return realm.where(className).equalTo(DatabaseConstants.KEY_IS_SYNC, false).equalTo(DatabaseConstants.KEY_IS_DELETED, false).findAll();
    }

    public List<T> getDeletedObjects(final  Class<T> className){

        Realm realm = Realm.getDefaultInstance();
        return realm.where(className).equalTo(DatabaseConstants.KEY_IS_DELETED, true).findAll();
    }

}
