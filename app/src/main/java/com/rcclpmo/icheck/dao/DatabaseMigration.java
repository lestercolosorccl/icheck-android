package com.rcclpmo.icheck.dao;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class DatabaseMigration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0){

            schema.get("ICheckItem")
                    .addField("areaManager", String.class)
                    .addField("claimNumber", String.class)
                    .addField("contractor", String.class)
                    .addField("contractorEmail", String.class);


            schema.create("Supplier")
                    .addField("company", String.class)
                    .addField("id", String.class);


            oldVersion++;
        }

        // TODO: set database migration here
        /***
         *
         * Migration old version to current version
         *
         * **/


    }
}

