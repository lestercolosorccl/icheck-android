package com.rcclpmo.icheck.interfaces;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public interface RecyclerViewClickListener<T> {

    public void onRecyclerItemClick(int position,T object);
}

