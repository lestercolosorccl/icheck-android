package com.rcclpmo.icheck.interfaces;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public interface ResponseHandler {

    public void onSuccess(int requestCode, Object object);
    public void onFailed(int requestCode, String message);

}
