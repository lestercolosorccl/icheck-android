package com.rcclpmo.icheck.utilities;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

import io.realm.RealmObject;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class GSONUtility {


    public static GsonBuilder createGsonBuilder(Type type, Object typeAdapter){
        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping();
        gsonBuilder.registerTypeAdapter(type, typeAdapter);
        return gsonBuilder;
    }

    /***
     * gson builder for model with realm
     * */
    public static GsonBuilder createGsonBuilder(){
        GsonBuilder gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        });
        gson.setDateFormat("yyyy-MM-dd HH:mm:ss");//todo add date formatter temp
        return gson;
    }

    public static String convertToJSON(Object objectPojo){
        Gson gson = new Gson();
        String jsonString = gson.toJson(objectPojo);

        return jsonString;
    }
}

