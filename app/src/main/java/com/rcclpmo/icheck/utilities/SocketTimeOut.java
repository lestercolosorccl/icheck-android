package com.rcclpmo.icheck.utilities;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;

/**
 * Created by Patrick Villanueva on 12/12/2017.
 */

public class SocketTimeOut {
    public static RetryPolicy getRetryPolicy(){
        int socketTimeout = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return policy;
    }

    /***
     * retry policy for images
     * */
    public static RetryPolicy getRetryPolicyExtended(){
        int socketTimeout = 120000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return policy;
    }
}

